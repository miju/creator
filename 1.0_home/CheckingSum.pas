unit CheckingSum;

Interface

{
  CheckSum for Borland Delphi Enterprise Ver. 7.0 (Build 4.453).
  Ivan V. Shnurchenko, Alex V. Zemlyakov, 2006.
}

Uses SysUtils; 

function CheckSum(Path: String): int64;
function GetFileSize(namefile: string): Integer;

Implementation


//������� ���������� �� ������� ����������� �����
function CheckSum(Path: String): int64;
var Buf: int64; // �����.
    fsize : Integer; // ������ �����.
    CS: int64; // ����������� �����.
    dif: smallint; // �������� �� � ������ (���������� ���� � ����� �����).
    in_file: File;
begin
  CS := 0;
  Buf := 0;
  dif := 7;
  fsize := -1;
  AssignFile(in_file, Path);
  if FileExists(Path) then begin  // ���� ����������.
    Reset(in_file, 1);  // ������ ����� ����� 1 ����
    fsize:= GetFileSize(Path);
    // ������ ���� ��-������ � ���������� ��� � ������. ������ ����� 7 ����.
    while (not EOF(in_file)) and (fsize > dif) do begin
      BlockRead(in_file, Buf, dif);
      fsize:= fsize - dif;
      CS := CS xor Buf; // ������� ��.
      end;
    Buf := 0;
    BlockRead(in_file, Buf, fsize); // ���������� ����.
    CS := CS xor Buf; // ����������� ��.
    CloseFile(in_file);
    end
  else begin
    CS := 0; // ����� �� ����������.
    end;
  Result := CS ;
end;

function GetFileSize(namefile: string): Integer;
var
  InfoFile: TSearchRec;
  AttrFile: Integer;
  ErrorReturn: Integer;
begin
  AttrFile := $0000003F; {Any file}
  ErrorReturn := FindFirst(namefile, AttrFile, InfoFile);
  if ErrorReturn <> 0 then
    Result := -1 {� ������, ���� ���� �� ������}
  else
    Result := InfoFile.Size; {������ ����� � ������}
  FindClose(InfoFile);
end;
end.
