unit Final_Dlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  RemoveWholeDir,
  ShellAPI;

type
  TFinal_Form4 = class(TForm)
    Title_Label: TLabel;
    Info_Memo: TMemo;
    Opn_Fld_Button: TButton;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Opn_Fld_ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Final_Form4: TFinal_Form4;

implementation
uses IUPCreator;

{$R *.dfm}

procedure TFinal_Form4.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  Close;
end;

procedure TFinal_Form4.FormShow(Sender: TObject);
begin
  Info_Memo.Lines.Add('���� � �����: ');
  Info_Memo.Lines.Add(IUP_file_path);
end;

// �������� �����, ���������� ���� ����������
procedure TFinal_Form4.Opn_Fld_ButtonClick(Sender: TObject);
begin
  // ���������� ShellExecute
  if ShellExecute(Self.Handle, 'open', PChar(ExtractFilePath(IUP_file_path)), nil, nil, SW_SHOWNORMAL) < 32 then
    begin
      ShowMessage('������ ��������� ShellExecute! ��������, ����� � ������ �������.')
    end;
end;

end.
