
{********************************************************************}
{                                                                    }
{                          XML Data Binding                          }
{                                                                    }
{         Generated on: 05.05.2008 9:48:42                           }
{       Generated from: D:\Work\Borland\Creator\0.98\AuthorInf.xml   }
{   Settings stored in: D:\Work\Borland\Creator\0.98\AuthorInf.xdb   }
{                                                                    }
{********************************************************************}

unit AuthorInf;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLAUTHOR_INFORMATIONType = interface;

{ IXMLAUTHOR_INFORMATIONType }

  IXMLAUTHOR_INFORMATIONType = interface(IXMLNode)
    ['{23CC0565-269B-41CF-9E25-D0A1F2293756}']
    { Property Accessors }
    function Get_AUTHOR: WideString;
    function Get_MAIL: WideString;
    function Get_PHONE: Integer;
    function Get_SCENARIO: WideString;
    function Get_UPD_PATH: WideString;
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_MAIL(Value: WideString);
    procedure Set_PHONE(Value: Integer);
    procedure Set_SCENARIO(Value: WideString);
    procedure Set_UPD_PATH(Value: WideString);
    { Methods & Properties }
    property AUTHOR: WideString read Get_AUTHOR write Set_AUTHOR;
    property MAIL: WideString read Get_MAIL write Set_MAIL;
    property PHONE: Integer read Get_PHONE write Set_PHONE;
    property SCENARIO: WideString read Get_SCENARIO write Set_SCENARIO;
    property UPD_PATH: WideString read Get_UPD_PATH write Set_UPD_PATH;
  end;

{ Forward Decls }

  TXMLAUTHOR_INFORMATIONType = class;

{ TXMLAUTHOR_INFORMATIONType }

  TXMLAUTHOR_INFORMATIONType = class(TXMLNode, IXMLAUTHOR_INFORMATIONType)
  protected
    { IXMLAUTHOR_INFORMATIONType }
    function Get_AUTHOR: WideString;
    function Get_MAIL: WideString;
    function Get_PHONE: Integer;
    function Get_SCENARIO: WideString;
    function Get_UPD_PATH: WideString;
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_MAIL(Value: WideString);
    procedure Set_PHONE(Value: Integer);
    procedure Set_SCENARIO(Value: WideString);
    procedure Set_UPD_PATH(Value: WideString);
  end;

{ Global Functions }

function GetAUTHOR_INFORMATION(Doc: IXMLDocument): IXMLAUTHOR_INFORMATIONType;
function LoadAUTHOR_INFORMATION(const FileName: WideString): IXMLAUTHOR_INFORMATIONType;
function NewAUTHOR_INFORMATION: IXMLAUTHOR_INFORMATIONType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetAUTHOR_INFORMATION(Doc: IXMLDocument): IXMLAUTHOR_INFORMATIONType;
begin
  Result := Doc.GetDocBinding('AUTHOR_INFORMATION', TXMLAUTHOR_INFORMATIONType, TargetNamespace) as IXMLAUTHOR_INFORMATIONType;
end;

function LoadAUTHOR_INFORMATION(const FileName: WideString): IXMLAUTHOR_INFORMATIONType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('AUTHOR_INFORMATION', TXMLAUTHOR_INFORMATIONType, TargetNamespace) as IXMLAUTHOR_INFORMATIONType;
end;

function NewAUTHOR_INFORMATION: IXMLAUTHOR_INFORMATIONType;
begin
  Result := NewXMLDocument.GetDocBinding('AUTHOR_INFORMATION', TXMLAUTHOR_INFORMATIONType, TargetNamespace) as IXMLAUTHOR_INFORMATIONType;
end;

{ TXMLAUTHOR_INFORMATIONType }

function TXMLAUTHOR_INFORMATIONType.Get_AUTHOR: WideString;
begin
  Result := ChildNodes['AUTHOR'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_AUTHOR(Value: WideString);
begin
  ChildNodes['AUTHOR'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_MAIL: WideString;
begin
  Result := ChildNodes['MAIL'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_MAIL(Value: WideString);
begin
  ChildNodes['MAIL'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_PHONE: Integer;
begin
  Result := ChildNodes['PHONE'].NodeValue;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_PHONE(Value: Integer);
begin
  ChildNodes['PHONE'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_SCENARIO: WideString;
begin
  Result := ChildNodes['SCENARIO'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_SCENARIO(Value: WideString);
begin
  ChildNodes['SCENARIO'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_UPD_PATH: WideString;
begin
  Result := ChildNodes['UPD_PATH'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_UPD_PATH(Value: WideString);
begin
  ChildNodes['UPD_PATH'].NodeValue := Value;
end;

end.