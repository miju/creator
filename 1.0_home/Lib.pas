unit Lib;

interface

function NVL(cond: boolean; exp1, exp2: string): string;

implementation

function NVL(cond: boolean; exp1, exp2: string): string;
begin
  if (cond) then Result := exp1
  else           Result := exp2;
end;


end.
