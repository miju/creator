unit QuickView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TQuickView_Form = class(TForm)
    QuickView_Memo: TMemo;
    Close_Button: TButton;
    procedure Close_ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  QuickView_Form: TQuickView_Form;

implementation

uses Filelist;

{$R *.dfm}

procedure TQuickView_Form.Close_ButtonClick(Sender: TObject);
begin
  QuickView_Form.Hide;
end;

procedure TQuickView_Form.FormShow(Sender: TObject);
begin
  QuickView_Memo.Lines.LoadFromFile(QuickViewPath);
end;

end.
