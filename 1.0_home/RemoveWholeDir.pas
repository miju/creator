unit RemoveWholeDir;

interface

uses SysUtils, Dialogs, CrtProcess;

function RemoveAllDir(sDir : String) : Boolean;

implementation

// ������� ������� ��� ���������� (�����, ����� � ���� ����������)
function RemoveAllDir(sDir : String) : Boolean;
var
  iIndex : Integer;
  SearchRec : TSearchRec;
  sFileName : String;
begin
  try
    if DirectoryExists(sDir) then begin
      CPRun('cmd /c rmdir /S /Q "' + sDir + '"');
      Result := True;
    end;
  except
    ShowMessage('�������� �������� ������� ������ ' + IntToStr(GetLastError));
    Result := False;
  end
end;

end.
