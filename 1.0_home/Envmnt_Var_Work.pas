{************************************************}
{     ������ � ����������� ��������� Windows	    }
{   	http://www.sources.ru/delphi_src1.shtml    }
{************************************************}

unit Envmnt_Var_Work;

interface

uses
  SysUtils, Windows;


function GetDOSEnvVar(const VarName: string): string;

implementation


//{$R *.dfm}

// ��������� ���������� ���������
function GetDOSEnvVar(const VarName: string): string;
var
  i: integer;
begin
  Result := '';
  try
    i := GetEnvironmentVariable(PChar(VarName), nil, 0);

    if i > 0 then  
      begin  
        SetLength(Result, i);  
        GetEnvironmentVariable(Pchar(VarName), PChar(Result), i);  
      end;
  except  
    Result := '';  
  end;  
end;  

end.
