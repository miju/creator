unit FolderActions;

interface

uses
  classes,windows,SysUtils;

const
  ArchiveSignature = $18061988;

  bkpFile          = 'C:\2E57E7D74DA44E659614C6E556BCB790.dat';
  bkpFile2         = 'C:\342E5726938F49C6A86F07C86966A747.dat';

  MainBufferSize   = $10000;

type
  TActionFuntion = function(SourceFileName,DestFileName:string):boolean;
  TFilesList = TStringList;

   //function return stringlist with file names in folder searched by filter
function GetAllFiles(Filter, Folder: string):TFilesList;

function DoFolderAction(FolderPath,ArchivePath:string;ActionFunction:TActionFuntion):boolean;
function De_DoFolderAction(FolderPath,ArchivePath:string;DeActionFunction:TActionFuntion):boolean;

implementation

procedure makedir(value:string);
var i,x:integer;
    cur_dir:string;
    RootDir:String;
begin
  RootDir:=value[1]+value[2]+value[3];
  SetCurrentDirectory(pchar(RootDir));
  x:=1;
  cur_dir:='';
  if (value[1]='\') then x:=2;
  for i:=x to Length(value) do
   begin
    if not (value[i]='\')then
      cur_dir:=cur_dir+value[i];
    if (value[i]='\')or (i=length(value)) then
     begin
      if not DirectoryExists(cur_dir) then
       CreateDirectory(pchar(cur_dir),0);
      SetCurrentDirectory(pchar(cur_dir));
      cur_dir:='';
     end;
   end;
end;

function Write_LS(FileHandle:THandle;STR:string):DWORD;
var
  _L,i:WORD;
  w:DWORD;
begin
  Result:=0;
  _L:=length(STR);
  WriteFile(FileHandle,_L,2,w,0);
  for i:=1 to _L do
   WriteFile(FileHandle,STR[i],1,w,0);
  Result:=sizeof(_L)+_L;
end;

function Read_LS(FileHandle:THandle):string;
var
  _L,i:Word;
  _R:DWORD;
  _CH:Byte;
begin
  Result:='';
  ReadFile(FileHandle,_L,sizeof(_L),_R,0);
  for i:=1 to _L do
   begin
    ReadFile(FileHandle,_CH,sizeof(_CH),_R,0);
    Result:=Result+chr(_CH);
   end;
end;

procedure GetSubDirs (Folder: string; sList: TStringList);
var
  sr: TSearchRec;
begin
  if FindFirst (Folder + '*.*', faDirectory, sr) = 0 then
  try
    repeat
      if (sr.Attr and faDirectory) = faDirectory then
        sList.Add (sr.Name);
    until FindNext(sr) <> 0;
  finally
    FindClose(sr);
  end;
end;

function GetAllFiles;
var
  sr: TSearchRec;
  sDirList,_FilesList,_LST: TStringList;
  i,j: Integer;
begin
  j:=0;
  _FilesList:= TStringList.create;
  _FilesList.Clear;

  if FindFirst (Folder + Filter,faAnyFile , sr) = 0 then
  repeat
    j:=j+1;
    if j<3 then Continue;
    if (sr.Attr and fadirectory)=faDirectory then Continue;
    _FilesList.Add(Folder + sr.Name);
  until FindNext(sr) <> 0;
  FindClose(sr);
  sDirList := TStringList.Create;
  try
    GetSubDirs (Folder, sDirList);
    for i := 0 to sDirList.Count - 1 do
      if (sDirList[i] <> '.') and (sDirList[i] <> '..') then
      begin
        _LST:=GetAllFiles(Filter,IncludeTrailingPathDelimiter (Folder + sDirList[i]));
        for j:=0 to _LST.Count-1 do
         _FilesList.Add(_LST.Strings[j]);
        _LST.free;
      end;
  finally
    sDirList.Free;
  end;
  Result:= _FilesList;
end;

function DoFolderAction;
var
  ArchiveFile,CurrFile:THandle;
  Value,i,CurrFileSize,_readed,_writed:DWORD;
  _Files:TStringList;
  path_in_archive_file:string;
  pBuff:Pointer;
begin
  Result:=false;
  if FolderPath[Length(FolderPath)]<>'\' then
   FolderPath:=FolderPath+'\';

  _Files:=GetAllFiles('*.*',FolderPath);

  ArchiveFile:=CreateFile(pchar(ArchivePath),GENERIC_READ+GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,0,0);

  Value:=ArchiveSignature;
  WriteFile(ArchiveFile,value,4,_writed,0);//write main archive signature
  Value:=_Files.Count;
  WriteFile(ArchiveFile,value,4,_writed,0);//write files count

  for i:=0 to value-1 do
   begin
    if not ActionFunction(_Files.Strings[i],bkpFile) then
     begin
      CloseHandle(ArchiveFile);
      exit;
     end;
    CurrFile:=CreateFile(pchar(bkpFile),GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,0);
    CurrFileSize:=GetFileSize(CurrFile,nil);
    path_in_archive_file:=copy(_Files.Strings[i],length(FolderPath),1000);// XA XA XA XA XA
    if path_in_archive_file[1] = '\' then
     Delete(path_in_archive_file,1,1);
    Write_LS(ArchiveFile,path_in_archive_file);
    WriteFile(ArchiveFile,currFileSize,4,_writed,0);
    pBuff:=VirtualAlloc(0,MainBufferSize,MEM_COMMIT+MEM_RESERVE,PAGE_READWRITE);

    repeat
     ReadFile(CurrFile,pBuff^,MainBufferSize,_readed,0);
     WriteFile(ArchiveFile,pBuff^,_readed,_writed,0);
    until _writed<MainBufferSize;

    VirtualFree(pBuff,MEM_RELEASE,0);

    CloseHandle(CurrFile);
    DeleteFile(bkpFile);
   end;

  CloseHandle(ArchiveFile);
  Result:=True;
end;

function De_DoFolderAction;
var
  ArchiveFile,CurrFile:THandle;
  Value,i,j,CurrFileSize,_readed,_writed,_Count,BytesCopyed:DWORD;
  CurrFileName,path_in_archive_file:string;
  pBuff:Pointer;
begin
  Result:=false;

  ArchiveFile:=CreateFile(pchar(ArchivePath),GENERIC_READ+GENERIC_WRITE,FILE_SHARE_READ,0,OPEN_EXISTING,0,0);

  ReadFile(ArchiveFile,value,4,_readed,0);//write main archive signature
  if Value <>ArchiveSignature then
   begin
    CloseHandle(ArchiveFile);
    exit;
   end;

  ReadFile(ArchiveFile,_Count,4,_readed,0);//read files count

  for i:=1 to _Count do
   begin
    path_in_archive_file:=Read_LS(ArchiveFile);
    ReadFile(ArchiveFile,CurrFileSize,4,_readed,0);
    CurrFile:=CreateFile(pchar(bkpFile),GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,0,0);

    pBuff:=VirtualAlloc(0,MainBufferSize,MEM_COMMIT+MEM_RESERVE,PAGE_READWRITE);

    for j:=1 to CurrFileSize div MainBufferSize do
     begin
      ReadFile(ArchiveFile,pBuff^,MainBufferSize,_readed,0);
      WriteFile(CurrFile,pBuff^,_readed,_writed,0);
     end;
     
    ReadFile(ArchiveFile,pBuff^,CurrFileSize mod MainBufferSize,_readed,0);
    WriteFile(CurrFile,pBuff^,_readed,_writed,0);
    

    VirtualFree(pBuff,MEM_RELEASE,0);

    CloseHandle(CurrFile);

    if FolderPath[Length(FolderPath)]<>'\' then Delete(FolderPath,Length(FolderPath),1);
    if path_in_archive_file[1]<>'\' then path_in_archive_file:='\'+path_in_archive_file;  
    path_in_archive_file:=FolderPath+path_in_archive_file;
    makedir(ExtractFileDir(path_in_archive_file));

    if not  DeActionFunction(bkpfile,path_in_archive_file) then
     begin
      CloseHandle(ArchiveFile);
      exit;
     end;

    DeleteFile(bkpFile);
   end;

  CloseHandle(ArchiveFile);
  Result:=True;
end;

end.
