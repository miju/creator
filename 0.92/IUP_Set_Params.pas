unit IUP_Set_Params;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TParams_Form2 = class(TForm)
    Title_Label: TLabel;
    IUP_PathLabel: TLabel;
    Name_Label: TLabel;
    Author_Label: TLabel;
    Version_Label: TLabel;
    Pass_Label: TLabel;
    Date_Label: TLabel;
    Desc_Label: TLabel;
    Name_Edit: TEdit;
    Author_Edit: TEdit;
    Version_Edit: TEdit;
    Pass_Edit: TEdit;
    Desc_Memo: TMemo;
    Next_ToFileIndexButton: TButton;
    Back_ToMainButton: TButton;
    Cncl_Button: TButton;
    Date_Picker: TDateTimePicker;
    Desc_OpenDialog: TOpenDialog;
    Email_Label: TLabel;
    Email_Edit: TEdit;
    Phone_Label: TLabel;
    Phone_Edit: TEdit;
    procedure FormShow(Sender: TObject);
    procedure Back_ToMainButtonClick(Sender: TObject);
    procedure Cncl_ButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Next_ToFileIndexButtonClick(Sender: TObject);
    procedure Desc_MemoDblClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Params_Form2: TParams_Form2;

implementation
uses IUPCreator, FileList;

{$R *.dfm}


procedure TParams_Form2.Back_ToMainButtonClick(Sender: TObject);
begin
  Params_Form2.Hide;
  Paths_Form1.Show;
end;

procedure TParams_Form2.Cncl_ButtonClick(Sender: TObject);
begin
  Paths_Form1.WhenClose;
end;

procedure TParams_Form2.Desc_MemoDblClick(Sender: TObject);
var 
  TheMStream : TMemoryStream; 
  Zero : char; 
begin
  TheMStream := TMemoryStream.Create;
  if Desc_OpenDialog.execute then begin
    TheMStream.LoadFromFile(Desc_OpenDialog.FileName);
    TheMStream.Seek(0, soFromEnd);
    //����� ����������� ����!
    Zero := #0;
    TheMStream.Write(Zero, 1);
    TheMStream.Seek(0, soFromBeginning);
    Desc_Memo.Clear;
    Desc_Memo.SetSelTextBuf(TheMStream.Memory);
    TheMStream.Free;
  end;
end;

procedure TParams_Form2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  Paths_Form1.WhenClose;
end;

procedure TParams_Form2.FormShow(Sender: TObject);
begin
  IUP_PathLabel.Caption := '���� � �����: ' + IUP_file_path;
  Date_Picker.Date := SysUtils.Now;
//  Name_Edit.Text := 'NAME';
//  Author_Edit.Text := 'AUTH';
//  Version_Edit.Text := 'VER';
//  Pass_Edit.Text := 'PASS';
//  Email_Edit.Text := 'E-MAIL';
//  Phone_Edit.Text := 'PHONE';
end;

procedure TParams_Form2.Next_ToFileIndexButtonClick(Sender: TObject);
begin
  Params_Form2.Hide;
  Filelist_Form3.Show;
end;

end.
