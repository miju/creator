object Paths_Form1: TPaths_Form1
  Left = 353
  Top = 279
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'IUP Creator: '#1091#1082#1072#1079#1072#1085#1080#1077' '#1087#1091#1090#1077#1081
  ClientHeight = 211
  ClientWidth = 691
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 48
    Width = 167
    Height = 16
    Caption = #1055#1091#1090#1100' '#1082' '#1087#1072#1087#1082#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1081
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 104
    Width = 245
    Height = 16
    Caption = #1055#1091#1090#1100' '#1082' '#1082#1086#1085#1077#1095#1085#1086#1084#1091' '#1092#1072#1081#1083#1091' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Title_Label: TLabel
    Left = 172
    Top = 8
    Width = 348
    Height = 33
    Caption = #1057#1086#1079#1076#1072#1085#1080#1077' '#1092#1072#1081#1083#1072' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clNavy
    Font.Height = -29
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object IUP_Fldr_Browse: TButton
    Left = 608
    Top = 70
    Width = 75
    Height = 25
    Caption = #1054#1073#1079#1086#1088
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = IUP_Fldr_BrowseClick
  end
  object fldr_path_edit: TEdit
    Left = 8
    Top = 70
    Width = 575
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnDblClick = fldr_path_editDblClick
  end
  object IUP_path_edit: TEdit
    Left = 8
    Top = 126
    Width = 575
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnDblClick = IUP_path_editDblClick
  end
  object IUP_File_Browse: TButton
    Left = 607
    Top = 126
    Width = 75
    Height = 25
    Caption = #1054#1073#1079#1086#1088
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = IUP_File_BrowseClick
  end
  object Next_ToFileList: TButton
    Left = 607
    Top = 172
    Width = 75
    Height = 25
    Caption = #1044#1072#1083#1077#1077' >>'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = Next_ToFileListClick
  end
  object Cncl_Button: TButton
    Left = 8
    Top = 172
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = Cncl_ButtonClick
  end
  object Open_upd_fldr: TOpenDialog
    Left = 171
    Top = 170
  end
  object Save_IUP: TSaveDialog
    Left = 123
    Top = 170
  end
end
