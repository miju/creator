object Final_Form4: TFinal_Form4
  Left = 0
  Top = 0
  BorderIcons = [biMinimize]
  BorderStyle = bsSingle
  Caption = #1057#1086#1079#1076#1072#1090#1077#1083#1100' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1081' '#1048#1053#1058#1045#1056#1048#1053': '#1079#1072#1074#1077#1088#1096#1077#1085#1080#1077' '#1088#1072#1073#1086#1090#1099
  ClientHeight = 226
  ClientWidth = 355
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Title_Label: TLabel
    Left = 51
    Top = 8
    Width = 252
    Height = 33
    Caption = #1054#1073#1085#1086#1074#1083#1077#1085#1080#1077' '#1089#1086#1079#1076#1072#1085#1086
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clNavy
    Font.Height = -29
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object Info_Memo: TMemo
    Left = 16
    Top = 56
    Width = 322
    Height = 113
    BorderStyle = bsNone
    Color = clBtnFace
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Opn_Fld_Button: TButton
    Left = 144
    Top = 192
    Width = 121
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1087#1072#1087#1082#1091
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = Opn_Fld_ButtonClick
  end
  object Close_Button: TButton
    Left = 271
    Top = 192
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = Close_ButtonClick
  end
end
