program IUP_Creator;

uses
  Forms,
  IUPCreator in 'IUPCreator.pas' {Paths_Form1},
  CheckingSum in 'CheckingSum.pas',
  FileList in 'FileList.pas' {Filelist_Form3},
  IUP_Set_Params in 'IUP_Set_Params.pas' {Params_Form2},
  Final_Dlg in 'Final_Dlg.pas' {Final_Form4},
  IUP_Creating in 'IUP_Creating.pas',
  CopyFile in 'CopyFile.pas',
  Envmnt_Var_Work in 'Envmnt_Var_Work.pas',
  RemoveWholeDir in 'RemoveWholeDir.pas',
  ZLibAdvTools in 'ZLibAdvTools.pas',
  FolderActions in 'FolderActions.pas',
  CrtProcess in 'CrtProcess.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TPaths_Form1, Paths_Form1);
  Application.CreateForm(TFilelist_Form3, Filelist_Form3);
  Application.CreateForm(TParams_Form2, Params_Form2);
  Application.CreateForm(TFinal_Form4, Final_Form4);
  Application.Run;
end.
