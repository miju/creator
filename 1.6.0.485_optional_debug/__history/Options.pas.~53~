unit Options;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, DBCtrls, Grids, ValEdit, Mask, AppEvnts, VirtualTrees,
  ColorGrd;

type

  TVTEditorKind = (
    ekString, // TEdit
    ekMemo,   // TMemo
    ekColor   // TColorBox
  );

  PVTEditNode = ^TVTEditNode;

  TVTEditNode = record
    Extension: string;
    Color:     string;
    Comment:   string;
    Kind:      TVTEditorKind;
  end;

  TVTCustomEditor = class(TInterfacedObject, IVTEditLink)
  private
    FEdit: TWinControl;        // ������� ����� ��� ������� ���� ���������
    FTree: TVirtualStringTree; // ������ �� ������, ��������� ��������������
    FNode: PVirtualNode;       // ������������� ����
    FColumn: Integer;          // ��� �������, � ������� ��� ����������
  protected
    procedure  EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  public
    destructor Destroy; override;

    function BeginEdit: Boolean; stdcall;
    function CancelEdit: Boolean; stdcall;
    function EndEdit: Boolean; stdcall;
    function GetBounds: TRect; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex): Boolean; stdcall;
    procedure ProcessMessage(var Message: TMessage); stdcall;
    procedure SetBounds(R: TRect); stdcall;
  end;


  // �������� �����
  TOptionsForm = class(TForm)
    CloseBtn: TButton;
    SaveBtn: TButton;
    AppRunModeComboBox: TComboBox;
    Label1: TLabel;
    RecompileFormsExtEdit: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    RecompileFormsPathEdit: TEdit;
    Label6: TLabel;
    OptionsPC: TPageControl;
    PathsTS: TTabSheet;
    ExtAppsTS: TTabSheet;
    RecFormsDirBrowseBtn: TButton;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    AuthorTS: TTabSheet;
    NameEd: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    MailEd: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    ScenEd: TEdit;
    ScenarioBrowseBtn: TButton;
    Label13: TLabel;
    VersRepEd: TEdit;
    VersRepBrowseBtn: TButton;
    PhoneEd: TMaskEdit;
    ApplicationEvents1: TApplicationEvents;
    ContentTableTS: TTabSheet;
    ExtColorVT: TVirtualStringTree;
    Label14: TLabel;
    AddNodeBtn: TButton;
    DelNodeBtn: TButton;
    Label15: TLabel;
    ArchiveAppPackEd: TEdit;
    Label16: TLabel;
    ArchiveAppPackBtn: TButton;
    Label18: TLabel;
    ArchiveAppNotesREd: TRichEdit;
    Label17: TLabel;
    ArchiveAppUnPackEd: TEdit;
    Label19: TLabel;
    ArchiveAppUnPackBtn: TButton;
    Label20: TLabel;
    procedure ArchiveAppUnPackBtnClick(Sender: TObject);
    procedure ArchiveAppPackBtnClick(Sender: TObject);
    procedure DelNodeBtnClick(Sender: TObject);
    procedure AddNodeBtnClick(Sender: TObject);
    procedure ExtColorVTBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas;
      Node: PVirtualNode; Column: TColumnIndex; CellPaintMode: TVTCellPaintMode; CellRect: TRect;
      var ContentRect: TRect);
    procedure FormShow(Sender: TObject);
    procedure ExtColorVTNewText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      NewText: WideString);
    procedure ExtColorVTInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
      var InitialStates: TVirtualNodeInitStates);
    procedure ExtColorVTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType; var CellText: WideString);
    procedure ExtColorVTCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure ExtColorVTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure VersRepBrowseBtnClick(Sender: TObject);
    procedure ScenarioBrowseBtnClick(Sender: TObject);
    procedure RecFormsDirBrowseBtnClick(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure FillExtensionColorVT(AuthFilePath_: string = 'AuthorInf.xml');
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OptionsForm: TOptionsForm;

implementation

uses AuthorInf, FileCtrl, XMLWork, XMLIntf;

{$R *.dfm}

// *********** << �������� ��������� ��� �� ***********
destructor TVTCustomEditor.Destroy;
begin
  FreeAndNil(FEdit);
  inherited;
end;

//------------------------------------------------------------------------------
// ��� ��������� ������� � ����������.
// ������ �������������� �� Escape, ���������� �������������� �� Enter,
// � ������� ����� ������ �� Up/Down, ���� ������ ��������� � ����� ����� ���
// ��������� ���� �� �������.
//------------------------------------------------------------------------------
procedure TVTCustomEditor.EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  CanContinue: Boolean;
begin
  CanContinue := True;
  case Key of
    VK_ESCAPE: // ������ Escape
      if CanContinue then begin
        FTree.CancelEditNode;
        Key := 0;
      end;
    VK_RETURN: // ������ Enter
      if CanContinue then begin
        // ���� Ctrl ��� TMemo �� �����, �� ��������� ��������������
        // ������� ���, ����� ����� ���� �� Ctrl+Enter ��������� � Memo
        // ����� �����.
        if (FEdit is TMemo) and (Shift = []) then
          FTree.EndEditNode
        else if not(FEdit is TMemo) then
          FTree.EndEditNode;
        Key := 0;
      end;
    VK_UP, VK_DOWN:
      begin
        // ���������, �� ��� �� ������ � ����������. ���� ���, �� ���������
        // ���������� ������, ���� ���, �� �������� ������� ������.
        CanContinue := Shift = [];
        if CanContinue then begin
          // �������� ������� ������
          PostMessage(FTree.Handle, WM_KEYDOWN, Key, 0);
          Key := 0;
        end;
      end;
  end;
end;

//------------------------------------------------------------------------------
// �������� ��������������, ����� �������� �������� � ���������� ��� �����.
//------------------------------------------------------------------------------
function TVTCustomEditor.BeginEdit: Boolean;
begin
  Result := True;
  with FEdit do
  begin
    Show;
    SetFocus;
  end;
end;

//------------------------------------------------------------------------------
// ����������, ������ ��������.
//------------------------------------------------------------------------------
function TVTCustomEditor.CancelEdit: Boolean;
 begin
  Result := True;
  FEdit.Hide;
end;

//------------------------------------------------------------------------------
// ������� �����������, ������ ��������, ��������� ������ ���� � ����������
// ����� ������.
//------------------------------------------------------------------------------
function TVTCustomEditor.EndEdit: Boolean;
var
  Txt: String;
begin
  Result := True;
  if FEdit is TEdit then
    Txt := TEdit(FEdit).Text
  else
    if FEdit is TMemo then
      Txt := TEdit(FEdit).Text
  else
    if FEdit is TColorBox then
      Txt := ColorToString(TColorBox(FEdit).Selected);
  // �������� ����� ���� (���� Value � TVTEditNode) ����� ������� OnNewText
  // � ������
  FTree.Text[FNode, FColumn] := Txt;
  FEdit.Hide;
  FTree.SetFocus;
end;

//------------------------------------------------------------------------------
// ���������� ������� ���������.
//------------------------------------------------------------------------------
function TVTCustomEditor.GetBounds: TRect;
begin
  Result := FEdit.BoundsRect;
end;

//------------------------------------------------------------------------------
// � ���������� � �������������� �� ������ ������� ��������� TWinControl
// ������� ������ ������� � ������������ � ����� Kind � TVTEditNode.
//------------------------------------------------------------------------------
function TVTCustomEditor.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex): Boolean;
var
  VTEditNode: PVTEditNode;
begin
  Result := True;
  FTree := Tree as TVirtualStringTree;
  FNode := Node;
  FColumn := Column;
  FreeAndNil(FEdit);
  VTEditNode := FTree.GetNodeData(Node);

  case Column of
    0: VTEditNode.Kind := ekString;
    1: VTEditNode.Kind := ekColor;
    2: VTEditNode.Kind := ekMemo;
  end;

  case VTEditNode.Kind of
    ekString:
    begin
      FEdit := TEdit.Create(nil);
      with FEdit as TEdit do
      begin
        AutoSize := False;
        Visible := False;
        Parent := Tree;
        Text := VTEditNode.Extension;
        OnKeyDown := EditKeyDown;
      end;
    end;
    ekMemo:
    begin
      FEdit := TMemo.Create(nil);
      with FEdit as TMemo do
      begin
        Visible := False;
        Parent := Tree;
        ScrollBars := ssVertical;
        Text := VTEditNode.Comment;
        OnKeyDown := EditKeyDown;
      end;
    end;
    ekColor:
    begin
      FEdit := TColorBox.Create(nil);
      with FEdit as TColorBox do
      begin
        Visible := False;
        Parent := Tree;
        Selected := StringToColor(VTEditNode.Color);
        Style := [cbPrettyNames, cbStandardColors, cbCustomColor, cbCustomColors];
        OnKeyDown := EditKeyDown;
      end;
    end;
  else
    Result := False;
  end;
end;

//------------------------------------------------------------------------------
// ��������� ��������� Windows ��� ���������.
//------------------------------------------------------------------------------
procedure TVTCustomEditor.ProcessMessage(var Message: TMessage);
begin
  FEdit.WindowProc(Message);
end;

//------------------------------------------------------------------------------
// ������������� ������� ��������� � ������������ � ������� � ������� �������.
//------------------------------------------------------------------------------
procedure TVTCustomEditor.SetBounds(R: TRect);
var
  Dummy: Integer;
begin
  FTree.Header.Columns.GetColumnBounds(FColumn, Dummy, R.Right);
  FEdit.BoundsRect := R;
  if FEdit is TMemo then begin
    FEdit.Height := 80;
    FEdit.Width  := 200;
  end;
end;

// *********** >> �������� ��������� ��� �� ***********

procedure TOptionsForm.AddNodeBtnClick(Sender: TObject);
var VTNode: PVirtualNode;
begin
  VTNode := ExtColorVT.AddChild(ExtColorVT.RootNode);
  ExtColorVT.Text[VTNode, 0] := '�����_������';
  ExtColorVT.Text[VTNode, 2] := '                                      ';
end;

procedure TOptionsForm.ApplicationEvents1Exception(Sender: TObject; E: Exception);
begin
  // ��������� ������������� ����� �������� 
  if (E is EDBeditError) and (Sender is TMaskEdit) then
  begin
    NIL;
//    MessProc('', '����������, ������� ���������� ������', False, False, True);
//    MessageBox(0, PChar('����������, ������� ���������� ������'), PChar('������!'), MB_ICONWARNING);
//    (Sender as TMaskEdit).SetFocus;
  end;
end;

procedure TOptionsForm.ArchiveAppPackBtnClick(Sender: TObject);
var ArchiveAppDlg: TOpenDialog;
begin
  ArchiveAppDlg := TOpenDialog.Create(AppRunModeComboBox);
  try
    with ArchiveAppDlg do begin
      InitialDir := GetEnvironmentVariable('ProgramFiles');
      FileName := '*';
      Filter := '����������� �����|*';
      if Execute then begin
        ArchiveAppPackEd.Text := '"' + FileName + '"';
      end;
    end;
  finally
    FreeAndNil(ArchiveAppDlg);
  end;
end;

procedure TOptionsForm.ArchiveAppUnPackBtnClick(Sender: TObject);
var ArchiveAppDlg: TOpenDialog;
begin
  ArchiveAppDlg := TOpenDialog.Create(AppRunModeComboBox);
  try
    with ArchiveAppDlg do begin
      InitialDir := GetEnvironmentVariable('ProgramFiles');
      FileName := '*';
      Filter := '����������� �����|*';
      if Execute then begin
        ArchiveAppUnPackEd.Text := '"' + FileName + '"';
      end;
    end;
  finally
    FreeAndNil(ArchiveAppDlg);
  end;
end;

procedure TOptionsForm.DelNodeBtnClick(Sender: TObject);
begin
  ExtColorVT.DeleteNode(ExtColorVT.FocusedNode);
end;

procedure TOptionsForm.RecFormsDirBrowseBtnClick(Sender: TObject);
var dir: string;
begin
  Dir := RecompileFormsPathEdit.Text;
  SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], 1000);
  if DirectoryExists(Dir) then
    RecompileFormsPathEdit.Text := dir;
end;

procedure TOptionsForm.ExtColorVTBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas;
  Node: PVirtualNode; Column: TColumnIndex; CellPaintMode: TVTCellPaintMode; CellRect: TRect;
  var ContentRect: TRect);
var DrawNode: PVTEditNode;
begin
  if Column <> 1 then exit; // ������ ������ ������ �������

  DrawNode := Sender.GetNodeData(Node);
  if not Assigned(Node) then exit;
  try
    with TargetCanvas do begin
      Brush.Color := StringToColor(DrawNode^.Color);
      FillRect(CellRect);
      ExtColorVT.Tag := 0;
    end;
  except
    on e:Exception  do begin
      DrawNode^.Color := 'clWindow';
      ExtColorVT.SetFocus;
      ExtColorVT.FocusedNode := Node;
      NULL;
    end;
  end;
end;

procedure TOptionsForm.ExtColorVTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  with Sender do begin
    if Assigned(Node) then begin
      // ������� ����� ����� ��������������� ����. ��� �� �� ��������� � �����,
      // (��������, ��������) ����� ������� �������������� �������.
      Sender.EditNode(Node, ExtColorVT.FocusedColumn);
    end;
  end;
end;

procedure TOptionsForm.ExtColorVTCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; out EditLink: IVTEditLink);
begin
  EditLink := TVTCustomEditor.Create;
end;

procedure TOptionsForm.ExtColorVTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
var VTEditNode: PVTEditNode;
begin
  VTEditNode := Sender.GetNodeData(Node);
  if not Assigned(VTEditNode) then exit;
  case Column of
    0: CellText := VTEditNode^.Extension;
    1: CellText := VTEditNode^.Color;
    2: CellText := VTEditNode^.Comment;
  end;
end;

procedure TOptionsForm.ExtColorVTInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
  var InitialStates: TVirtualNodeInitStates);
begin
  Node.CheckType := ctButton;
end;

procedure TOptionsForm.ExtColorVTNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; NewText: WideString);
var ElemNode: PVTEditNode;
begin
  ElemNode := ExtColorVT.GetNodeData(Node);
  if Assigned(ElemNode) then begin
    case Column of
      0: ElemNode^.Extension := NewText;
      1: ElemNode^.Color     := NewText;
      2: ElemNode^.Comment   := NewText;
    end;
  end;
end;

procedure TOptionsForm.FormCreate(Sender: TObject);
//var AuthInfFile: IXMLAUTHOR_INFORMATIONType;
begin

  ExtColorVT.NodeDataSize := SizeOf(TVTEditNode);

  With AppRunModeComboBox do begin
    Items.Add('��������');
    Items.Add('���������');
    Items.Add('����������');
  end;
  try
//    if FileExists(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml') then begin
//      AuthInfFile := LoadAUTHOR_INFORMATION(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml');

//      case StrToInt(XMLWork.GetXMLNodeValue('RUNMODE')) of
//        // 00 SW_HIDE            �������� ���� ���������� � ���������� ������ ����.
//        00: AppRunModeComboBox.ItemIndex := 0;
//        // 07 SW_SHOWMINNOACTIVE ���������� ���� � ��������� ����. �������� ���� �������� ��������.
//        07: AppRunModeComboBox.ItemIndex := 1;
//        // 10 SW_SHOWDEFAULT     ������������� ����� ������ ����������� �� SW_����� ��������� � ��������� STATUPINFO, ������������� ��� ������� CreateProcess ����������, ������� ��������� ����������. ���������� ������ ������� ShowWindow � ���� ������ ��� ��������� ���������� ������ ����������� ������ ����.
//        10: AppRunModeComboBox.ItemIndex := 2;
//      end;
//
//      RecompileFormsExtEdit.Text  := XMLWork.GetXMLNodeValue('MASK');
//      RecompileFormsPathEdit.Text := XMLWork.GetXMLNodeValue('DIR');
//      ScenEd.Text    := XMLWork.GetXMLNodeValue('SCENARIO');
//      VersRepEd.Text := XMLWork.GetXMLNodeValue('VERS_PATH');
//      NameEd.Text    := XMLWork.GetXMLNodeValue('AUTHOR');
//      MailEd.Text    := XMLWork.GetXMLNodeValue('MAIL');
//      PhoneEd.Text   := XMLWork.GetXMLNodeValue('PHONE');

//    end;
  except
    on e:Exception do begin
       Application.MessageBox(PChar('Options.pas: ������ ���������� ��������. ' + e.Message), '������');
    end;
  end;
end;


procedure TOptionsForm.FillExtensionColorVT(AuthFilePath_: string = 'AuthorInf.xml');
var AuthFile: IXMLAUTHOR_INFORMATIONType;
    i: integer;
    ElemNode: PVTEditNode;
begin
  if not FileExists(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml') then exit;
  AuthFile := LoadAUTHOR_INFORMATION(AuthFilePath_);
  for i := 0 to (AuthFile.CONTENTTABLE.EXTCOLOR.ChildNodes.Count - 1) do begin
    ElemNode := ExtColorVT.GetNodeData(ExtColorVT.AddChild(ExtColorVT.RootNode));
    if not Assigned(ElemNode) then exit;
    ElemNode^.Extension := AuthFile.CONTENTTABLE.EXTCOLOR.ChildNodes[i].LocalName;
    ElemNode^.Color     := AuthFile.CONTENTTABLE.EXTCOLOR.ChildNodes[i].Text;
    ElemNode^.Comment   := AuthFile.CONTENTTABLE.EXTCOLOR.ChildNodes[i].Attributes['Comment'];
  end;
end;


procedure TOptionsForm.FormShow(Sender: TObject);
begin
  case StrToInt(XMLWork.GetXMLNodeValue('RUNMODE')) of
    // 00 SW_HIDE            �������� ���� ���������� � ���������� ������ ����.
    00: AppRunModeComboBox.ItemIndex := 0;
    // 07 SW_SHOWMINNOACTIVE ���������� ���� � ��������� ����. �������� ���� �������� ��������.
    07: AppRunModeComboBox.ItemIndex := 1;
    // 10 SW_SHOWDEFAULT     ������������� ����� ������ ����������� �� SW_����� ��������� � ��������� STATUPINFO, ������������� ��� ������� CreateProcess ����������, ������� ��������� ����������. ���������� ������ ������� ShowWindow � ���� ������ ��� ��������� ���������� ������ ����������� ������ ����.
    10: AppRunModeComboBox.ItemIndex := 2;
  end;

  RecompileFormsExtEdit.Text  := XMLWork.GetXMLNodeValue('MASK');
  RecompileFormsPathEdit.Text := XMLWork.GetXMLNodeValue('DIR');
  ScenEd.Text    := XMLWork.GetXMLNodeValue('SCENARIO');
  VersRepEd.Text := XMLWork.GetXMLNodeValue('VERS_PATH');
  NameEd.Text    := XMLWork.GetXMLNodeValue('AUTHOR');
  MailEd.Text    := XMLWork.GetXMLNodeValue('MAIL');
  PhoneEd.Text   := XMLWork.GetXMLNodeValue('PHONE');
  ArchiveAppPackEd.Text   := XMLWork.GetXMLNodeValue('ARCHIVEAPPPACK');
  ArchiveAppUnPackEd.Text := XMLWork.GetXMLNodeValue('ARCHIVEAPPUNPACK');
  ArchiveAppNotesREd.Text := XMLWork.GetXMLNodeValue('ARCHIVEAPPNOTES');

  ExtColorVT.Clear;
  FillExtensionColorVT;

end;

procedure TOptionsForm.SaveBtnClick(Sender: TObject);
var AuthInfFile: IXMLAUTHOR_INFORMATIONType;
    AuthInfFilePath: string;
    ElemNode: PVTEditNode;
    CurNode: PVirtualNode;
begin
  AuthInfFilePath := ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml';
  try
    if FileExists(AuthInfFilePath) then begin
      AuthInfFile := LoadAUTHOR_INFORMATION(AuthInfFilePath);
      AuthInfFile.OwnerDocument.Options := AuthInfFile.OwnerDocument.Options + [doNodeAutoIndent];

      with AuthInfFile do begin
        case AppRunModeComboBox.ItemIndex of
          // 00 SW_HIDE            �������� ���� ���������� � ���������� ������ ����.
          00: EXTERNALAPPS.RUNMODE := 0;
          // 07 SW_SHOWMINNOACTIVE ���������� ���� � ��������� ����. �������� ���� �������� ��������.
          01: EXTERNALAPPS.RUNMODE := 7;
          // 10 SW_SHOWDEFAULT     ������������� ����� ������ ����������� �� SW_����� ��������� � ��������� STATUPINFO, ������������� ��� ������� CreateProcess ����������, ������� ��������� ����������. ���������� ������ ������� ShowWindow � ���� ������ ��� ��������� ���������� ������ ����������� ������ ����.
          02: EXTERNALAPPS.RUNMODE := 10;
        end;

        EXTERNALAPPS.ARCHIVEAPPPACK      := ArchiveAppEd.Text;
        EXTERNALAPPS.ARCHIVEAPPNOTES := ArchiveAppNotesREd.Text;

        RECFORMS.MASK := RecompileFormsExtEdit.Text;
        RECFORMS.DIR  := RecompileFormsPathEdit.Text;

        SCENARIO  := ScenEd.Text;
        VERS_PATH := VersRepEd.Text;

        AUTHOR := NameEd.Text;
        MAIL   := MailEd.Text;
        PHONE  := PhoneEd.Text;

        // << ����� ���������
        if ExtColorVT.RootNodeCount = 0 then exit;
        AuthInfFile.CONTENTTABLE.EXTCOLOR.ChildNodes.Clear;
        CurNode := ExtColorVT.GetFirst(False);
        if not Assigned(CurNode) then exit;
        repeat
          ElemNode := ExtColorVT.GetNodeData(CurNode);
          if Assigned(ElemNode) then begin
            with AuthInfFile.CONTENTTABLE.EXTCOLOR do begin
              ChildNodes[UpperCase(ElemNode.Extension)].Text := ElemNode.Color;
              ChildNodes[UpperCase(ElemNode.Extension)].Attributes['Comment'] := ElemNode.Comment;
            end;
         end;
          CurNode := ExtColorVT.GetNext(CurNode, False);
        until CurNode = nil;
        // >> ����� ���������

        OwnerDocument.SaveToFile(AuthInfFilePath);
      end;

    end;
  except
    on e:Exception do begin
       Application.MessageBox(PChar('Options.pas: ������ ���������� ��������. ' + e.Message), '������');
    end;
  end;
end;

procedure TOptionsForm.ScenarioBrowseBtnClick(Sender: TObject);
var ScenarioBrowseDlg: TOpenDialog;
begin
  ScenarioBrowseDlg := TOpenDialog.Create(AppRunModeComboBox);
  try
    with ScenarioBrowseDlg do begin
      InitialDir := ExtractFileDir(XMLWork.GetXMLNodeValue('SCENARIO'));
      FileName := 'Scenario.xml';
      Filter := '����� XML|*.xml';
      if Execute then begin
        ScenEd.Text := FileName;
      end;
    end;
  finally
    FreeAndNil(ScenarioBrowseDlg);
  end;
end;

procedure TOptionsForm.VersRepBrowseBtnClick(Sender: TObject);
var VersRepBrowseDlg: TOpenDialog;
begin
  VersRepBrowseDlg := TOpenDialog.Create(AppRunModeComboBox);
  try
    with VersRepBrowseDlg do begin
      InitialDir := ExtractFileDir(XMLWork.GetXMLNodeValue('VERS_PATH'));
      Filter := '����� XML|*.xml';
      FileName := 'VersionRepository.xml';
      if Execute then begin
        VersRepEd.Text := FileName;
      end;
    end;
  finally
    FreeAndNil(VersRepBrowseDlg);
  end;
end;

end.
