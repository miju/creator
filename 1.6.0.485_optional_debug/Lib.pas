{$WARN SYMBOL_DEPRECATED OFF}
{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_PLATFORM OFF}

unit Lib;

interface

uses SysUtils, Windows, Dialogs, Grids, Classes, WinSock, Masks, ClipBrd;

{����������� �������� ��������}
function NVL(cond: boolean; pos_res, neg_res: string): string;
{������ ��������� � ����� �����}
procedure ReplaceSpacesByUnderline(Dir_: string; OldSymb_, NewSymb_: string);
{������ ��������� � ������}
function ReplaceStr(const Str, OldSub, NewSub: string): string;
{���������� ����� �� ������ �������}
procedure SortStringGrid(var GenStrGrid: TStringGrid; ThatCol: Integer);

function GetIPTable: string;
function RemoveFilesAndFoldersByMask(Folder_, mask_: string): boolean;
function FileVersion(AFileName: string): string;
function CPRun(cline_:string; ShowMode_: Word = SW_SHOWMINIMIZED):Boolean;
function ForceFolderPath(Path_:string): string;
function DetectArchType(Path_: string): string;
procedure RemoveEmptyStringFromFilesInFolder(FolderPath_, exts_: string);
procedure StrToClipbrd(StrValue: string);
procedure PutStringIntoClipBoard(const Str: WideString);


implementation

{ DONE : ������� ������ ������ ������� ������� ���������� }

function ForceFolderPath(Path_:string): string;
begin
  Result := Path_;
  if Length(Path_) = 0 then exit;
  if Path_[length(Path_)]<>'\' then begin
    Result := (Path_+'\');
  end;
end;

function CPRun(cline_:string; ShowMode_: Word = SW_SHOWMINIMIZED):Boolean;
var
  //����������� ��������� ��� CreateProcess
  pi : TProcessInformation;
  si : TStartupInfo;
begin
  Result := True;
  try
    ZeroMemory(@si,sizeof(si));
    with si do begin
      cb:=SizeOf(si);
      dwFlags :=  STARTF_USESHOWWINDOW;
      wShowWindow := ShowMode_;
    end;

    if not CreateProcess(
      nil, //pointer to name of executable module
      PChar(cline_), // Command line.
      nil, // Process handle not inheritable.
      nil, // Thread handle not inheritable.
      False, // Set handle inheritance to FALSE.
      0, // No creation flags.
      nil, // Use parent's environment block.
      nil, // Use parent's starting directory.
      si, // Pointer to STARTUPINFO structure.
      pi ) // Pointer to PROCESS_INFORMATION structure.
    then
    begin
      ShowMessage('���������� ��������� ������� �� ����������: ' + cline_);
      Result := False;
    end;
    //������� ��������� ��������
    WaitForSingleObject(pi.hProcess,INFINITE);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
  except
    on e:Exception do begin
      ShowMessage('�� ������� ��������� ������� "' + cline_ + '".' + sLineBreak +
                  '������: ' + e.Message);
      Result := False;
      Exit;
    end;
  end;
end;

{ DONE : ������� �������� ������ ����� �� ������ }
// ������� ��������, ���� �� � ������� ���������� �����
procedure RemoveEmptyStringFromFile(FilePath_: string);
var TmpStringList: TStringList;
    I: integer;
begin
  TmpStringList := TStringList.Create;
  try
    TmpStringList.LoadFromFile(FilePath_);
    for I := (TmpStringList.Count - 1) downto 0 do begin
      if Trim(TmpStringList.Strings[I]) = '' then begin
        TmpStringList.Delete(I);
      end;
    end;
    TmpStringList.SaveToFile(FilePath_);
    TmpStringList.Free;
  except
    on e:Exception do begin
      MessageBox(0, PChar('�������� � ��������� ������ ����� �� �����: "' + FilePath_ + '"' + sLineBreak + '������: ' + e.Message),
                    'RemoveEmptyStringFromFile',
                    mb_IconInformation + mb_OK + mb_TaskModal);
      TmpStringList.Free;
      Exit;
    end;
  end;
end;

{ ��������� ������� ������ � ���������� ������ �� ���� ������, ����������� � ��������� ���������� �
    ������� ��������� ����������.
    ����� �������� ����� ������������ ��� '\' �� �����.
    ������ ���������� ����� ��� ' .ext1 | ext2 | ext3 '
}
{ DONE : ���������� ���������� ����� }
procedure RemoveEmptyStringFromFilesInFolder(FolderPath_, exts_: string);
var SearchRec:TSearchRec;
    FileDateTime: Integer;
begin

  try
    if FolderPath_<>'' then begin
      if FolderPath_[length(FolderPath_)]<>'\' then begin
        FolderPath_:=FolderPath_+'\';
      end;
    end;

    if FindFirst(FolderPath_+'*.*', faAnyFile, SearchRec)=0 then
     repeat
       if (SearchRec.name='.') or (SearchRec.name='..') then continue;
       if (SearchRec.Attr and faDirectory)<>0 then begin
         RemoveEmptyStringFromFilesInFolder(FolderPath_+SearchRec.name, exts_) //������������ ����������
       end
       else
         if Pos(ExtractFileExt(FolderPath_+SearchRec.name), exts_) > 0 then begin
           // �������� �� ����� ��� ������ � ���������� ������, �������� ��� ������� �������
           FileDateTime := FileAge(FolderPath_+SearchRec.name);
           RemoveEmptyStringFromFile(FolderPath_+SearchRec.name);
           FileSetDate(FolderPath_+SearchRec.name, FileDateTime);
         end;
      until FindNext(SearchRec)<>0;
  except
    on e:Exception do begin
      MessageBox(0,
                 PChar('�������� � ��������� ������ ����� �� �����: "' + FolderPath_+SearchRec.name + '"' + sLineBreak +
                 '������: ' + e.Message),
                 'RemoveEmptyStringFromFilesInFolder',
                 MB_ICONERROR + mb_OK + mb_TaskModal);
    end;
  end;
end;

{ �����: Vit
  WEB-����: http://forum.vingrad.ru
}
function FileVersion(AFileName: string): string;
var
  szName: array[0..255] of Char;
  P: Pointer;
  Value: Pointer;
  Len: UINT;
  GetTranslationString: string;
  FFileName: PChar;
  FValid: boolean;
  FSize: DWORD;
  FHandle: DWORD;
  FBuffer: PChar;
begin
  try
    FFileName := StrPCopy(StrAlloc(Length(AFileName) + 1), AFileName);
    FValid := False;
    FSize := GetFileVersionInfoSize(FFileName, FHandle);
    if FSize > 0 then
    try
      GetMem(FBuffer, FSize);
      FValid := GetFileVersionInfo(FFileName, FHandle, FSize, FBuffer);
    except
      FValid := False;
      raise;
    end;
    Result := '';
    if FValid then
      VerQueryValue(FBuffer, '\VarFileInfo\Translation', p, Len)
    else
      p := nil;
    if P <> nil then
      GetTranslationString := IntToHex(MakeLong(HiWord(Longint(P^)),
        LoWord(Longint(P^))), 8);
    if FValid then
    begin
      StrPCopy(szName, '\StringFileInfo\' + GetTranslationString +
        '\FileVersion');
      if VerQueryValue(FBuffer, szName, Value, Len) then
        Result := StrPas(PChar(Value));
    end;
  finally
    try
      if FBuffer <> nil then
        FreeMem(FBuffer, FSize);
    except
    end;
    try
      StrDispose(FFileName);
    except
    end;
  end;
end;

function RemoveFilesAndFoldersByMask(Folder_, mask_: string): boolean;
var SearchRec:TSearchRec;
    tmpstr: string;
begin
  try
    // << ��������� ������������ ������� ������
    if ((not DirectoryExists(Folder_))
    or  (Length(mask_) = 0)) then begin
      Result := True;
      exit;
    end;
    if Folder_<>'' then
      if Folder_[length(Folder_)]<>'\' then Folder_:=Folder_+'\';
    // >> ��������� ������������ ������� ������

    // << ������� ��� �����, ���������� ��� �����.
    tmpstr := 'cmd /c del /Q "' + Folder_ + mask_ + '"';
//    if WinExec(PChar(tmpstr), SW_SHOWMINNOACTIVE) < 32 then begin
    if not CPRun(tmpstr) then begin
      ShowMessage('Lib.pas. RemoveFilesAndFoldersByMask: �������� �������� ��� ���������� �������� "' + tmpstr + '"');
    end;
    // >> ������� ��� �����, ���������� ��� �����.

    // << ������� ��� �����, ���������� ��� �����.
    if FindFirst(Folder_+'*.*', faDirectory, SearchRec)=0 then
      repeat
        if (SearchRec.name='.') or (SearchRec.name='..') then continue;
        if masks.MatchesMask(SearchRec.Name, mask_) then begin
          tmpstr := 'cmd /c rmdir /S /Q "' + Folder_ + SearchRec.Name + '"';
//          if WinExec(PChar(tmpstr), SW_SHOWMINNOACTIVE) < 32 then begin
          if not CPRun(tmpstr) then begin
            ShowMessage('Lib.pas. RemoveFilesAndFoldersByMask: �������� �������� ��� ���������� �������� "' + tmpstr + '"');
          end;
          Sleep(200);
        end;
      until FindNext(SearchRec)<>0;
    SysUtils.FindClose(SearchRec);
    // >> ������� ��� �����, ���������� ��� �����.
    Result := True;
  except
    on e:Exception do begin
      ShowMessage('Lib.pas. RemoveFilesAndFoldersByMask: '+ e.Message);
      Result := False;
    end;
  end;

end;

function NVL(cond: boolean; pos_res, neg_res: string): string;
begin
  if (cond) then Result := pos_res
  else           Result := neg_res;
end;

function GetIPTable: string;
type
  InAddr = Array[0..10] of PInAddr;
  TInAddr = ^InAddr;
var
  Host: PHostEnt;
  pPtr: TInAddr;
  Buffer: Array[0..63] of char;
  i: Integer;
  Socket: TWSAData;
begin
  try
   Result := '';
   WSAStartup( $101, Socket );
   GetHostName( Buffer, SizeOf( Buffer ) );
   Host := GetHostByName( buffer );
   if Host = nil then Exit;
   pPtr := TInAddr( Host^.h_addr_list );
   i := 0;
   while pPtr^[i] <> nil do
   begin
      Result := nvl(Result <> '', Result + ', ', Result) + inet_ntoa( pPtr^[i]^ );
      Inc( i );
   end;
   WSACleanup;
  except
    on e:Exception do
      ShowMessage('������ ��������� ������ � ������� ����������� (IP-�������): '+ e.Message);
  end;
end;

// ������ �������
procedure ReplaceSpacesByUnderline(Dir_: string; OldSymb_, NewSymb_: string);
var SearchRec: TSearchRec;
    Filename: string;
begin
  if Dir_<>'' then
    if Dir_[length(Dir_)]<>'\' then Dir_:=Dir_+'\';
  if FindFirst(Dir_+'*.*', faAnyFile, SearchRec)=0 then
    repeat
      if (SearchRec.name='.') or (SearchRec.name='..') then continue;
      if (SearchRec.Attr and faDirectory)<>0 then
        ReplaceSpacesByUnderline(Dir_+SearchRec.name, OldSymb_, NewSymb_) //we found Directory: "Dir+SearchRec.name"
      else
        FileName := ReplaceStr(SearchRec.name, OldSymb_, NewSymb_);
        RenameFile(Dir_+SearchRec.name, Dir_+FileName);
    until FindNext(SearchRec)<>0;
  SysUtils.FindClose(SearchRec);
end;

function ReplaceStr(const Str, OldSub, NewSub: string): string;
{������ ��������� � ������}
var
 I:Integer;
 Source:string;
begin
  Source:= Str;
  Result:= '';
  repeat
    I:=Pos(OldSub, Source);
    if I > 0 then begin
      Result:=Result+Copy(Source,1,I-1)+NewSub;
      Source:=Copy(Source,I+Length(OldSub),MaxInt);
    end else Result:=Result+Source;
  until I<=0;
end;

procedure SortStringGrid(var GenStrGrid: TStringGrid; ThatCol: Integer);
 const
   // Define the Separator
  TheSeparator = '@';
 var
   CountItem, R, T, K, ThePosition: integer;
   MyList: TStringList;
   MyString, TempString: string;
 begin
   // Give the number of rows in the StringGrid
  CountItem := GenStrGrid.RowCount;
   //Create the List
  MyList        := TStringList.Create;
   MyList.Sorted := False;
   try
     begin
       for R := 1 to (CountItem - 1) do
         MyList.Add(GenStrGrid.Rows[R].Strings[ThatCol] + TheSeparator +
           GenStrGrid.Rows[R].Text);
       //Sort the List 
      Mylist.Sort;

       for K := 1 to Mylist.Count do
       begin
         //Take the String of the line (K � 1)
        MyString := MyList.Strings[(K - 1)];
         //Find the position of the Separator in the String
        ThePosition := Pos(TheSeparator, MyString);
         TempString  := '';
         {Eliminate the Text of the column on which we have sorted the StringGrid  }
         TempString := Copy(MyString, (ThePosition + 1), Length(MyString));
         MyList.Strings[(K - 1)] := '';
         MyList.Strings[(K - 1)] := TempString;
       end;

       // Refill the StringGrid
      for T := 1 to (CountItem - 1) do
         GenStrGrid.Rows[T].Text := MyList.Strings[(T - 1)];
     end;
   finally
     //Free the List
    MyList.Free;
   end;
 end;

// ���������� ��� ������: rar, int(-ernal), etc.
function DetectArchType(Path_: string): string;
var I: integer;
    ch: char;
    archfile: File of Byte;
    tmp: string;
begin
  Result := 'int';
  try
    if not FileExists(Path_) then exit;
    assign(archfile,Path_);
    reset(archfile);
    for I := 0 to 30 do begin
      Blockread(archfile,ch,sizeof(ch));
      tmp := tmp + ch;
    end;
    if Pos('Rar!', tmp) > 0 then begin
      Result := 'rar';
    end;
    close(archfile);
  except
    on e:Exception do begin
      MessageBox(0, '������!', PChar('������ ����������� ���� �����: '+ e.Message), MB_OK);
    end;
  end;
end;

procedure StrToClipbrd(StrValue: string);
var
  S: string;
  hMem: THandle;
  pMem: PChar;
begin
  hMem := GlobalAlloc(GHND or GMEM_SHARE, Length(StrValue) + 1);
  if hMem <> 0 then
  begin
    pMem := GlobalLock(hMem);
    if pMem <> nil then
    begin
      StrPCopy(pMem, StrValue);
      GlobalUnlock(hMem);
      if OpenClipboard(0) then
      begin
        EmptyClipboard;
        SetClipboardData(CF_TEXT, hMem);
        CloseClipboard;
      end
      else
        GlobalFree(hMem);
    end
    else
      GlobalFree(hMem);
  end;
end;

procedure PutStringIntoClipBoard(const Str: WideString);
var
  Size: Integer;   
  Data: THandle;   
  DataPtr: Pointer;   
begin  
  Size:=Length(Str);   
  if Size = 0 then exit;   
  if not IsClipboardFormatAvailable(CF_UNICODETEXT) then  
    Clipboard.AsText:=Str   
  else  
  begin  
    Size:=Size shl 1 + 2;   
    Data := GlobalAlloc(GMEM_MOVEABLE+GMEM_DDESHARE, Size);   
    try  
      DataPtr := GlobalLock(Data);   
      try  
        Move(Pointer(Str)^, DataPtr^, Size);   
        Clipboard.SetAsHandle(CF_UNICODETEXT, Data);   
      finally  
        GlobalUnlock(Data);   
      end;   
    except  
      GlobalFree(Data);   
      raise;   
    end;   
  end;   
end;

end.
