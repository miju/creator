
{*****************************************************************}
{                                                                 }
{                        XML Data Binding                         }
{                                                                 }
{         Generated on: 31.07.2007 9:33:35                        }
{       Generated from: F:\Temp_user\TEMP\!IUP\New\Scenario.xml   }
{   Settings stored in: F:\Temp_user\TEMP\!IUP\New\Scenario.xdb   }
{                                                                 }
{*****************************************************************}

unit Scenario2;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLSCENARIOType = interface;
  IXMLITEMType = interface;
  IXMLITEMTypeList = interface;
  IXMLINSTALLType = interface;
  IXMLBACK_UPType = interface;
  IXMLRETURNType = interface;
  IXMLString_List = interface;

{ IXMLSCENARIOType }

  IXMLSCENARIOType = interface(IXMLNode)
    ['{5A2BA037-3CAE-4FF7-9643-871E40BB63CE}']
    { Property Accessors }
    function Get_DATE: WideString;
    function Get_AUTHOR: WideString;
    function Get_CONTACT: WideString;
    function Get_ITEM: IXMLITEMTypeList;
    procedure Set_DATE(Value: WideString);
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_CONTACT(Value: WideString);
    { Methods & Properties }
    property DATE: WideString read Get_DATE write Set_DATE;
    property AUTHOR: WideString read Get_AUTHOR write Set_AUTHOR;
    property CONTACT: WideString read Get_CONTACT write Set_CONTACT;
    property ITEM: IXMLITEMTypeList read Get_ITEM;
  end;

{ IXMLITEMType }

  IXMLITEMType = interface(IXMLNode)
    ['{59D7EF65-8AF8-40D0-8900-03508D4E61E7}']
    { Property Accessors }
    function Get_EXT: WideString;
    function Get_INSTALL: IXMLINSTALLType;
    function Get_BACK_UP: IXMLBACK_UPType;
    function Get_RETURN: IXMLRETURNType;
    function Get_Description: WideString;
    procedure Set_EXT(Value: WideString);
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property EXT: WideString read Get_EXT write Set_EXT;
    property INSTALL: IXMLINSTALLType read Get_INSTALL;
    property BACK_UP: IXMLBACK_UPType read Get_BACK_UP;
    property RETURN: IXMLRETURNType read Get_RETURN;
    property Description: WideString read Get_Description write Set_Description;
  end;

{ IXMLITEMTypeList }

  IXMLITEMTypeList = interface(IXMLNodeCollection)
    ['{088E6FED-D097-4081-A4E3-1032FC7E673A}']
    { Methods & Properties }
    function Add: IXMLITEMType;
    function Insert(const Index: Integer): IXMLITEMType;
    function Get_Item(Index: Integer): IXMLITEMType;
    property Items[Index: Integer]: IXMLITEMType read Get_Item; default;
  end;

{ IXMLINSTALLType }

  IXMLINSTALLType = interface(IXMLNode)
    ['{7476DE8F-9993-4E0D-ADA8-74DADBDE9E36}']
    { Property Accessors }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    function Get_IS_SUCCSSFL: WideString;
    procedure Set_NUM(Value: Integer);
    procedure Set_IS_SUCCSSFL(Value: WideString);
    { Methods & Properties }
    property NUM: Integer read Get_NUM write Set_NUM;
    property OPERATION: IXMLString_List read Get_OPERATION;
    property IS_SUCCSSFL: WideString read Get_IS_SUCCSSFL write Set_IS_SUCCSSFL;
  end;

{ IXMLBACK_UPType }

  IXMLBACK_UPType = interface(IXMLNode)
    ['{877BE728-1DD3-4A02-BCE1-50AD3CD24813}']
    { Property Accessors }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
    { Methods & Properties }
    property NUM: Integer read Get_NUM write Set_NUM;
    property OPERATION: IXMLString_List read Get_OPERATION;
  end;

{ IXMLRETURNType }

  IXMLRETURNType = interface(IXMLNode)
    ['{652C36AA-FA79-4699-AED2-530E9DB88714}']
    { Property Accessors }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
    { Methods & Properties }
    property NUM: Integer read Get_NUM write Set_NUM;
    property OPERATION: IXMLString_List read Get_OPERATION;
  end;

{ IXMLString_List }

  IXMLString_List = interface(IXMLNodeCollection)
    ['{F4F1A7E9-C4F4-4E9D-ADC2-984DA5B72E4B}']
    { Methods & Properties }
    function Add(const Value: WideString): IXMLNode;
    function Insert(const Index: Integer; const Value: WideString): IXMLNode;
    function Get_Item(Index: Integer): WideString;
    property Items[Index: Integer]: WideString read Get_Item; default;
  end;

{ Forward Decls }

  TXMLSCENARIOType = class;
  TXMLITEMType = class;
  TXMLITEMTypeList = class;
  TXMLINSTALLType = class;
  TXMLBACK_UPType = class;
  TXMLRETURNType = class;
  TXMLString_List = class;

{ TXMLSCENARIOType }

  TXMLSCENARIOType = class(TXMLNode, IXMLSCENARIOType)
  private
    FITEM: IXMLITEMTypeList;
  protected
    { IXMLSCENARIOType }
    function Get_DATE: WideString;
    function Get_AUTHOR: WideString;
    function Get_CONTACT: WideString;
    function Get_ITEM: IXMLITEMTypeList;
    procedure Set_DATE(Value: WideString);
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_CONTACT(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLITEMType }

  TXMLITEMType = class(TXMLNode, IXMLITEMType)
  protected
    { IXMLITEMType }
    function Get_EXT: WideString;
    function Get_INSTALL: IXMLINSTALLType;
    function Get_BACK_UP: IXMLBACK_UPType;
    function Get_RETURN: IXMLRETURNType;
    function Get_Description: WideString;
    procedure Set_EXT(Value: WideString);
    procedure Set_Description(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLITEMTypeList }

  TXMLITEMTypeList = class(TXMLNodeCollection, IXMLITEMTypeList)
  protected
    { IXMLITEMTypeList }
    function Add: IXMLITEMType;
    function Insert(const Index: Integer): IXMLITEMType;
    function Get_Item(Index: Integer): IXMLITEMType;
  end;

{ TXMLINSTALLType }

  TXMLINSTALLType = class(TXMLNode, IXMLINSTALLType)
  private
    FOPERATION: IXMLString_List;
  protected
    { IXMLINSTALLType }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    function Get_IS_SUCCSSFL: WideString;
    procedure Set_NUM(Value: Integer);
    procedure Set_IS_SUCCSSFL(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBACK_UPType }

  TXMLBACK_UPType = class(TXMLNode, IXMLBACK_UPType)
  private
    FOPERATION: IXMLString_List;
  protected
    { IXMLBACK_UPType }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRETURNType }

  TXMLRETURNType = class(TXMLNode, IXMLRETURNType)
  private
    FOPERATION: IXMLString_List;
  protected
    { IXMLRETURNType }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLString_List }

  TXMLString_List = class(TXMLNodeCollection, IXMLString_List)
  protected
    { IXMLString_List }
    function Add(const Value: WideString): IXMLNode;
    function Insert(const Index: Integer; const Value: WideString): IXMLNode;
    function Get_Item(Index: Integer): WideString;
  end;

{ Global Functions }

function GetSCENARIO(Doc: IXMLDocument): IXMLSCENARIOType;
function LoadSCENARIO(const FileName: WideString): IXMLSCENARIOType;
function NewSCENARIO: IXMLSCENARIOType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetSCENARIO(Doc: IXMLDocument): IXMLSCENARIOType;
begin
  Result := Doc.GetDocBinding('SCENARIO', TXMLSCENARIOType, TargetNamespace) as IXMLSCENARIOType;
end;

function LoadSCENARIO(const FileName: WideString): IXMLSCENARIOType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('SCENARIO', TXMLSCENARIOType, TargetNamespace) as IXMLSCENARIOType;
end;

function NewSCENARIO: IXMLSCENARIOType;
begin
  Result := NewXMLDocument.GetDocBinding('SCENARIO', TXMLSCENARIOType, TargetNamespace) as IXMLSCENARIOType;
end;

{ TXMLSCENARIOType }

procedure TXMLSCENARIOType.AfterConstruction;
begin
  RegisterChildNode('ITEM', TXMLITEMType);
  FITEM := CreateCollection(TXMLITEMTypeList, IXMLITEMType, 'ITEM') as IXMLITEMTypeList;
  inherited;
end;

function TXMLSCENARIOType.Get_DATE: WideString;
begin
  Result := ChildNodes['DATE'].Text;
end;

procedure TXMLSCENARIOType.Set_DATE(Value: WideString);
begin
  ChildNodes['DATE'].NodeValue := Value;
end;

function TXMLSCENARIOType.Get_AUTHOR: WideString;
begin
  Result := ChildNodes['AUTHOR'].Text;
end;

procedure TXMLSCENARIOType.Set_AUTHOR(Value: WideString);
begin
  ChildNodes['AUTHOR'].NodeValue := Value;
end;

function TXMLSCENARIOType.Get_CONTACT: WideString;
begin
  Result := ChildNodes['CONTACT'].Text;
end;

procedure TXMLSCENARIOType.Set_CONTACT(Value: WideString);
begin
  ChildNodes['CONTACT'].NodeValue := Value;
end;

function TXMLSCENARIOType.Get_ITEM: IXMLITEMTypeList;
begin
  Result := FITEM;
end;

{ TXMLITEMType }

procedure TXMLITEMType.AfterConstruction;
begin
  RegisterChildNode('INSTALL', TXMLINSTALLType);
  RegisterChildNode('BACK_UP', TXMLBACK_UPType);
  RegisterChildNode('RETURN', TXMLRETURNType);
  inherited;
end;

function TXMLITEMType.Get_EXT: WideString;
begin
  Result := ChildNodes['EXT'].Text;
end;

procedure TXMLITEMType.Set_EXT(Value: WideString);
begin
  ChildNodes['EXT'].NodeValue := Value;
end;

function TXMLITEMType.Get_INSTALL: IXMLINSTALLType;
begin
  Result := ChildNodes['INSTALL'] as IXMLINSTALLType;
end;

function TXMLITEMType.Get_BACK_UP: IXMLBACK_UPType;
begin
  Result := ChildNodes['BACK_UP'] as IXMLBACK_UPType;
end;

function TXMLITEMType.Get_RETURN: IXMLRETURNType;
begin
  Result := ChildNodes['RETURN'] as IXMLRETURNType;
end;

function TXMLITEMType.Get_Description: WideString;
begin
  Result := ChildNodes['DESCRIPTION'].Text;
end;

procedure TXMLITEMType.Set_Description(Value: WideString);
begin
  ChildNodes['DESCRIPTION'].NodeValue := Value;
end;

{ TXMLITEMTypeList }

function TXMLITEMTypeList.Add: IXMLITEMType;
begin
  Result := AddItem(-1) as IXMLITEMType;
end;

function TXMLITEMTypeList.Insert(const Index: Integer): IXMLITEMType;
begin
  Result := AddItem(Index) as IXMLITEMType;
end;
function TXMLITEMTypeList.Get_Item(Index: Integer): IXMLITEMType;
begin
  Result := List[Index] as IXMLITEMType;
end;

{ TXMLINSTALLType }

procedure TXMLINSTALLType.AfterConstruction;
begin
  FOPERATION := CreateCollection(TXMLString_List, IXMLNode, 'OPERATION') as IXMLString_List;
  inherited;
end;

function TXMLINSTALLType.Get_NUM: Integer;
begin
  Result := ChildNodes['NUM'].NodeValue;
end;

procedure TXMLINSTALLType.Set_NUM(Value: Integer);
begin
  ChildNodes['NUM'].NodeValue := Value;
end;

function TXMLINSTALLType.Get_OPERATION: IXMLString_List;
begin
  Result := FOPERATION;
end;

function TXMLINSTALLType.Get_IS_SUCCSSFL: WideString;
begin
  Result := ChildNodes['IS_SUCCSSFL'].Text;
end;

procedure TXMLINSTALLType.Set_IS_SUCCSSFL(Value: WideString);
begin
  ChildNodes['IS_SUCCSSFL'].NodeValue := Value;
end;

{ TXMLBACK_UPType }

procedure TXMLBACK_UPType.AfterConstruction;
begin
  FOPERATION := CreateCollection(TXMLString_List, IXMLNode, 'OPERATION') as IXMLString_List;
  inherited;
end;

function TXMLBACK_UPType.Get_NUM: Integer;
begin
  Result := ChildNodes['NUM'].NodeValue;
end;

procedure TXMLBACK_UPType.Set_NUM(Value: Integer);
begin
  ChildNodes['NUM'].NodeValue := Value;
end;

function TXMLBACK_UPType.Get_OPERATION: IXMLString_List;
begin
  Result := FOPERATION;
end;

{ TXMLRETURNType }

procedure TXMLRETURNType.AfterConstruction;
begin
  FOPERATION := CreateCollection(TXMLString_List, IXMLNode, 'OPERATION') as IXMLString_List;
  inherited;
end;

function TXMLRETURNType.Get_NUM: Integer;
begin
  Result := ChildNodes['NUM'].NodeValue;
end;

procedure TXMLRETURNType.Set_NUM(Value: Integer);
begin
  ChildNodes['NUM'].NodeValue := Value;
end;

function TXMLRETURNType.Get_OPERATION: IXMLString_List;
begin
  Result := FOPERATION;
end;

{ TXMLString_List }

function TXMLString_List.Add(const Value: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLString_List.Insert(const Index: Integer; const Value: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;
function TXMLString_List.Get_Item(Index: Integer): WideString;
begin
  Result := List[Index].NodeValue;
end;

end.