object ExtInstForm: TExtInstForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'ExtInstForm'
  ClientHeight = 445
  ClientWidth = 694
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 54
    Height = 13
    Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072
  end
  object Label2: TLabel
    Left = 8
    Top = 143
    Width = 162
    Height = 13
    Caption = #1056#1077#1079#1077#1088#1074#1085#1086#1077' '#1082#1086#1087#1080#1088#1086#1074#1072#1085#1080#1077' ('#1073#1101#1082#1072#1087')'
  end
  object Label3: TLabel
    Left = 8
    Top = 278
    Width = 82
    Height = 13
    Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1077
  end
  object Label4: TLabel
    Left = 8
    Top = 420
    Width = 127
    Height = 13
    Caption = #1055#1086#1076#1075#1088#1091#1079#1080#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1076#1083#1103' '
  end
  object ScenPathLblLbl: TLabel
    Left = 80
    Top = 8
    Width = 3
    Height = 13
    Visible = False
  end
  object CloseBtn: TButton
    Left = 609
    Top = 413
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 2
    TabOrder = 4
    OnKeyPress = CloseBtnKeyPress
  end
  object SaveBtn: TButton
    Left = 528
    Top = 413
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object InstallMemo: TMemo
    Left = 8
    Top = 27
    Width = 678
    Height = 110
    TabOrder = 0
  end
  object BackupMemo: TMemo
    Left = 8
    Top = 162
    Width = 678
    Height = 110
    TabOrder = 1
  end
  object ReturnMemo: TMemo
    Left = 8
    Top = 297
    Width = 678
    Height = 110
    TabOrder = 2
  end
  object ExtsCB: TComboBox
    Left = 141
    Top = 416
    Width = 100
    Height = 21
    Style = csDropDownList
    Color = clBtnFace
    ItemHeight = 13
    Sorted = True
    TabOrder = 5
    OnSelect = ExtsCBSelect
  end
  object LoadScenarioDataBtn: TButton
    Left = 247
    Top = 415
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 6
    OnClick = LoadScenarioDataBtnClick
  end
  object ClearBtn: TButton
    Left = 328
    Top = 415
    Width = 113
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1074#1089#1077' '#1087#1086#1083#1103
    TabOrder = 7
    OnClick = ClearBtnClick
  end
end
