
{*************************************************************************************************************}
{                                                                                                             }
{                                              XML Data Binding                                               }
{                                                                                                             }
{         Generated on: 21.04.2011 16:38:18                                                                   }
{       Generated from: z:\Projects\�����������_����������\dev\!Modificator\v1.6.2_DB_back_up\UpdateXML.xml   }
{   Settings stored in: z:\Projects\�����������_����������\dev\!Modificator\v1.6.2_DB_back_up\UpdateXML.xdb   }
{                                                                                                             }
{*************************************************************************************************************}

unit UpdateXML;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUPDATEType = interface;
  IXMLREQUIREMENTSType = interface;
  IXMLCOMPILE_FORMSType = interface;
  IXMLITEMType = interface;
  IXMLITEMTypeList = interface;
  IXMLEXTINSType = interface;
  IXMLINSTALLType = interface;
  IXMLBACK_UPType = interface;
  IXMLRETURNType = interface;
  IXMLString_List = interface;

{ IXMLUPDATEType }

  IXMLUPDATEType = interface(IXMLNode)
    ['{F5767D09-7C0C-401F-A1B6-86FAF7C407CD}']
    { Property Accessors }
    function Get_DATE: WideString;
    function Get_AUTHOR: WideString;
    function Get_EMAIL: WideString;
    function Get_PHONE: WideString;
    function Get_VERSION: WideString;
    function Get_REQUIREMENTS: IXMLREQUIREMENTSType;
    function Get_UPDATE_NAME: WideString;
    function Get_DESCRIPTION: WideString;
    function Get_COMPILE_FORMS: IXMLCOMPILE_FORMSType;
    function Get_ITEM: IXMLITEMTypeList;
    procedure Set_DATE(Value: WideString);
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_EMAIL(Value: WideString);
    procedure Set_PHONE(Value: WideString);
    procedure Set_VERSION(Value: WideString);
    procedure Set_UPDATE_NAME(Value: WideString);
    procedure Set_DESCRIPTION(Value: WideString);
    { Methods & Properties }
    property DATE: WideString read Get_DATE write Set_DATE;
    property AUTHOR: WideString read Get_AUTHOR write Set_AUTHOR;
    property EMAIL: WideString read Get_EMAIL write Set_EMAIL;
    property PHONE: WideString read Get_PHONE write Set_PHONE;
    property VERSION: WideString read Get_VERSION write Set_VERSION;
    property REQUIREMENTS: IXMLREQUIREMENTSType read Get_REQUIREMENTS;
    property UPDATE_NAME: WideString read Get_UPDATE_NAME write Set_UPDATE_NAME;
    property DESCRIPTION: WideString read Get_DESCRIPTION write Set_DESCRIPTION;
    property COMPILE_FORMS: IXMLCOMPILE_FORMSType read Get_COMPILE_FORMS;
    property ITEM: IXMLITEMTypeList read Get_ITEM;
  end;

{ IXMLREQUIREMENTSType }

  IXMLREQUIREMENTSType = interface(IXMLNodeCollection)
    ['{A8C7D86A-FA01-440E-843E-406E379C9116}']
    { Property Accessors }
    function Get_REQ(Index: Integer): WideString;
    { Methods & Properties }
    function Add(const REQ: WideString): IXMLNode;
    function Insert(const Index: Integer; const REQ: WideString): IXMLNode;
    property REQ[Index: Integer]: WideString read Get_REQ; default;
  end;

{ IXMLCOMPILE_FORMSType }

  IXMLCOMPILE_FORMSType = interface(IXMLNodeCollection)
    ['{C5E14783-DE49-467F-B0FC-14CE2B165696}']
    { Property Accessors }
    function Get_FORM(Index: Integer): WideString;
    { Methods & Properties }
    function Add(const FORM: WideString): IXMLNode;
    function Insert(const Index: Integer; const FORM: WideString): IXMLNode;
    property FORM[Index: Integer]: WideString read Get_FORM; default;
  end;

{ IXMLITEMType }

  IXMLITEMType = interface(IXMLNode)
    ['{A1AAEC22-32A0-40B2-9D38-0C265354D100}']
    { Property Accessors }
    function Get_SEQ: Integer;
    function Get_OBJECT_TYPE: WideString;
    function Get_FILE_NAME: WideString;
    function Get_OBJECT_NAME: WideString;
    function Get_DESCRIPTION: WideString;
    function Get_EXTINS: IXMLEXTINSType;
    procedure Set_SEQ(Value: Integer);
    procedure Set_OBJECT_TYPE(Value: WideString);
    procedure Set_FILE_NAME(Value: WideString);
    procedure Set_OBJECT_NAME(Value: WideString);
    procedure Set_DESCRIPTION(Value: WideString);
    { Methods & Properties }
    property SEQ: Integer read Get_SEQ write Set_SEQ;
    property OBJECT_TYPE: WideString read Get_OBJECT_TYPE write Set_OBJECT_TYPE;
    property FILE_NAME: WideString read Get_FILE_NAME write Set_FILE_NAME;
    property OBJECT_NAME: WideString read Get_OBJECT_NAME write Set_OBJECT_NAME;
    property DESCRIPTION: WideString read Get_DESCRIPTION write Set_DESCRIPTION;
    property EXTINS: IXMLEXTINSType read Get_EXTINS;
  end;

{ IXMLITEMTypeList }

  IXMLITEMTypeList = interface(IXMLNodeCollection)
    ['{EECB0FBD-2F2C-40E4-AAD9-761DC64113CA}']
    { Methods & Properties }
    function Add: IXMLITEMType;
    function Insert(const Index: Integer): IXMLITEMType;
    function Get_Item(Index: Integer): IXMLITEMType;
    property Items[Index: Integer]: IXMLITEMType read Get_Item; default;
  end;

{ IXMLEXTINSType }

  IXMLEXTINSType = interface(IXMLNode)
    ['{16F12BAC-6E66-46A1-95E6-3D4820DA816E}']
    { Property Accessors }
    function Get_INSTALL: IXMLINSTALLType;
    function Get_BACK_UP: IXMLBACK_UPType;
    function Get_RETURN: IXMLRETURNType;
    { Methods & Properties }
    property INSTALL: IXMLINSTALLType read Get_INSTALL;
    property BACK_UP: IXMLBACK_UPType read Get_BACK_UP;
    property RETURN: IXMLRETURNType read Get_RETURN;
  end;

{ IXMLINSTALLType }

  IXMLINSTALLType = interface(IXMLNode)
    ['{061B133C-A8A0-44A5-9DCF-7FD5AFA3C9DE}']
    { Property Accessors }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
    { Methods & Properties }
    property NUM: Integer read Get_NUM write Set_NUM;
    property OPERATION: IXMLString_List read Get_OPERATION;
  end;

{ IXMLBACK_UPType }

  IXMLBACK_UPType = interface(IXMLNode)
    ['{CA99EF78-8FFC-485D-8187-7673976E762B}']
    { Property Accessors }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
    { Methods & Properties }
    property NUM: Integer read Get_NUM write Set_NUM;
    property OPERATION: IXMLString_List read Get_OPERATION;
  end;

{ IXMLRETURNType }

  IXMLRETURNType = interface(IXMLNode)
    ['{EB45B034-9971-4531-B6A1-487B72D60254}']
    { Property Accessors }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
    { Methods & Properties }
    property NUM: Integer read Get_NUM write Set_NUM;
    property OPERATION: IXMLString_List read Get_OPERATION;
  end;

{ IXMLString_List }

  IXMLString_List = interface(IXMLNodeCollection)
    ['{ED57C03A-276E-4B37-854C-C1F1C7633204}']
    { Methods & Properties }
    function Add(const Value: WideString): IXMLNode;
    function Insert(const Index: Integer; const Value: WideString): IXMLNode;
    function Get_Item(Index: Integer): WideString;
    property Items[Index: Integer]: WideString read Get_Item; default;
  end;

{ Forward Decls }

  TXMLUPDATEType = class;
  TXMLREQUIREMENTSType = class;
  TXMLCOMPILE_FORMSType = class;
  TXMLITEMType = class;
  TXMLITEMTypeList = class;
  TXMLEXTINSType = class;
  TXMLINSTALLType = class;
  TXMLBACK_UPType = class;
  TXMLRETURNType = class;
  TXMLString_List = class;

{ TXMLUPDATEType }

  TXMLUPDATEType = class(TXMLNode, IXMLUPDATEType)
  private
    FITEM: IXMLITEMTypeList;
  protected
    { IXMLUPDATEType }
    function Get_DATE: WideString;
    function Get_AUTHOR: WideString;
    function Get_EMAIL: WideString;
    function Get_PHONE: WideString;
    function Get_VERSION: WideString;
    function Get_REQUIREMENTS: IXMLREQUIREMENTSType;
    function Get_UPDATE_NAME: WideString;
    function Get_DESCRIPTION: WideString;
    function Get_COMPILE_FORMS: IXMLCOMPILE_FORMSType;
    function Get_ITEM: IXMLITEMTypeList;
    procedure Set_DATE(Value: WideString);
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_EMAIL(Value: WideString);
    procedure Set_PHONE(Value: WideString);
    procedure Set_VERSION(Value: WideString);
    procedure Set_UPDATE_NAME(Value: WideString);
    procedure Set_DESCRIPTION(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLREQUIREMENTSType }

  TXMLREQUIREMENTSType = class(TXMLNodeCollection, IXMLREQUIREMENTSType)
  protected
    { IXMLREQUIREMENTSType }
    function Get_REQ(Index: Integer): WideString;
    function Add(const REQ: WideString): IXMLNode;
    function Insert(const Index: Integer; const REQ: WideString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCOMPILE_FORMSType }

  TXMLCOMPILE_FORMSType = class(TXMLNodeCollection, IXMLCOMPILE_FORMSType)
  protected
    { IXMLCOMPILE_FORMSType }
    function Get_FORM(Index: Integer): WideString;
    function Add(const FORM: WideString): IXMLNode;
    function Insert(const Index: Integer; const FORM: WideString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLITEMType }

  TXMLITEMType = class(TXMLNode, IXMLITEMType)
  protected
    { IXMLITEMType }
    function Get_SEQ: Integer;
    function Get_OBJECT_TYPE: WideString;
    function Get_FILE_NAME: WideString;
    function Get_OBJECT_NAME: WideString;
    function Get_DESCRIPTION: WideString;
    function Get_EXTINS: IXMLEXTINSType;
    procedure Set_SEQ(Value: Integer);
    procedure Set_OBJECT_TYPE(Value: WideString);
    procedure Set_FILE_NAME(Value: WideString);
    procedure Set_OBJECT_NAME(Value: WideString);
    procedure Set_DESCRIPTION(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLITEMTypeList }

  TXMLITEMTypeList = class(TXMLNodeCollection, IXMLITEMTypeList)
  protected
    { IXMLITEMTypeList }
    function Add: IXMLITEMType;
    function Insert(const Index: Integer): IXMLITEMType;
    function Get_Item(Index: Integer): IXMLITEMType;
  end;

{ TXMLEXTINSType }

  TXMLEXTINSType = class(TXMLNode, IXMLEXTINSType)
  protected
    { IXMLEXTINSType }
    function Get_INSTALL: IXMLINSTALLType;
    function Get_BACK_UP: IXMLBACK_UPType;
    function Get_RETURN: IXMLRETURNType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLINSTALLType }

  TXMLINSTALLType = class(TXMLNode, IXMLINSTALLType)
  private
    FOPERATION: IXMLString_List;
  protected
    { IXMLINSTALLType }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLBACK_UPType }

  TXMLBACK_UPType = class(TXMLNode, IXMLBACK_UPType)
  private
    FOPERATION: IXMLString_List;
  protected
    { IXMLBACK_UPType }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRETURNType }

  TXMLRETURNType = class(TXMLNode, IXMLRETURNType)
  private
    FOPERATION: IXMLString_List;
  protected
    { IXMLRETURNType }
    function Get_NUM: Integer;
    function Get_OPERATION: IXMLString_List;
    procedure Set_NUM(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLString_List }

  TXMLString_List = class(TXMLNodeCollection, IXMLString_List)
  protected
    { IXMLString_List }
    function Add(const Value: WideString): IXMLNode;
    function Insert(const Index: Integer; const Value: WideString): IXMLNode;
    function Get_Item(Index: Integer): WideString;
  end;

{ Global Functions }

function GetUPDATE(Doc: IXMLDocument): IXMLUPDATEType;
function LoadUPDATE(const FileName: WideString): IXMLUPDATEType;
function NewUPDATE: IXMLUPDATEType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetUPDATE(Doc: IXMLDocument): IXMLUPDATEType;
begin
  Result := Doc.GetDocBinding('UPDATE', TXMLUPDATEType, TargetNamespace) as IXMLUPDATEType;
end;

function LoadUPDATE(const FileName: WideString): IXMLUPDATEType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('UPDATE', TXMLUPDATEType, TargetNamespace) as IXMLUPDATEType;
end;

function NewUPDATE: IXMLUPDATEType;
begin
  Result := NewXMLDocument.GetDocBinding('UPDATE', TXMLUPDATEType, TargetNamespace) as IXMLUPDATEType;
end;

{ TXMLUPDATEType }

procedure TXMLUPDATEType.AfterConstruction;
begin
  RegisterChildNode('REQUIREMENTS', TXMLREQUIREMENTSType);
  RegisterChildNode('COMPILE_FORMS', TXMLCOMPILE_FORMSType);
  RegisterChildNode('ITEM', TXMLITEMType);
  FITEM := CreateCollection(TXMLITEMTypeList, IXMLITEMType, 'ITEM') as IXMLITEMTypeList;
  inherited;
end;

function TXMLUPDATEType.Get_DATE: WideString;
begin
  Result := ChildNodes['DATE'].Text;
end;

procedure TXMLUPDATEType.Set_DATE(Value: WideString);
begin
  ChildNodes['DATE'].NodeValue := Value;
end;

function TXMLUPDATEType.Get_AUTHOR: WideString;
begin
  Result := ChildNodes['AUTHOR'].Text;
end;

procedure TXMLUPDATEType.Set_AUTHOR(Value: WideString);
begin
  ChildNodes['AUTHOR'].NodeValue := Value;
end;

function TXMLUPDATEType.Get_EMAIL: WideString;
begin
  Result := ChildNodes['EMAIL'].Text;
end;

procedure TXMLUPDATEType.Set_EMAIL(Value: WideString);
begin
  ChildNodes['EMAIL'].NodeValue := Value;
end;

function TXMLUPDATEType.Get_PHONE: WideString;
begin
  Result := ChildNodes['PHONE'].Text;
end;

procedure TXMLUPDATEType.Set_PHONE(Value: WideString);
begin
  ChildNodes['PHONE'].NodeValue := Value;
end;

function TXMLUPDATEType.Get_VERSION: WideString;
begin
  Result := ChildNodes['VERSION'].Text;
end;

procedure TXMLUPDATEType.Set_VERSION(Value: WideString);
begin
  ChildNodes['VERSION'].NodeValue := Value;
end;

function TXMLUPDATEType.Get_REQUIREMENTS: IXMLREQUIREMENTSType;
begin
  Result := ChildNodes['REQUIREMENTS'] as IXMLREQUIREMENTSType;
end;

function TXMLUPDATEType.Get_UPDATE_NAME: WideString;
begin
  Result := ChildNodes['UPDATE_NAME'].Text;
end;

procedure TXMLUPDATEType.Set_UPDATE_NAME(Value: WideString);
begin
  ChildNodes['UPDATE_NAME'].NodeValue := Value;
end;

function TXMLUPDATEType.Get_DESCRIPTION: WideString;
begin
  Result := ChildNodes['DESCRIPTION'].Text;
end;

procedure TXMLUPDATEType.Set_DESCRIPTION(Value: WideString);
begin
  ChildNodes['DESCRIPTION'].NodeValue := Value;
end;

function TXMLUPDATEType.Get_COMPILE_FORMS: IXMLCOMPILE_FORMSType;
begin
  Result := ChildNodes['COMPILE_FORMS'] as IXMLCOMPILE_FORMSType;
end;

function TXMLUPDATEType.Get_ITEM: IXMLITEMTypeList;
begin
  Result := FITEM;
end;

{ TXMLREQUIREMENTSType }

procedure TXMLREQUIREMENTSType.AfterConstruction;
begin
  ItemTag := 'REQ';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLREQUIREMENTSType.Get_REQ(Index: Integer): WideString;
begin
  Result := List[Index].Text;
end;

function TXMLREQUIREMENTSType.Add(const REQ: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := REQ;
end;

function TXMLREQUIREMENTSType.Insert(const Index: Integer; const REQ: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := REQ;
end;

{ TXMLCOMPILE_FORMSType }

procedure TXMLCOMPILE_FORMSType.AfterConstruction;
begin
  ItemTag := 'FORM';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLCOMPILE_FORMSType.Get_FORM(Index: Integer): WideString;
begin
  Result := List[Index].Text;
end;

function TXMLCOMPILE_FORMSType.Add(const FORM: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := FORM;
end;

function TXMLCOMPILE_FORMSType.Insert(const Index: Integer; const FORM: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := FORM;
end;

{ TXMLITEMType }

procedure TXMLITEMType.AfterConstruction;
begin
  RegisterChildNode('EXTINS', TXMLEXTINSType);
  inherited;
end;

function TXMLITEMType.Get_SEQ: Integer;
begin
  Result := ChildNodes['SEQ'].NodeValue;
end;

procedure TXMLITEMType.Set_SEQ(Value: Integer);
begin
  ChildNodes['SEQ'].NodeValue := Value;
end;

function TXMLITEMType.Get_OBJECT_TYPE: WideString;
begin
  Result := ChildNodes['OBJECT_TYPE'].Text;
end;

procedure TXMLITEMType.Set_OBJECT_TYPE(Value: WideString);
begin
  ChildNodes['OBJECT_TYPE'].NodeValue := Value;
end;

function TXMLITEMType.Get_FILE_NAME: WideString;
begin
  Result := ChildNodes['FILE_NAME'].Text;
end;

procedure TXMLITEMType.Set_FILE_NAME(Value: WideString);
begin
  ChildNodes['FILE_NAME'].NodeValue := Value;
end;

function TXMLITEMType.Get_OBJECT_NAME: WideString;
begin
  Result := ChildNodes['OBJECT_NAME'].Text;
end;

procedure TXMLITEMType.Set_OBJECT_NAME(Value: WideString);
begin
  ChildNodes['OBJECT_NAME'].NodeValue := Value;
end;

function TXMLITEMType.Get_DESCRIPTION: WideString;
begin
  Result := ChildNodes['DESCRIPTION'].Text;
end;

procedure TXMLITEMType.Set_DESCRIPTION(Value: WideString);
begin
  ChildNodes['DESCRIPTION'].NodeValue := Value;
end;

function TXMLITEMType.Get_EXTINS: IXMLEXTINSType;
begin
  Result := ChildNodes['EXTINS'] as IXMLEXTINSType;
end;

{ TXMLITEMTypeList }

function TXMLITEMTypeList.Add: IXMLITEMType;
begin
  Result := AddItem(-1) as IXMLITEMType;
end;

function TXMLITEMTypeList.Insert(const Index: Integer): IXMLITEMType;
begin
  Result := AddItem(Index) as IXMLITEMType;
end;
function TXMLITEMTypeList.Get_Item(Index: Integer): IXMLITEMType;
begin
  Result := List[Index] as IXMLITEMType;
end;

{ TXMLEXTINSType }

procedure TXMLEXTINSType.AfterConstruction;
begin
  RegisterChildNode('INSTALL', TXMLINSTALLType);
  RegisterChildNode('BACK_UP', TXMLBACK_UPType);
  RegisterChildNode('RETURN', TXMLRETURNType);
  inherited;
end;

function TXMLEXTINSType.Get_INSTALL: IXMLINSTALLType;
begin
  Result := ChildNodes['INSTALL'] as IXMLINSTALLType;
end;

function TXMLEXTINSType.Get_BACK_UP: IXMLBACK_UPType;
begin
  Result := ChildNodes['BACK_UP'] as IXMLBACK_UPType;
end;

function TXMLEXTINSType.Get_RETURN: IXMLRETURNType;
begin
  Result := ChildNodes['RETURN'] as IXMLRETURNType;
end;

{ TXMLINSTALLType }

procedure TXMLINSTALLType.AfterConstruction;
begin
  FOPERATION := CreateCollection(TXMLString_List, IXMLNode, 'OPERATION') as IXMLString_List;
  inherited;
end;

function TXMLINSTALLType.Get_NUM: Integer;
begin
  Result := ChildNodes['NUM'].NodeValue;
end;

procedure TXMLINSTALLType.Set_NUM(Value: Integer);
begin
  ChildNodes['NUM'].NodeValue := Value;
end;

function TXMLINSTALLType.Get_OPERATION: IXMLString_List;
begin
  Result := FOPERATION;
end;

{ TXMLBACK_UPType }

procedure TXMLBACK_UPType.AfterConstruction;
begin
  FOPERATION := CreateCollection(TXMLString_List, IXMLNode, 'OPERATION') as IXMLString_List;
  inherited;
end;

function TXMLBACK_UPType.Get_NUM: Integer;
begin
  Result := ChildNodes['NUM'].NodeValue;
end;

procedure TXMLBACK_UPType.Set_NUM(Value: Integer);
begin
  ChildNodes['NUM'].NodeValue := Value;
end;

function TXMLBACK_UPType.Get_OPERATION: IXMLString_List;
begin
  Result := FOPERATION;
end;

{ TXMLRETURNType }

procedure TXMLRETURNType.AfterConstruction;
begin
  FOPERATION := CreateCollection(TXMLString_List, IXMLNode, 'OPERATION') as IXMLString_List;
  inherited;
end;

function TXMLRETURNType.Get_NUM: Integer;
begin
  Result := ChildNodes['NUM'].NodeValue;
end;

procedure TXMLRETURNType.Set_NUM(Value: Integer);
begin
  ChildNodes['NUM'].NodeValue := Value;
end;

function TXMLRETURNType.Get_OPERATION: IXMLString_List;
begin
  Result := FOPERATION;
end;

{ TXMLString_List }

function TXMLString_List.Add(const Value: WideString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLString_List.Insert(const Index: Integer; const Value: WideString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;
function TXMLString_List.Get_Item(Index: Integer): WideString;
begin
  Result := List[Index].NodeValue;
end;

end.