unit Journal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, jpeg, Oracle, Grids, ValEdit, ExtCtrls, Lib;

type
  TJournalForm = class(TForm)
    osJournal: TOracleSession;
    olJournal: TOracleLogon;
    opJournal: TOraclePackage;
    mLog: TMemo;
    oqClients: TOracleQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure OraLogon;
    function  OraLogon(): Boolean;
    procedure OraLogoff;
    procedure FillAuthorsList(CB_: TComboBox);
    procedure FillCompsList(CB_: TComboBox);
    procedure FillClientsList(CB_: TComboBox);
  private
    { Private declarations }
    function  CheckIUPinJournal(IUPName_, IUPVers_: string): Boolean;
    function JournalInsert(NAME_        :string;   // ��� ����������
                                    FILE_NAME_   :string;   // ��� ����� ����������
                                    CRT_DATE_    :string;   // ���� ����������
                                    DESCRIPTION_ :string;   // ��������
                                    VERSION_     :string;   // ��������
                                    AUTHOR_      :integer = -1;  // ����� ����������
                                    AUTHOR_IN_   :integer = -1;  // ����� ������ � ������� ����������
                                    DEPT_        :integer = -1;        // ���������� ���, � ������� ��������� ������ ����������
                                    CLIENT_ID_   :integer = -1;        // ��������
                                    ACTION_      :string = 'add'; // ��� ������ ����� ������� � ����������� � ������ ����������
                                    IS_IUP_      :integer = 1): string;    // ������ �� ���� ����������

  public
    function  JournalProc(NAME_: string; FILE_NAME_ :string; CRT_DATE_: string;
                          DESCRIPTION_: string; VERSION_ :string; AUTHOR_: integer; AUTHOR_IN_ :integer;
                          DEPT_: integer; CLIENT_ID_: integer; ACTION_: string = 'add';
                          IS_IUP_:integer = 1): string;
    { Public declarations }
  end;

var
  JournalForm: TJournalForm;

implementation
uses main;
{$R *.dfm}

procedure TJournalForm.FillCompsList(CB_: TComboBox);
{
  ������������� ���������� ������ "������ ���"
}
var
  oqComponents: TOracleQuery;
begin

//  OraLogon; // ���������

  oqComponents := TOracleQuery.Create(self);
  with oqComponents do begin
    Session := osJournal;
    Optimize := False;
    try
      Clear;
      SQL.Add('select D.id ID, D.name NAME');
      SQL.Add('  from int_depts D');
      SQL.Add(' where D.status = 0');
      SQL.Add(' order by D.shortname');
      Execute;
      with CB_ do begin
        while not Eof do begin
          AddItem(String(Field('NAME')), TObject(integer(Field('ID'))));
          Next;
        end;
        if Items.Count > 0 then begin
          ItemIndex := 0;
          Tag := integer(Items.Objects[0]);
        end;
      end;

    finally
      Clear;
      FreeAndNil(oqComponents);
    end;
  end;
end;

procedure TJournalForm.FillAuthorsList(CB_: TComboBox);
{
  ������������� ���������� ������ "��������"
}
var
  oqComponents: TOracleQuery;
begin

//  OraLogon; // ���������

  oqComponents := TOracleQuery.Create(self);
  with oqComponents do begin
    Session := osJournal;
    Optimize := False;
    try
      Clear;
      SQL.Add('select P.id ID, P.lastname||'' ''|| P.name||'' ''|| P.patronymic NAME');
      SQL.Add('  from int_persons P');
      SQL.Add(' where P.status = 0');
      SQL.Add(' order by P.lastname');
      Execute;
      with CB_ do begin
        while not Eof do begin
          AddItem(String(Field('NAME')), TObject(integer(Field('ID'))));
          Next;
        end;
        if Items.Count > 0 then begin
          ItemIndex := 0;
          Tag := integer(Items.Objects[0]);
        end;
      end;

    finally
      Clear;
      FreeAndNil(oqComponents);
    end;
  end;
end;

procedure TJournalForm.FillClientsList(CB_: TComboBox);
{
  ������������� ���������� ������ "��������"
}
var
  oqComponents: TOracleQuery;
begin

//  OraLogon; // ���������

  oqComponents := TOracleQuery.Create(self);
  with oqComponents do begin
    Session := osJournal;
    Optimize := False;
    try
      Clear;
      SQL.Add('select C.id ID, C.name NAME');
      SQL.Add('  from int_clients C');
      SQL.Add(' where C.status = 0');
      SQL.Add(' order by C.name');
      Execute;
      with CB_ do begin
        while not Eof do begin
          AddItem(String(Field('NAME')), TObject(integer(Field('ID'))));
          Next;
        end;
        if Items.Count > 0 then begin
          ItemIndex := 0;
          Tag := integer(Items.Objects[0]);
        end;
      end;

    finally
      Clear;
      FreeAndNil(oqComponents);
    end;
  end;
end;

function TJournalForm.OraLogon(): Boolean;
{
  ����������� � ��.
}
begin
  try
    Result := True;
    if osJournal.Connected then begin // �� ����� ������������ ��� ���.
      exit;
    end;

    with olJournal do begin
      Options := Options + [ldLogonHistory];
      HistoryWithPassword := True;
      HistoryIniFile := ExtractFileDir(ParamStr(0)) + '\' + 'dbident.ini';
      Execute;
    end;
    if not osJournal.Connected then begin
      mLog.Lines.Add('�� ������� ������������ � �� ' + osJournal.LogonDatabase);
      Result := False;
    end
    else
      mLog.Lines.Add('������������ � �� ' + osJournal.LogonDatabase);
  except
    on e:Exception do begin
      Result := False;
      mLog.Lines.Add('��������� ������ ����������� � �� ' + osJournal.LogonDatabase + ': ' + #13#10 + e.Message);
    end;
  end;
end;

procedure TJournalForm.OraLogoff();
{
  ���������� �� ��
}
begin
  if osJournal.Connected then begin
    osJournal.LogOff;
  end;
  mLog.Lines.Add('����������� �� �� ' + osJournal.LogonDatabase);
end;

procedure TJournalForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  OraLogoff;
end;

procedure TJournalForm.FormCreate(Sender: TObject);
begin
  JournalForm.OraLogon;
  FillCompsList(IUPCreatorForm.cbComponent);
  FillClientsList(IUPCreatorForm.cbClient);
  FillAuthorsList(IUPCreatorForm.cbAuthor);
end;

function TJournalForm.JournalInsert(NAME_        :string;   // ��� ����������
                                    FILE_NAME_   :string;   // ��� ����� ����������
                                    CRT_DATE_    :string;   // ���� ����������
                                    DESCRIPTION_ :string;   // ��������
                                    VERSION_     :string;   // ������
                                    AUTHOR_      :integer = -1;  // ����� ����������
                                    AUTHOR_IN_   :integer = -1;  // ����� ������ � ������� ����������
                                    DEPT_        :integer = -1;        // ���������� ���, � ������� ��������� ������ ����������
                                    CLIENT_ID_   :integer = -1;        // ��������
                                    ACTION_      :string = 'add'; // ��� ������ ����� ������� � ����������� � ������ ����������
                                    IS_IUP_      :integer = 1): string;    // ������ �� ���� ����������
{
  ��������� �� ������� �� ����������. ������������ �������:
  "True."
  "False. $���������_��_������_Oracle"
}

  var PackName, FuncName: string;
      insresult: string;
begin
  PackName := 'interin.int_main'; // ��� ������ � ��
  FuncName := 'insert_update';      // ��� ������� � ������, ������� ������� ������ �� ���������� � ��

  with opJournal do begin
    try
      ParameterMode := pmNamed;  // �������� ����� �������� ���������� �� �����
      PackageName   := PackName;
      Optimize      := False;    // ��������� ����� "��������� ���, �� �� ������"

      DESCRIPTION_ := nvl((Length(DESCRIPTION_) = 0), '�������� ���������� �� �������', DESCRIPTION_);

      if ( (length(NAME_)        = 0) or
           (length(FILE_NAME_)   = 0) or
           (length(CRT_DATE_)    = 0) or
//           (length(DESCRIPTION_) = 0) or
           (length(VERSION_)     = 0) or
           (AUTHOR_              = -1) or
           (AUTHOR_IN_           = -1) or
           (DEPT_                = -1) or
           (CLIENT_ID_           = -1) or
           (length(ACTION_)      = 0)  or
           (IS_IUP_              = -1) )
      then begin
        mLog.Lines.Add('�� ������� ������ ��� �������� ������ � ������� ����������: ');
        mLog.Lines.Add('��� ���������� (NAME_ ): ' + NAME_);
        mLog.Lines.Add('��� ����� ���������� (FILE_NAME_): ' + FILE_NAME_);
        mLog.Lines.Add('���� ���������� (CRT_DATE_ ): ' + CRT_DATE_);
        mLog.Lines.Add('�������� (DESCRIPTION_): ' + DESCRIPTION_);
        mLog.Lines.Add('������ (VERSION_): ' + VERSION_);
        mLog.Lines.Add('����� ���������� (AUTHOR_ ): ' + IntToStr(AUTHOR_));
        mLog.Lines.Add('����� ������ � ������� ���������� (AUTHOR_IN_): ' + IntToStr(AUTHOR_IN_));
        mLog.Lines.Add('���������� ���, � ������� ��������� ������ ���������� (DEPT_ ): ' + IntToStr(DEPT_));
        mLog.Lines.Add('�������� (CLIENT_ID_): ' + IntToStr(CLIENT_ID_));
        mLog.Lines.Add('��� ������ ����� ������� � ����������� � ������ ���������� (ACTION_ ): ' + ACTION_);
        mLog.Lines.Add('������ �� ���� ���������� (IS_IUP_ ): ' + IntToStr(IS_IUP_));
        Result := 'False. �� ������� ������ ��� �������� ������ � ������� ����������';
        exit;
      end;


      insresult := CallStringFunction(FuncName, ['NAME_',        UTF8Encode(NAME_),
                                                 'FILE_NAME_',   UTF8Encode(FILE_NAME_),
                                                 'CRT_DATE_',    UTF8Encode(CRT_DATE_),
                                                 'DESCRIPTION_', UTF8Encode(DESCRIPTION_),
                                                 'VERSION_',     UTF8Encode(VERSION_),
                                                 'AUTHOR_',      AUTHOR_,
                                                 'AUTHOR_IN_',   AUTHOR_IN_,
                                                 'IS_IUP_',      IS_IUP_,
                                                 'DEPT_',        DEPT_,
                                                 'ACTION_',      UTF8Encode(ACTION_),
                                                 'CLIENT_ID_',   CLIENT_ID_]);
      if ( Pos('True.', insresult) = 1 ) then begin
      { ��������� ��������� �� ������� }
        mLog.Lines.Add('���������� � "' + NAME_ + '" ��������� � ��.');
        mLog.Lines.Add('��������� ��������� � ��...');
        osJournal.Commit;
        mLog.Lines.Add('��������� � �� ���������.');
      end
      else begin
      { ���-�� ����� �� ��� ): }
        mLog.Lines.Add('�� ������� ��������� ���������� � "' + NAME_ + '" � ��. ��������� �� ������: ' + insresult);
      end;
      Result := insresult;
    except
      on E:EOracleError do begin
        ShowMessage('��������� ������ � �������� �������� ��������� � ��: ' + #13#10 + e.Message);
        Result := 'False.';
      end;
    end;
  end;
end;

function  TJournalForm.CheckIUPinJournal(IUPName_, IUPVers_: string): Boolean;
{
  ��������, ��� �� � ������� ���������� ������ �� IUP'�.
}
var
  oqUIPs: TOracleQuery;
  journal_rec: string;

  function FieldCheck(Field_: string): string;
  {
    ���� ��������-������ �� �����, �� ���������� � ��.
    ���� �����, �� ���������� ������ "<���� �� ���������>".
  }
  begin
    Result := NVL(Length(Trim(Field_)) = 0, '<���� �� ���������>', Field_);
  end;

begin
  journal_rec := '';
//  JournalForm.OraLogon; // ���������
  oqUIPs := TOracleQuery.Create(nil);
  with oqUIPs do begin
    Session := JournalForm.osJournal;
    Optimize := False;
//    Debug := True;
    Scrollable:=True;
    try
      Clear;
      DeclareAndSet('IUPNAME', otString, UTF8Encode(IUPName_));
      DeclareAndSet('IUPVERS', otString, UTF8Encode(IUPVers_));

      SQL.Add('  select U.version vers, U.name iup_name, P.lastname||'' ''||P.name auth, U.crt_date, U.description ds');
      SQL.Add('    from int_updates U, int_persons P');
      SQL.Add('   where P.id = U.author_id');
      SQL.Add('     and ( (U.name = :IUPNAME) or (U.version = :IUPVERS) )');
      SQL.Add('   order by U.crt_date asc');

      Execute;

      // ���� ���-�� � �� ����, �� ���������� �������� � ���������
      journal_rec := #13#10;
      First;
      while not Eof do begin
        journal_rec := journal_rec +
                       '������: '                  + FieldCheck(string(Field('vers')))     + #13#10 +
                       '������������ ����������: ' + FieldCheck(string(Field('iup_name'))) + #13#10 +
                       '����� ����������: '        + FieldCheck(string(Field('auth')))     + #13#10 +
                       '���� ��������: '           + FieldCheck(string(Field('crt_date'))) + #13#10 +
                       '��������: '                + FieldCheck(string(Field('ds'))) + #13#10;
        Next;
      end;

    except
      on E:EOracleError do begin
        if E.ErrorCode <> 01403 then begin // 01403 - NO_DATA_FOUND
          ShowMessage('��������� ������ � �������� ������� ������� ����������: ' + #13#10 + e.Message);
        end;
        FreeAndNil(oqUIPs);
        Result := False;
        Exit;               // ���� ������ �� ����� ��� ���-�� ���������, �� ������� ����� ��
      end;
    end;
  end;

  Result := (MessageDlg('� ������� ���������� ������� ������� ����������: ' + #13#10 +
                         journal_rec + #13#10 +
                        '�� ������ �������� ���������� � ����� ���������� � ������?',
                         mtConfirmation, [mbYes, mbNo], 0) = mrCancel);
  FreeAndNil(oqUIPs);
end;

function TJournalForm.JournalProc(NAME_        :string;   // ��� ����������
                                  FILE_NAME_   :string;   // ��� ����� ����������
                                  CRT_DATE_    :string;   // ���� ����������
                                  DESCRIPTION_ :string;   // ��������
                                  VERSION_     :string;   // ������
                                  AUTHOR_      :integer;  // ����� ����������
                                  AUTHOR_IN_   :integer;  // ����� ������ � ������� ����������
                                  DEPT_        :integer;        // ���������� ���, � ������� ��������� ������ ����������
                                  CLIENT_ID_   :integer;        // ��������
                                  ACTION_      :string = 'add'; // ��� ������ ����� ������� � ����������� � ������ ����������
                                  IS_IUP_      :integer = 1): string;   // ������ �� ���� ����������
{
  ���������� ����, ������������ � ��, ������ ������, ����������� �� ��, �������� ����.
}
begin
  JournalForm.Show;
  mLog.Lines.Add(DateTimeToStr(Now));
//  OraLogon;
  if not CheckIUPinJournal(NAME_ , VERSION_) then begin
    Result := 'False. ������ ���������� ��� ������������ � �� ���� �� ���������� ���������� ���������� � ��� � ������.';
    JournalForm.Hide;
    exit;
  end;

  Result := JournalInsert(NAME_,FILE_NAME_, CRT_DATE_, DESCRIPTION_, VERSION_, AUTHOR_,
                          AUTHOR_IN_, DEPT_, CLIENT_ID_, ACTION_, IS_IUP_);
  OraLogoff;
  mLog.Lines.Add(DateTimeToStr(Now));
  JournalForm.Hide;
end;

end.
