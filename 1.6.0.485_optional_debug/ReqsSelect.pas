unit ReqsSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VirtualTrees, StdCtrls, CheckLst, JvExCheckLst, JvCheckListBox, Journal, Oracle,
  Grids, ValEdit, JvExGrids, JvStringGrid;

type
  TReqSelectForm = class(TForm)
    btnClose: TButton;
    edSearch: TEdit;
    Label1: TLabel;
    cbShowQuery: TCheckBox;
    jvsgJournalIUPs: TJvStringGrid;
    procedure jvsgJournalIUPsCaptionClick(Sender: TJvStringGrid; AColumn,
      ARow: Integer);
    procedure edSearchChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillIUPsListFromDB(Filter_: string = '');
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReqSelectForm: TReqSelectForm;

implementation

{$R *.dfm}

procedure TReqSelectForm.edSearchChange(Sender: TObject);
begin
  FillIUPsListFromDB(edSearch.Text);
end;

procedure TReqSelectForm.FillIUPsListFromDB(Filter_: string = '');
{
  ������ ���������� ������ ����� � ������� �� �����������.
  � String'e ������ ������ �������� ������+������������+�����+����+�������� ����������.
  � Object'e - ������ ����������.
}
var
  oqComponents: TOracleQuery;
  i: integer;
  function FieldCheck(Field_: string): string;
  {
    ���� ��������-������ �� �����, �� ���������� � ��.
    ���� �����, �� ���������� ������ ������.
  }
  begin
    Result := Field_;
    if ( Length(Trim(Field_)) = 0 ) then begin
      Result := '';
    end;
  end;

begin
//  JournalForm.OraLogon;
  ReqSelectForm.Cursor := crHourGlass;
  oqComponents := TOracleQuery.Create(self);
  with oqComponents do begin
    Session := JournalForm.osJournal;
    Optimize := False;
    Debug := cbShowQuery.Checked;
    try
      Clear;

      Filter_ := UpperCase(Utf8Encode(Filter_));

      SQL.Add('select nvl(U.version, '''') version, U.name iup_name, P.lastname||'' ''||P.name auth, U.crt_date, U.description ds');
      SQL.Add('  from int_updates U, int_persons P');
      SQL.Add(' where U.status < 99');
      SQL.Add('   and P.id = U.author_id');

      if FieldCheck(Filter_) <> '' then begin
        SQL.Add('   and ( ''' + Filter_ + ''' is NULL or ( upper(u.version)     like ''%' + Filter_ + '%'' or');
        SQL.Add('                                          upper(u.name)        like ''%' + Filter_ + '%'' or');
        SQL.Add('                                          upper(p.name)        like ''%' + Filter_ + '%'' or');
        SQL.Add('                                          upper(p.lastname)    like ''%' + Filter_ + '%'' or');
        SQL.Add('                                          U.crt_date           like ''%' + Filter_ + '%'' or');
        SQL.Add('                                          upper(u.description) like ''%' + Filter_ + '%''');
        SQL.Add('                                    )');
        SQL.Add('        )');
      end;

      SQL.Add(' order by U.crt_date desc');

      Execute;
      with jvsgJournalIUPs do begin
        Clear;

        Cells[0, 0] := '������ /\';
        Cells[1, 0] := '������������ /\';
        Cells[2, 0] := '����� /\';
        Cells[3, 0] := '���� �������� /\';
        Cells[4, 0] := '�������� /\';

        i := 1;
        while not Eof do begin
          InsertRow(i);
          Cells[0, i] := VarToStr(Field('version'));
          Cells[1, i] := String(Field('iup_name'));
          Cells[2, i] := String(Field('auth'));
          Cells[3, i] := VarToStr(Field('crt_date'));
          Cells[4, i] := String(Field('ds'));
          Next;
          inc(i);
        end;

      end;
    finally
      FreeAndNil(oqComponents);
      ReqSelectForm.Cursor := crDefault;
    end;
  end;
end;

procedure TReqSelectForm.FormCreate(Sender: TObject);
{
  ������������� ������ ����������.
}
begin
  FillIUPsListFromDB();
end;

procedure TReqSelectForm.jvsgJournalIUPsCaptionClick(Sender: TJvStringGrid;
  AColumn, ARow: Integer);
begin
  if ( Pos('/\', jvsgJournalIUPs.Cells[AColumn, ARow]) > 0 ) then begin
    jvsgJournalIUPs.SortGrid(AColumn, True);
    jvsgJournalIUPs.Cells[AColumn, ARow] := StringReplace(jvsgJournalIUPs.Cells[AColumn, ARow], '/\', '\/', [rfReplaceAll]);
  end
  else begin
    jvsgJournalIUPs.SortGrid(AColumn, False);
    jvsgJournalIUPs.Cells[AColumn, ARow] := StringReplace(jvsgJournalIUPs.Cells[AColumn, ARow], '\/', '/\', [rfReplaceAll]);
  end;
end;

end.
