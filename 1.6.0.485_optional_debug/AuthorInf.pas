
{*************************************************************************}
{                                                                         }
{                            XML Data Binding                             }
{                                                                         }
{         Generated on: 25.11.2011 13:29:26                               }
{       Generated from: D:\Work\���\Creator\current\Debug\AuthorInf.xml   }
{   Settings stored in: D:\Work\���\Creator\current\Debug\AuthorInf.xdb   }
{                                                                         }
{*************************************************************************}

unit AuthorInf;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLAUTHOR_INFORMATIONType = interface;
  IXMLEXTERNALAPPSType = interface;
  IXMLRECFORMSType = interface;
  IXMLCONTENTTABLEType = interface;
  IXMLEXTCOLORType = interface;
  IXMLFMBType = interface;
  IXMLEXEType = interface;

{ IXMLAUTHOR_INFORMATIONType }

  IXMLAUTHOR_INFORMATIONType = interface(IXMLNode)
    ['{EC02C29C-721A-4630-BFE9-22D952BB4853}']
    { Property Accessors }
    function Get_AUTHOR: WideString;
    function Get_MAIL: WideString;
    function Get_PHONE: WideString;
    function Get_SCENARIO: WideString;
    function Get_VERS_PATH: WideString;
    function Get_EXTERNALAPPS: IXMLEXTERNALAPPSType;
    function Get_RECFORMS: IXMLRECFORMSType;
    function Get_CONTENTTABLE: IXMLCONTENTTABLEType;
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_MAIL(Value: WideString);
    procedure Set_PHONE(Value: WideString);
    procedure Set_SCENARIO(Value: WideString);
    procedure Set_VERS_PATH(Value: WideString);
    { Methods & Properties }
    property AUTHOR: WideString read Get_AUTHOR write Set_AUTHOR;
    property MAIL: WideString read Get_MAIL write Set_MAIL;
    property PHONE: WideString read Get_PHONE write Set_PHONE;
    property SCENARIO: WideString read Get_SCENARIO write Set_SCENARIO;
    property VERS_PATH: WideString read Get_VERS_PATH write Set_VERS_PATH;
    property EXTERNALAPPS: IXMLEXTERNALAPPSType read Get_EXTERNALAPPS;
    property RECFORMS: IXMLRECFORMSType read Get_RECFORMS;
    property CONTENTTABLE: IXMLCONTENTTABLEType read Get_CONTENTTABLE;
  end;

{ IXMLEXTERNALAPPSType }

  IXMLEXTERNALAPPSType = interface(IXMLNode)
    ['{7F50B09C-933F-4C54-A224-49E6CD6FB1C9}']
    { Property Accessors }
    function Get_RUNMODE: Integer;
    function Get_ARCHIVEAPPPACK: WideString;
    function Get_ARCHIVEAPPUNPACK: WideString;
    function Get_ARCHIVEAPPNOTES: WideString;
    procedure Set_RUNMODE(Value: Integer);
    procedure Set_ARCHIVEAPPPACK(Value: WideString);
    procedure Set_ARCHIVEAPPUNPACK(Value: WideString);
    procedure Set_ARCHIVEAPPNOTES(Value: WideString);
    { Methods & Properties }
    property RUNMODE: Integer read Get_RUNMODE write Set_RUNMODE;
    property ARCHIVEAPPPACK: WideString read Get_ARCHIVEAPPPACK write Set_ARCHIVEAPPPACK;
    property ARCHIVEAPPUNPACK: WideString read Get_ARCHIVEAPPUNPACK write Set_ARCHIVEAPPUNPACK;
    property ARCHIVEAPPNOTES: WideString read Get_ARCHIVEAPPNOTES write Set_ARCHIVEAPPNOTES;
  end;

{ IXMLRECFORMSType }

  IXMLRECFORMSType = interface(IXMLNode)
    ['{CAD370C7-9BC4-44E2-8689-43837F73B63D}']
    { Property Accessors }
    function Get_MASK: WideString;
    function Get_DIR: WideString;
    procedure Set_MASK(Value: WideString);
    procedure Set_DIR(Value: WideString);
    { Methods & Properties }
    property MASK: WideString read Get_MASK write Set_MASK;
    property DIR: WideString read Get_DIR write Set_DIR;
  end;

{ IXMLCONTENTTABLEType }

  IXMLCONTENTTABLEType = interface(IXMLNode)
    ['{F7C8E96E-C77F-478E-8A8C-084D249DB38F}']
    { Property Accessors }
    function Get_EXTCOLOR: IXMLEXTCOLORType;
    { Methods & Properties }
    property EXTCOLOR: IXMLEXTCOLORType read Get_EXTCOLOR;
  end;

{ IXMLEXTCOLORType }

  IXMLEXTCOLORType = interface(IXMLNode)
    ['{2BE251D4-3442-4DCA-990E-A9B41DAB99D6}']
    { Property Accessors }
    function Get_FMB: IXMLFMBType;
    function Get_EXE: IXMLEXEType;
    { Methods & Properties }
    property FMB: IXMLFMBType read Get_FMB;
    property EXE: IXMLEXEType read Get_EXE;
  end;

{ IXMLFMBType }

  IXMLFMBType = interface(IXMLNode)
    ['{5F23A96C-8631-42A6-807C-D7AD5BB22085}']
    { Property Accessors }
    function Get_Comment: WideString;
    procedure Set_Comment(Value: WideString);
    { Methods & Properties }
    property Comment: WideString read Get_Comment write Set_Comment;
  end;

{ IXMLEXEType }

  IXMLEXEType = interface(IXMLNode)
    ['{FC81191F-48D2-4F7F-B387-67233FE36FAC}']
    { Property Accessors }
    function Get_Comment: WideString;
    procedure Set_Comment(Value: WideString);
    { Methods & Properties }
    property Comment: WideString read Get_Comment write Set_Comment;
  end;

{ Forward Decls }

  TXMLAUTHOR_INFORMATIONType = class;
  TXMLEXTERNALAPPSType = class;
  TXMLRECFORMSType = class;
  TXMLCONTENTTABLEType = class;
  TXMLEXTCOLORType = class;
  TXMLFMBType = class;
  TXMLEXEType = class;

{ TXMLAUTHOR_INFORMATIONType }

  TXMLAUTHOR_INFORMATIONType = class(TXMLNode, IXMLAUTHOR_INFORMATIONType)
  protected
    { IXMLAUTHOR_INFORMATIONType }
    function Get_AUTHOR: WideString;
    function Get_MAIL: WideString;
    function Get_PHONE: WideString;
    function Get_SCENARIO: WideString;
    function Get_VERS_PATH: WideString;
    function Get_EXTERNALAPPS: IXMLEXTERNALAPPSType;
    function Get_RECFORMS: IXMLRECFORMSType;
    function Get_CONTENTTABLE: IXMLCONTENTTABLEType;
    procedure Set_AUTHOR(Value: WideString);
    procedure Set_MAIL(Value: WideString);
    procedure Set_PHONE(Value: WideString);
    procedure Set_SCENARIO(Value: WideString);
    procedure Set_VERS_PATH(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEXTERNALAPPSType }

  TXMLEXTERNALAPPSType = class(TXMLNode, IXMLEXTERNALAPPSType)
  protected
    { IXMLEXTERNALAPPSType }
    function Get_RUNMODE: Integer;
    function Get_ARCHIVEAPPPACK: WideString;
    function Get_ARCHIVEAPPUNPACK: WideString;
    function Get_ARCHIVEAPPNOTES: WideString;
    procedure Set_RUNMODE(Value: Integer);
    procedure Set_ARCHIVEAPPPACK(Value: WideString);
    procedure Set_ARCHIVEAPPUNPACK(Value: WideString);
    procedure Set_ARCHIVEAPPNOTES(Value: WideString);
  end;

{ TXMLRECFORMSType }

  TXMLRECFORMSType = class(TXMLNode, IXMLRECFORMSType)
  protected
    { IXMLRECFORMSType }
    function Get_MASK: WideString;
    function Get_DIR: WideString;
    procedure Set_MASK(Value: WideString);
    procedure Set_DIR(Value: WideString);
  end;

{ TXMLCONTENTTABLEType }

  TXMLCONTENTTABLEType = class(TXMLNode, IXMLCONTENTTABLEType)
  protected
    { IXMLCONTENTTABLEType }
    function Get_EXTCOLOR: IXMLEXTCOLORType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEXTCOLORType }

  TXMLEXTCOLORType = class(TXMLNode, IXMLEXTCOLORType)
  protected
    { IXMLEXTCOLORType }
    function Get_FMB: IXMLFMBType;
    function Get_EXE: IXMLEXEType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFMBType }

  TXMLFMBType = class(TXMLNode, IXMLFMBType)
  protected
    { IXMLFMBType }
    function Get_Comment: WideString;
    procedure Set_Comment(Value: WideString);
  end;

{ TXMLEXEType }

  TXMLEXEType = class(TXMLNode, IXMLEXEType)
  protected
    { IXMLEXEType }
    function Get_Comment: WideString;
    procedure Set_Comment(Value: WideString);
  end;

{ Global Functions }

function GetAUTHOR_INFORMATION(Doc: IXMLDocument): IXMLAUTHOR_INFORMATIONType;
function LoadAUTHOR_INFORMATION(const FileName: WideString): IXMLAUTHOR_INFORMATIONType;
function NewAUTHOR_INFORMATION: IXMLAUTHOR_INFORMATIONType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetAUTHOR_INFORMATION(Doc: IXMLDocument): IXMLAUTHOR_INFORMATIONType;
begin
  Result := Doc.GetDocBinding('AUTHOR_INFORMATION', TXMLAUTHOR_INFORMATIONType, TargetNamespace) as IXMLAUTHOR_INFORMATIONType;
end;

function LoadAUTHOR_INFORMATION(const FileName: WideString): IXMLAUTHOR_INFORMATIONType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('AUTHOR_INFORMATION', TXMLAUTHOR_INFORMATIONType, TargetNamespace) as IXMLAUTHOR_INFORMATIONType;
end;

function NewAUTHOR_INFORMATION: IXMLAUTHOR_INFORMATIONType;
begin
  Result := NewXMLDocument.GetDocBinding('AUTHOR_INFORMATION', TXMLAUTHOR_INFORMATIONType, TargetNamespace) as IXMLAUTHOR_INFORMATIONType;
end;

{ TXMLAUTHOR_INFORMATIONType }

procedure TXMLAUTHOR_INFORMATIONType.AfterConstruction;
begin
  RegisterChildNode('EXTERNALAPPS', TXMLEXTERNALAPPSType);
  RegisterChildNode('RECFORMS', TXMLRECFORMSType);
  RegisterChildNode('CONTENTTABLE', TXMLCONTENTTABLEType);
  inherited;
end;

function TXMLAUTHOR_INFORMATIONType.Get_AUTHOR: WideString;
begin
  Result := ChildNodes['AUTHOR'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_AUTHOR(Value: WideString);
begin
  ChildNodes['AUTHOR'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_MAIL: WideString;
begin
  Result := ChildNodes['MAIL'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_MAIL(Value: WideString);
begin
  ChildNodes['MAIL'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_PHONE: WideString;
begin
  Result := ChildNodes['PHONE'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_PHONE(Value: WideString);
begin
  ChildNodes['PHONE'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_SCENARIO: WideString;
begin
  Result := ChildNodes['SCENARIO'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_SCENARIO(Value: WideString);
begin
  ChildNodes['SCENARIO'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_VERS_PATH: WideString;
begin
  Result := ChildNodes['VERS_PATH'].Text;
end;

procedure TXMLAUTHOR_INFORMATIONType.Set_VERS_PATH(Value: WideString);
begin
  ChildNodes['VERS_PATH'].NodeValue := Value;
end;

function TXMLAUTHOR_INFORMATIONType.Get_EXTERNALAPPS: IXMLEXTERNALAPPSType;
begin
  Result := ChildNodes['EXTERNALAPPS'] as IXMLEXTERNALAPPSType;
end;

function TXMLAUTHOR_INFORMATIONType.Get_RECFORMS: IXMLRECFORMSType;
begin
  Result := ChildNodes['RECFORMS'] as IXMLRECFORMSType;
end;

function TXMLAUTHOR_INFORMATIONType.Get_CONTENTTABLE: IXMLCONTENTTABLEType;
begin
  Result := ChildNodes['CONTENTTABLE'] as IXMLCONTENTTABLEType;
end;

{ TXMLEXTERNALAPPSType }

function TXMLEXTERNALAPPSType.Get_RUNMODE: Integer;
begin
  Result := ChildNodes['RUNMODE'].NodeValue;
end;

procedure TXMLEXTERNALAPPSType.Set_RUNMODE(Value: Integer);
begin
  ChildNodes['RUNMODE'].NodeValue := Value;
end;

function TXMLEXTERNALAPPSType.Get_ARCHIVEAPPPACK: WideString;
begin
  Result := ChildNodes['ARCHIVEAPPPACK'].Text;
end;

procedure TXMLEXTERNALAPPSType.Set_ARCHIVEAPPPACK(Value: WideString);
begin
  ChildNodes['ARCHIVEAPPPACK'].NodeValue := Value;
end;

function TXMLEXTERNALAPPSType.Get_ARCHIVEAPPUNPACK: WideString;
begin
  Result := ChildNodes['ARCHIVEAPPUNPACK'].Text;
end;

procedure TXMLEXTERNALAPPSType.Set_ARCHIVEAPPUNPACK(Value: WideString);
begin
  ChildNodes['ARCHIVEAPPUNPACK'].NodeValue := Value;
end;

function TXMLEXTERNALAPPSType.Get_ARCHIVEAPPNOTES: WideString;
begin
  Result := ChildNodes['ARCHIVEAPPNOTES'].Text;
end;

procedure TXMLEXTERNALAPPSType.Set_ARCHIVEAPPNOTES(Value: WideString);
begin
  ChildNodes['ARCHIVEAPPNOTES'].NodeValue := Value;
end;

{ TXMLRECFORMSType }

function TXMLRECFORMSType.Get_MASK: WideString;
begin
  Result := ChildNodes['MASK'].Text;
end;

procedure TXMLRECFORMSType.Set_MASK(Value: WideString);
begin
  ChildNodes['MASK'].NodeValue := Value;
end;

function TXMLRECFORMSType.Get_DIR: WideString;
begin
  Result := ChildNodes['DIR'].Text;
end;

procedure TXMLRECFORMSType.Set_DIR(Value: WideString);
begin
  ChildNodes['DIR'].NodeValue := Value;
end;

{ TXMLCONTENTTABLEType }

procedure TXMLCONTENTTABLEType.AfterConstruction;
begin
  RegisterChildNode('EXTCOLOR', TXMLEXTCOLORType);
  inherited;
end;

function TXMLCONTENTTABLEType.Get_EXTCOLOR: IXMLEXTCOLORType;
begin
  Result := ChildNodes['EXTCOLOR'] as IXMLEXTCOLORType;
end;

{ TXMLEXTCOLORType }

procedure TXMLEXTCOLORType.AfterConstruction;
begin
  RegisterChildNode('FMB', TXMLFMBType);
  RegisterChildNode('EXE', TXMLEXEType);
  inherited;
end;

function TXMLEXTCOLORType.Get_FMB: IXMLFMBType;
begin
  Result := ChildNodes['FMB'] as IXMLFMBType;
end;

function TXMLEXTCOLORType.Get_EXE: IXMLEXEType;
begin
  Result := ChildNodes['EXE'] as IXMLEXEType;
end;

{ TXMLFMBType }

function TXMLFMBType.Get_Comment: WideString;
begin
  Result := AttributeNodes['Comment'].Text;
end;

procedure TXMLFMBType.Set_Comment(Value: WideString);
begin
  SetAttribute('Comment', Value);
end;

{ TXMLEXEType }

function TXMLEXEType.Get_Comment: WideString;
begin
  Result := AttributeNodes['Comment'].Text;
end;

procedure TXMLEXEType.Set_Comment(Value: WideString);
begin
  SetAttribute('Comment', Value);
end;

end.