object OptionsForm: TOptionsForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 457
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 22
    Top = 437
    Width = 297
    Height = 13
    Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1103' '#1074#1089#1090#1091#1087#1103#1090' '#1074' '#1089#1080#1083#1091' '#1087#1086#1089#1083#1077' '#1087#1077#1088#1077#1079#1072#1087#1091#1089#1082#1072' '#1087#1088#1080#1083#1086#1078#1077#1085#1080#1103
  end
  object Label5: TLabel
    Left = 382
    Top = 30
    Width = 8
    Height = 16
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 437
    Width = 8
    Height = 16
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object CloseBtn: TButton
    Left = 551
    Top = 424
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object SaveBtn: TButton
    Left = 470
    Top = 424
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = SaveBtnClick
  end
  object OptionsPC: TPageControl
    Left = 8
    Top = 8
    Width = 618
    Height = 410
    ActivePage = ExtAppsTS
    HotTrack = True
    MultiLine = True
    TabOrder = 0
    object AuthorTS: TTabSheet
      Caption = #1051#1080#1095#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      ImageIndex = 2
      object Label9: TLabel
        Left = 3
        Top = 6
        Width = 23
        Height = 13
        Caption = #1060#1048#1054
      end
      object Label10: TLabel
        Left = 3
        Top = 43
        Width = 28
        Height = 13
        Caption = 'E-mail'
      end
      object Label11: TLabel
        Left = 443
        Top = 43
        Width = 44
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085
      end
      object Label12: TLabel
        Left = 3
        Top = 81
        Width = 76
        Height = 13
        Caption = #1060#1072#1081#1083' '#1089#1094#1077#1085#1072#1088#1080#1103
      end
      object Label13: TLabel
        Left = 3
        Top = 120
        Width = 103
        Height = 13
        Caption = #1056#1077#1087#1086#1079#1080#1090#1086#1088#1080#1081' '#1074#1077#1088#1089#1080#1081
      end
      object NameEd: TEdit
        Left = 37
        Top = 3
        Width = 570
        Height = 21
        TabOrder = 0
      end
      object MailEd: TEdit
        Left = 37
        Top = 40
        Width = 300
        Height = 21
        TabOrder = 1
      end
      object ScenEd: TEdit
        Left = 112
        Top = 78
        Width = 414
        Height = 21
        TabOrder = 2
      end
      object ScenarioBrowseBtn: TButton
        Left = 532
        Top = 76
        Width = 75
        Height = 25
        Caption = #1054#1073#1079#1086#1088
        TabOrder = 3
        OnClick = ScenarioBrowseBtnClick
      end
      object VersRepEd: TEdit
        Left = 112
        Top = 117
        Width = 414
        Height = 21
        TabOrder = 4
      end
      object VersRepBrowseBtn: TButton
        Left = 532
        Top = 115
        Width = 75
        Height = 25
        Caption = #1054#1073#1079#1086#1088
        TabOrder = 5
        OnClick = VersRepBrowseBtnClick
      end
      object PhoneEd: TMaskEdit
        Left = 497
        Top = 40
        Width = 110
        Height = 21
        EditMask = '!\8-000-900-0009;1;_'
        MaxLength = 14
        TabOrder = 6
        Text = '8-   -   -    '
      end
    end
    object PathsTS: TTabSheet
      Caption = #1055#1091#1090#1080
      object Label2: TLabel
        Left = 3
        Top = 6
        Width = 379
        Height = 13
        Caption = 
          #1052#1072#1089#1082#1072' '#1087#1086#1080#1089#1082#1072' '#1074' "'#1060#1086#1088#1084#1072#1093' '#1076#1083#1103' '#1087#1077#1088#1077#1082#1086#1084#1087#1080#1083#1103#1094#1080#1080'" ('#1091#1084#1086#1083#1095#1072#1090#1077#1083#1100#1085#1086#1077' '#1079#1085#1072#1095#1077#1085 +
          #1080#1077'):'
      end
      object Label6: TLabel
        Left = 3
        Top = 33
        Width = 373
        Height = 13
        Caption = 
          #1055#1091#1090#1100' '#1087#1086#1080#1089#1082#1072' '#1074' "'#1060#1086#1088#1084#1072#1093' '#1076#1083#1103' '#1087#1077#1088#1077#1082#1086#1084#1087#1080#1083#1103#1094#1080#1080'" ('#1091#1084#1086#1083#1095#1072#1090#1077#1083#1100#1085#1086#1077' '#1079#1085#1072#1095#1077#1085#1080 +
          #1077'):'
      end
      object Label7: TLabel
        Left = 375
        Top = -1
        Width = 8
        Height = 16
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 375
        Top = 30
        Width = 8
        Height = 16
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RecompileFormsExtEdit: TEdit
        Left = 389
        Top = 3
        Width = 218
        Height = 21
        TabOrder = 0
      end
      object RecompileFormsPathEdit: TEdit
        Left = 3
        Top = 52
        Width = 523
        Height = 21
        TabOrder = 1
      end
      object RecFormsDirBrowseBtn: TButton
        Left = 532
        Top = 50
        Width = 75
        Height = 25
        Caption = #1054#1073#1079#1086#1088
        TabOrder = 2
        OnClick = RecFormsDirBrowseBtnClick
      end
    end
    object ExtAppsTS: TTabSheet
      Caption = #1042#1085#1077#1096#1085#1080#1077' '#1087#1088#1080#1083#1086#1078#1077#1085#1080#1103
      ImageIndex = 1
      object Label1: TLabel
        Left = 3
        Top = 6
        Width = 299
        Height = 13
        Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1074#1085#1077#1096#1085#1080#1077' '#1087#1088#1080#1083#1086#1078#1077#1085#1080#1103' (cmd.exe, '#1072#1088#1093#1080#1074#1072#1090#1086#1088#1099'):'
      end
      object Label16: TLabel
        Left = 3
        Top = 33
        Width = 136
        Height = 13
        Hint = #1057#1090#1088#1086#1082#1072' '#1079#1072#1087#1091#1089#1082#1072' '#1079#1072#1087#1072#1082#1086#1074#1082#1080
        Caption = #1057#1090#1088#1086#1082#1072' '#1079#1072#1087#1091#1089#1082#1072' '#1079#1072#1087#1072#1082#1086#1074#1082#1080
      end
      object Label18: TLabel
        Left = 3
        Top = 88
        Width = 224
        Height = 13
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1080' '#1082' '#1089#1090#1088#1086#1082#1077' '#1079#1072#1087#1091#1089#1082#1072' '#1072#1088#1093#1080#1074#1072#1090#1086#1088#1072':'
      end
      object Label17: TLabel
        Left = 138
        Top = 25
        Width = 7
        Height = 7
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 3
        Top = 60
        Width = 142
        Height = 13
        Hint = #1057#1090#1088#1086#1082#1072' '#1079#1072#1087#1091#1089#1082#1072' '#1088#1072#1089#1087#1072#1082#1086#1074#1082#1080
        Caption = #1057#1090#1088#1086#1082#1072' '#1079#1072#1087#1091#1089#1082#1072' '#1088#1072#1089#1087#1072#1082#1086#1074#1082#1080
      end
      object Label20: TLabel
        Left = 138
        Top = 52
        Width = 7
        Height = 7
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object AppRunModeComboBox: TComboBox
        Left = 308
        Top = 3
        Width = 299
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
      end
      object ArchiveAppPackEd: TEdit
        Left = 152
        Top = 30
        Width = 374
        Height = 21
        Hint = #1057#1090#1088#1086#1082#1072' '#1079#1072#1087#1091#1089#1082#1072' '#1072#1088#1093#1080#1074#1072#1090#1086#1088#1072' ('#1089' '#1087#1072#1088#1072#1084#1077#1090#1088#1072#1084#1080')'
        TabOrder = 1
      end
      object ArchiveAppPackBtn: TButton
        Left = 532
        Top = 30
        Width = 75
        Height = 25
        Caption = #1054#1073#1079#1086#1088
        TabOrder = 2
        OnClick = ArchiveAppPackBtnClick
      end
      object ArchiveAppNotesREd: TRichEdit
        Left = 3
        Top = 107
        Width = 604
        Height = 272
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          'ArchiveAppNotesREd')
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 5
      end
      object ArchiveAppUnPackEd: TEdit
        Left = 152
        Top = 61
        Width = 374
        Height = 21
        Hint = #1057#1090#1088#1086#1082#1072' '#1079#1072#1087#1091#1089#1082#1072' '#1072#1088#1093#1080#1074#1072#1090#1086#1088#1072' ('#1089' '#1087#1072#1088#1072#1084#1077#1090#1088#1072#1084#1080')'
        TabOrder = 3
      end
      object ArchiveAppUnPackBtn: TButton
        Left = 532
        Top = 61
        Width = 75
        Height = 25
        Caption = #1054#1073#1079#1086#1088
        TabOrder = 4
        OnClick = ArchiveAppUnPackBtnClick
      end
    end
    object ContentTableTS: TTabSheet
      Caption = #1058#1072#1073#1083#1080#1094#1072
      ImageIndex = 3
      object Label14: TLabel
        Left = 3
        Top = 3
        Width = 158
        Height = 13
        Caption = #1055#1086#1076#1089#1074#1077#1090#1082#1072' '#1101#1083#1077#1084#1077#1085#1090#1086#1074' '#1090#1072#1073#1083#1080#1094#1099
      end
      object Label15: TLabel
        Left = 161
        Top = 0
        Width = 8
        Height = 16
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ExtColorVT: TVirtualStringTree
        Left = 3
        Top = 22
        Width = 604
        Height = 326
        DefaultNodeHeight = 20
        DrawSelectionMode = smBlendedRectangle
        EditDelay = 0
        Header.AutoSizeIndex = 0
        Header.DefaultHeight = 17
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'Tahoma'
        Header.Font.Style = []
        Header.Options = [hoColumnResize, hoDblClickResize, hoDrag, hoHotTrack, hoShowHint, hoShowSortGlyphs, hoVisible, hoDisableAnimatedResize]
        Margin = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TextMargin = 10
        TreeOptions.MiscOptions = [toAcceptOLEDrop, toEditable, toFullRepaintOnResize, toInitOnSave, toToggleOnDblClick, toWheelPanning, toEditOnClick]
        TreeOptions.PaintOptions = [toShowBackground, toShowButtons, toShowDropmark, toShowHorzGridLines, toThemeAware, toUseBlendedImages, toFullVertGridLines]
        TreeOptions.SelectionOptions = [toExtendedFocus]
        OnBeforeCellPaint = ExtColorVTBeforeCellPaint
        OnChange = ExtColorVTChange
        OnCreateEditor = ExtColorVTCreateEditor
        OnGetText = ExtColorVTGetText
        OnInitNode = ExtColorVTInitNode
        OnNewText = ExtColorVTNewText
        Columns = <
          item
            CheckType = ctNone
            Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus]
            Position = 0
            Width = 90
            WideText = #1056#1072#1089#1096#1080#1088#1077#1085#1080#1077
          end
          item
            Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus]
            Position = 1
            Width = 200
            WideText = #1062#1074#1077#1090
          end
          item
            Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus]
            Position = 2
            Width = 310
            WideText = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
          end>
      end
      object AddNodeBtn: TButton
        Left = 3
        Top = 354
        Width = 75
        Height = 25
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100
        TabOrder = 1
        OnClick = AddNodeBtnClick
      end
      object DelNodeBtn: TButton
        Left = 84
        Top = 354
        Width = 75
        Height = 25
        Caption = #1059#1076#1072#1083#1080#1090#1100
        TabOrder = 2
        OnClick = DelNodeBtnClick
      end
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnException = ApplicationEvents1Exception
    Left = 344
    Top = 424
  end
end
