object ReqSelectForm: TReqSelectForm
  Left = 0
  Top = 0
  Caption = #1059#1082#1072#1078#1080#1090#1077' '#1079#1072#1074#1080#1089#1080#1084#1086#1089#1090#1080' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
  ClientHeight = 568
  ClientWidth = 580
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  DesignSize = (
    580
    568)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 276
    Top = 8
    Width = 30
    Height = 13
    Caption = #1055#1086#1080#1089#1082
  end
  object btnClose: TButton
    Left = 497
    Top = 535
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 1
    TabOrder = 0
  end
  object edSearch: TEdit
    Left = 312
    Top = 4
    Width = 260
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    OnChange = edSearchChange
  end
  object cbShowQuery: TCheckBox
    Left = 8
    Top = 543
    Width = 145
    Height = 17
    Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1079#1072#1087#1088#1086#1089' '#1074' '#1041#1044
    TabOrder = 2
  end
  object jvsgJournalIUPs: TJvStringGrid
    Left = 8
    Top = 31
    Width = 564
    Height = 498
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultColWidth = 110
    DefaultRowHeight = 16
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goThumbTracking]
    TabOrder = 3
    Alignment = taLeftJustify
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'Tahoma'
    FixedFont.Style = []
    OnCaptionClick = jvsgJournalIUPsCaptionClick
  end
end
