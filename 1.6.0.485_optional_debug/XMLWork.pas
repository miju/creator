unit XMLWork;

interface

function GetXMLNodeValue(Tag_: string; FilePath_: string = 'AuthorInf.xml'): string;

implementation

uses SysUtils, Forms, XMLDom, XMLIntf, MSXMLDom, XMLDoc;

function GetXMLNodeValue(Tag_: string; FilePath_: string = 'AuthorInf.xml'): string;
var OptFile: TXMLDocument;

  function GetNodeValue(const ANodeList: IXMLNodeList; Tag_: string): string;
  var i: integer;
      TargetNode: IXMLNode;
  begin
    try
      TargetNode := ANodeList.FindNode(Tag_);
      if TargetNode <> nil then Result := TargetNode.Text
      else begin
        for i := 0 to (ANodeList.Count - 1) do begin
          Result := GetNodeValue(ANodeList[i].ChildNodes, Tag_);
        end;
      end;
    except
      on e:Exception do begin
        Exit;
      end;
    end;
  end;

begin
  OptFile := TXMLDocument.Create(Application);
  try
    OptFile.LoadFromFile(FilePath_);
    Result := GetNodeValue(OptFile.ChildNodes, Tag_);
    FreeAndNil(OptFile);
  except
    on e:Exception do begin
      Exit;
    end;
  end;
end;

end.
