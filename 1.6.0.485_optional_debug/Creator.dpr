program Creator;

uses
  Forms,
  main in 'main.pas' {IUPCreatorForm},
  FolderActions in 'FolderActions.pas',
  ZLibAdvTools in 'ZLibAdvTools.pas',
  CheckingSum in 'CheckingSum.pas',
  PackingLib in 'PackingLib.pas',
  VersionRepository in 'VersionRepository.pas',
  Lib in 'Lib.pas',
  FilesSelect in 'FilesSelect.pas' {FilesSelectForm},
  Options in 'Options.pas' {OptionsForm},
  ExtInst in 'ExtInst.pas' {ExtInstForm},
  Scenario2 in 'Scenario2.pas',
  About in 'About.pas' {AboutForm},
  Windows,
  SysUtils,
  XMLWork in 'XMLWork.pas',
  UpdateXML in 'UpdateXML.pas',
  journal in 'journal.pas' {JournalForm},
  ReqsSelect in 'ReqsSelect.pas' {ReqSelectForm},
  AuthorInf in 'AuthorInf.pas';

var hMutex: cardinal;

{$R *.res}

begin
  Application.Initialize;

  // << �� �������� �� ��� ���, ���� �� ����� ������� ������ ���� �����������
  hMutex := CreateMutex(nil, false, '{CB166DD9-9657-458F-920D-EDD75AF8CF52}');
  while GetLastError<>0 do begin
    CloseHandle(hMutex);
    Sleep(5000);
    Application.MessageBox(PChar('����� ������������ ������ �������� ��� ��������� ����� ������������'), '��������!');
    hMutex := CreateMutex(nil, false, '{CB166DD9-9657-458F-920D-EDD75AF8CF52}');
  end;
  // >> �� �������� �� ��� ���, ���� �� ����� ������� ������ ���� �����������

  // ������� ��������� �����.
  Lib.RemoveFilesAndFoldersByMask(SysUtils.GetEnvironmentVariable('TEMP'), '*IUPCreator*');

  Application.CreateForm(TIUPCreatorForm, IUPCreatorForm);
  Application.CreateForm(TFilesSelectForm, FilesSelectForm);
  Application.CreateForm(TOptionsForm, OptionsForm);
  Application.CreateForm(TExtInstForm, ExtInstForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.CreateForm(TJournalForm, JournalForm);
  Application.CreateForm(TReqSelectForm, ReqSelectForm);
  // ������������� ���� ������������ ������� ����������
  IUPCreatorForm.FillReqsMemo;

  Application.Run;
end.
