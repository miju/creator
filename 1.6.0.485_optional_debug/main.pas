{$WARN SYMBOL_DEPRECATED OFF}
{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_PLATFORM OFF}

{ DONE : �������� �������� - �������� � ����������� ������ �� ����������� ���������� }

unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, ExtDlgs,
  //
  FileCtrl, Buttons, Envmnt_Var_Work,
  FolderActions, ZLibAdvTools, CheckingSum, Spin,
  ShellAPI, CrtProcess, PackingLib, VersionRepository,
  AuthorInf, Lib, Mask, VirtualTrees, ActiveX, Menus, ShlObj, ToolWin, ImgList, StdActns, ActnList,
  AppEvnts, Journal, Oracle, ReqsSelect;

type
  // << �������� � ��
  TVTEditorKind = (            // �������� ��� ��.
    ekMemo,      // TMemo
    ekComboBox   // TComboB
  );

  PElemNode = ^TElemNode;      // ������, �������� � ������ ����
  TElemNode = record
    ListNum:    integer;
    InstallNum: integer;
    Ext:        string;
    FileName:   string;
    ObjName:    string;
    Desc:       string;
    Path:       string;
    Kind:       TVTEditorKind;
    Changed:    Boolean;
    Install:    TStringList;
    BackUp:     TStringList;
    Return:     TStringList;
    VTColor:    TColor;
  end;

  TVTCustomEditor = class(TInterfacedObject, IVTEditLink)
  private
    FEdit: TWinControl; // ������� ����� ��� ������� ���� ���������
    FTree: TVirtualStringTree; // ������ �� ������, ��������� ��������������
    FNode: PVirtualNode; // ������������� ����
    FColumn: Integer; // ��� �������, � ������� ��� ����������
  protected
    procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  public
    destructor Destroy; override;
    function   BeginEdit: Boolean; stdcall;
    function   CancelEdit: Boolean; stdcall;
    function   EndEdit: Boolean; stdcall;
    function   GetBounds: TRect; stdcall;
    function   PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; stdcall;
    procedure  ProcessMessage(var Message: TMessage); stdcall;
    procedure  SetBounds(R: TRect); stdcall;
  end;


  // >> �������� � ��

  TIUPCreatorForm = class(TForm)
    FileListPanel: TPanel;
    LegendPanel: TPanel;
    AddFileBtn: TBitBtn;
    AddFolderBtn: TBitBtn;
    DelBtn: TBitBtn;
    LoadOldIUPBtn: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    IUPNameEdit: TEdit;
    IUPAuthor: TEdit;
    IUP_EMailEdit: TEdit;
    VersYear: TSpinEdit;
    VersUpd: TSpinEdit;
    VersPatch: TSpinEdit;
    VersStatus: TSpinEdit;
    IUP_Date: TDateTimePicker;
    Label8: TLabel;
    ScenarioXMLPathLabel: TLabel;
    Label10: TLabel;
    LogMemo: TMemo;
    LoadArchIUPBtn: TBitBtn;
    _RecompileListBtn: TButton;
    RecompileListMemo: TMemo;
    Label11: TLabel;
    DelAllBtn: TBitBtn;
    LoadIUPBtn: TBitBtn;
    VT: TVirtualStringTree;
    NodeMenu: TPopupMenu;
    CopyFilePathToClipboardMI: TMenuItem;
    OpenInNotepadMI: TMenuItem;
    OpenInAppMI: TMenuItem;
    ExploreNFocusFileMI: TMenuItem;
    ExtInstMI: TMenuItem;
    SB: TStatusBar;
    MainMenu: TMainMenu;
    FileMI: TMenuItem;
    AddFileMI: TMenuItem;
    PrefMI: TMenuItem;
    OptMI: TMenuItem;
    InfoMI: TMenuItem;
    HelpMI: TMenuItem;
    AboutMI: TMenuItem;
    ToolBar1: TToolBar;
    AddFileTBtn: TToolButton;
    ToolBarImageList: TImageList;
    AddFolderTBtn: TToolButton;
    AddUIPTBtn: TToolButton;
    ToolBarActionList: TActionList;
    FileOpenAct: TFileOpen;
    BrowseForFolderAct: TBrowseForFolder;
    AddFolderMI: TMenuItem;
    N1: TMenuItem;
    IUPOpenAct: TFileOpen;
    RemRecTBtn: TToolButton;
    ClrListTBtn: TToolButton;
    OptTBtn: TToolButton;
    AboutTBtn: TToolButton;
    RemRecAct: TAction;
    ClrListAct: TAction;
    OptAct: TAction;
    HelpAct: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    RefreshAppTBtn: TToolButton;
    RefreshAllAct: TAction;
    ScenLoadAct: TFileOpen;
    SCenLoadTBtn: TToolButton;
    CrtIUPTBtn: TToolButton;
    ToolButton5: TToolButton;
    UpdateXMLSaveAct: TFileSaveAs;
    UpdXMLTBtn: TToolButton;
    LogSaveAct: TFileSaveAs;
    SaveLogMI: TMenuItem;
    CrtIUPAct: TFileSaveAs;
    ExitAct: TAction;
    AboutAct: TAction;
    ScelLoadMI: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    AddIUPMI: TMenuItem;
    N4: TMenuItem;
    UpdXMLSaveMI: TMenuItem;
    N5: TMenuItem;
    RefreshMI: TMenuItem;
    ToolButton3: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    IUP_PhoneEdit: TMaskEdit;
    IUPVers_Edit: TEdit;
    JournalPanel: TPanel;
    cbComponent: TComboBox;
    Label1: TLabel;
    Label9: TLabel;
    cbClient: TComboBox;
    Label12: TLabel;
    Label13: TLabel;
    cbAuthor: TComboBox;
    Label14: TLabel;
    IUP_Desc_Memo: TMemo;
    Button1: TButton;
    RecompileListBtn: TBitBtn;
    RequirementsListMemo: TMemo;
    Label15: TLabel;
    ReqsListBtn: TBitBtn;
    JournalDBLoginAct: TAction;
    ToolButton4: TToolButton;
    procedure JournalDBLoginActExecute(Sender: TObject);
    procedure ReqsListBtnClick(Sender: TObject);
    procedure RecompileListBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cbAuthorSelect(Sender: TObject);
    procedure cbClientSelect(Sender: TObject);
    procedure cbComponentSelect(Sender: TObject);
    procedure IUPVers_EditKeyPress(Sender: TObject; var Key: Char);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure ToolBar1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure CrtIUPTBtnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure LogSaveActBeforeExecute(Sender: TObject);
    procedure AboutActExecute(Sender: TObject);
    procedure ExitActExecute(Sender: TObject);
    procedure CrtIUPActBeforeExecute(Sender: TObject);
    procedure CrtIUPActAccept(Sender: TObject);
    procedure LogSaveActAccept(Sender: TObject);
    procedure UpdateXMLSaveActAccept(Sender: TObject);
    procedure ScenLoadActAccept(Sender: TObject);
    procedure HelpActExecute(Sender: TObject);
    procedure OptActExecute(Sender: TObject);
    procedure RefreshAllActExecute(Sender: TObject);
    procedure IUPOpenActAccept(Sender: TObject);
    procedure BrowseForFolderActAccept(Sender: TObject);
    procedure FileOpenActAccept(Sender: TObject);
    procedure ClrListActExecute(Sender: TObject);
    procedure RemRecActExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ExtInstMIClick(Sender: TObject);
    procedure VTGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: WideString);
    procedure ExploreNFocusFileMIClick(Sender: TObject);
    procedure OpenInNotepadMIClick(Sender: TObject);
    procedure OpenInAppMIClick(Sender: TObject);
    procedure VTGetPopupMenu(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      const P: TPoint; var AskParent: Boolean; var PopupMenu: TPopupMenu);
    procedure CopyFilePathToClipboardMIClick(Sender: TObject);
    procedure VTInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
      var InitialStates: TVirtualNodeInitStates);
    procedure VTDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject;
      Formats: TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
    procedure VTDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState;
      State: TDragState; Pt: TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
    procedure VTDragAllowed(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      var Allowed: Boolean);
    procedure SaveLogBtnClick(Sender: TObject);
    procedure VTCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      out EditLink: IVTEditLink);
    procedure VTCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode;
      Column: TColumnIndex; var Result: Integer);
    procedure VTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VTNewText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      NewText: WideString);
    procedure VTHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
    procedure VTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType; var CellText: WideString);
    procedure VTEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      var Allowed: Boolean);
    procedure VTPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType);
    procedure LoadIUPBtnClick(Sender: TObject);
    procedure DelAllBtnClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure _RecompileListBtnClick(Sender: TObject);
    procedure LoadArchIUPBtnClick(Sender: TObject);
    procedure VersUpdChange(Sender: TObject);
    procedure IUP_Desc_MemoDblClick(Sender: TObject);
    procedure ScenarioXMLLoadBtnClick(Sender: TObject);
    procedure LoadOldIUPBtnClick(Sender: TObject);
    procedure UpdateXMLWriteBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure DelBtnClick(Sender: TObject);
    procedure AddFolderBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AddFileBtnClick(Sender: TObject);
    procedure FillReqsMemo();
  private
    { Private declarations }
    procedure FillIUPInfoWithUpdateXML(UpdXML_: string);
    procedure FillGridWithUpdateXML(VT_: TVirtualStringTree; UpdXML_: string);
    procedure MessProc(Capt_, Text_: string; Dlg_, Memo_, StBar_: Boolean; flag_: integer = MB_ICONINFORMATION);
    procedure MoveSQLToTop(VT_: TVirtualStringTree);

    function  isSelectedElements(VT_: TVirtualStringTree): Boolean;
    function  InsertIntoVT(FilePath_, Ext_: string; TargetNode_: PVirtualNode): Boolean;
    function  UpdateXMLWriter(): string;
    function  CopyMan(src, dest: string): Boolean;
  public
    { Public declarations }
  end;

var
  IUPCreatorForm: TIUPCreatorForm;
  WorkPG: TProgressBar;

implementation

uses Types, FilesSelect, UpdateXML, Options, ExtInst, About, Math, XMLWork,
  XMLIntf;

{$R *.dfm}

function DODO3(fl1,fl2:string):boolean;
begin
  Result:=ZLATdecompressfile(fl1,fl2,false,true,nil,0,0);
end;

// �������� ���������� �� ������ � ������ � ��������� ��
function CS_check(Path: String): Boolean;

var checksum_file: TextFile; // ��������� ����������� ����
    CS, CS_etal: int64;
    search:    TSearchRec;
    AttrFile: Integer;//�������� ������
    mask: string;
    sfile: string; // ������ ���� �� ���������  �����
    extn: string; // ��� ��������������� �����

begin
    CS := -1; // �������� ������� ��
    mask :=Path;
//    ShowMessage('Mask = '+mask);
    extn := '*.*';
    AttrFile := $0000003F; {Any file}
    // ������� �� ������ � �����
    if FindFirst(mask+'\'+extn, AttrFile , search) = 0 then
      repeat
        sfile := mask+'\'+search.Name;
//        ShowMessage('search.Name = '+search.Name);
        if search.Name <> 'checksum' then
          CS := CS+CheckSum(sfile);
      until FindNext(search) <> 0;
    SysUtils.FindClose(search);
    // ��������� � ��������� �� �� ����� checksum ��
    AssignFile(checksum_file, Path+'\checksum');
    Reset(checksum_file);
    readln(checksum_file,CS_etal);
    CloseFile(checksum_file);
    if CS_etal <> CS then Result := False
    else Result := True;

end;

// ��������� ������� ����������� � ���������� ����, ����, ���������.
procedure TIUPCreatorForm.MessProc(Capt_, Text_: string; Dlg_, Memo_, StBar_: Boolean; flag_: integer = MB_ICONINFORMATION);
begin
  if Dlg_   then MessageBox(0, PChar(Text_), PChar(Capt_), flag_);

  if Memo_  then LogMemo.Lines.Add(Text_);
  LogMemo.Refresh;

  if StBar_ then SB.Panels[0].Text := Text_;
  SB.Refresh;
end;

// ��������� ��� ������, ��������� �� ��� �� Explorer'a.
function GetDropFilesList(DataObject: IDataObject; Target: TVirtualStringTree): TStringList;
var
  FormatEtc: TFormatEtc;
  Medium: TStgMedium;
  OLEData: PDropFiles;
  Files: PChar;
  Str: String;
begin
  // �� ���� ��� ��� ���������� ���� �� � ������ CF_HDROP ������
  Result := TStringList.Create;
  with FormatEtc do
  begin
    cfFormat := CF_HDROP;
    ptd := nil;
    dwAspect := DVASPECT_CONTENT;
    lindex := -1;
    tymed := TYMED_HGLOBAL;
  end;
  if DataObject.QueryGetData(FormatEtc) <> S_OK then begin
    exit;
  end;
  if DataObject.GetData(FormatEtc, Medium) <> S_OK then begin
    exit;
  end;
  OLEData := GlobalLock(Medium.hGlobal);
  if not Assigned(OLEData) then begin
    exit;
  end;
  try
    // ������ ��������� ������ �������� � ������ ���������
    // + offset, ������� � ���� OLEData^.pFiles.
    Files := PChar(OLEData) + OLEData^.pFiles;
    // ������ ������������ ������� null ��������
    Target.BeginUpdate;
    while Files^ <> #0 do begin
      if OLEData^.fWide then begin
        Str := PWideChar(Files);
        // +1 ����� ��� ����, ����� ����������� null ������ ������ �� ��� ������ � ������
        Inc(Files, (Length(PWideChar(Files)) + 1)*SizeOf(WideChar));
      end
      else begin
        Str := Files;
        // ����������
        Inc(Files, (Length(PChar(Files)) + 1)*SizeOf(Char));
      end;
      Result.Add(Str);
    end;
  finally
    GlobalUnlock(Medium.hGlobal);
    Target.EndUpdate;
  end;
  ReleaseStgMedium(Medium);
end;

destructor TVTCustomEditor.Destroy;
begin
  FreeAndNil(FEdit);
  inherited;
end;

//------------------------------------------------------------------------------
// ��� ��������� ������� � ����������.
// ������ �������������� �� Escape, ���������� �������������� �� Enter,
// � ������� ����� ������ �� Up/Down, ���� ������ ��������� � ����� ����� ���
// ��������� ���� �� �������.
//------------------------------------------------------------------------------
procedure TVTCustomEditor.EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  CanContinue: Boolean;
begin
  CanContinue := True;
  case Key of
    VK_ESCAPE: // ������ Escape
      if CanContinue then
      begin
        FTree.CancelEditNode;
        Key := 0;
      end;
    VK_RETURN: // ������ Enter
      if CanContinue then
      begin
        // ���� Ctrl ��� TMemo �� �����, �� ��������� ��������������
        // ������� ���, ����� ����� ���� �� Ctrl+Enter ��������� � Memo
        // ����� �����.
        if (FEdit is TMemo) and (Shift = []) then
          FTree.EndEditNode
        else if not(FEdit is TMemo) then
          FTree.EndEditNode;
        Key := 0;
      end;
    VK_UP, VK_DOWN:
      begin
        // ���������, �� ��� �� ������ � ����������. ���� ���, �� ���������
        // ���������� ������, ���� ���, �� �������� ������� ������.
        CanContinue := Shift = [];
        if FEdit is TComboBox then
          CanContinue := CanContinue and not TComboBox(FEdit).DroppedDown;
        if CanContinue then
        begin
          // �������� ������� ������
          PostMessage(FTree.Handle, WM_KEYDOWN, Key, 0);
          Key := 0;
        end;
      end;
  end;
end;

//------------------------------------------------------------------------------
// �������� ��������������, ����� �������� �������� � ���������� ��� �����.
//------------------------------------------------------------------------------
function TVTCustomEditor.BeginEdit: Boolean;
begin
  Result := True;
  with FEdit do
  begin
    Show;
    SetFocus;
  end;
end;

//------------------------------------------------------------------------------
// ������� �����������, ������ ��������, ��������� ������ ���� � ����������
// ����� ������.
//------------------------------------------------------------------------------
function TVTCustomEditor.EndEdit: Boolean;
var
  Txt: String;
begin
  Result := True;
  if FEdit is TMemo then
    Txt := TEdit(FEdit).Text
  else if FEdit is TComboBox then
    Txt := TComboBox(FEdit).Text;
  // �������� ����� ���� (���� Value � TVTEditNode) ����� ������� OnNewText
  // � ������
  FTree.Text[FNode, FColumn] := Txt;
  FEdit.Hide;
  FTree.SetFocus;
end;

//------------------------------------------------------------------------------
// ���������� ������� ���������.
//------------------------------------------------------------------------------
function TVTCustomEditor.GetBounds: TRect;
begin
  Result := FEdit.BoundsRect;
end;

//------------------------------------------------------------------------------
// � ���������� � �������������� �� ������ ������� ��������� TWinControl
// ������� ������ ������� � ������������ � ����� Kind � TVTEditNode.
//------------------------------------------------------------------------------
function TVTCustomEditor.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex): Boolean;
var
  VTElemNode: PElemNode;
  i: integer;
begin
  Result := True;
  FTree := Tree as TVirtualStringTree;
  FNode := Node;
  FColumn := Column;
  FreeAndNil(FEdit);
  VTElemNode := FTree.GetNodeData(Node);

  if Column = 1 then VTElemNode.Kind := ekComboBox // ������ ):
                else VTElemNode.Kind := ekMemo;

  case VTElemNode.Kind of
    ekMemo: begin
      FEdit := TMemo.Create(nil);
      with FEdit as TMemo do
      begin
        Visible := False;
        Parent := Tree;
        ScrollBars := ssVertical;
        Text := VTElemNode.ObjName;
        if Column = 4 then Text := VTElemNode.ObjName // ������ ):
        else               Text := VTElemNode.Desc;
        OnKeyDown := EditKeyDown;
      end;
    end;
    ekComboBox: begin
      FEdit := TComboBox.Create(nil);
      with FEdit as TComboBox do
      begin
        Visible := False;
        Parent := Tree;
        Text := IntToStr(VTElemNode.InstallNum);
        for i := 0 to (FTree.RootNodeCount-2) do begin
          Items.Add(IntToStr(i+1));
        end;
        OnKeyDown := EditKeyDown;
      end;
    end;
  else
    Result := False;
  end;
end;

//------------------------------------------------------------------------------
// ��������� ��������� Windows ��� ���������.
//------------------------------------------------------------------------------
procedure TVTCustomEditor.ProcessMessage(var Message: TMessage);
begin
  FEdit.WindowProc(Message);
end;

//------------------------------------------------------------------------------
// ������������� ������� ��������� � ������������ � ������� � ������� �������.
//------------------------------------------------------------------------------
procedure TVTCustomEditor.SetBounds(R: TRect);
var
  Dummy: Integer;
begin
  FTree.Header.Columns.GetColumnBounds(FColumn, Dummy, R.Right);
  FEdit.BoundsRect := R;
  if FEdit is TMemo then begin
    FEdit.Height := 130;
    if FEdit.Width < 240 then FEdit.Width := 240;
  end;
end;

//------------------------------------------------------------------------------
// ����������, ������ ��������.
//------------------------------------------------------------------------------
function TVTCustomEditor.CancelEdit: Boolean;
 begin
  Result := True;
  FEdit.Hide;
end;

procedure TIUPCreatorForm.FileOpenActAccept(Sender: TObject);
var i: integer;
begin
  WorkPG.Position := 0;
  for i := 0 to (FileOpenAct.Dialog.Files.Count - 1) do begin
    InsertIntoVT(FileOpenAct.Dialog.Files[i], '', VT.RootNode);
    WorkPG.Position := WorkPG.Position + Round((WorkPG.Max - 5)/FileOpenAct.Dialog.Files.Count);
  end;
  WorkPG.Position := 0;
  MessProc('', '����� ���������', False, True, True);

  if Length(Trim(IUPNameEdit.Text)) = 0 then begin
    IUPNameEdit.Text := ExtractFileName(ExtractFileDir(FileOpenAct.Dialog.Files[0]));
  end;
  MoveSQLToTop(VT);
end;

// ��������� ��������� ���� ������� �� ���������� Update.xml
procedure TIUPCreatorForm.FillGridWithUpdateXML(VT_: TVirtualStringTree; UpdXML_: string);
var
  Upd: IXMLUPDATEType; //��������� ���-���� ���� ������
  i  : Integer;
  fileext: string;
  NewNode: PVirtualNode;
  NewElem: PElemNode;

begin
  try
    Upd := LoadUpdate(UpdXML_);

    WorkPG.Position := 0;
    for i:= 1 to Upd.ITEM.Count do begin
      NewNode := VT_.AddChild(VT_.RootNode);
      NewElem := VT_.GetNodeData(NewNode);
      if Assigned(NewElem) then begin
        with NewElem^ do begin
          fileext := Upd.ITEM.Items[i-1].FILE_NAME;
          fileext := ExtractFileExt(fileext);
          System.Delete(fileext, 1, 1);

          InstallNum := Upd.ITEM.Items[i-1].SEQ;
          Ext        := fileext;
          FileName   := Upd.ITEM.Items[i-1].FILE_NAME;
          ObjName    := Upd.ITEM.Items[i-1].OBJECT_NAME;
          Desc       := Upd.ITEM.Items[i-1].DESCRIPTION;
//          Path       := Upd.ITEM.Items[i-1].FILE_NAME;
          Path       := GetEnvironmentVariable('TEMP')+'\IUPCreator\' + Upd.ITEM.Items[i-1].FILE_NAME;

          Changed    := False;
          Install := TStringList.Create;
          Install.Add('');
          BackUp := TStringList.Create;
          BackUp.Add('');
          Return := TStringList.Create;
          Return.Add('');

        end;
        MessProc('', '��������� ���������� � "' + VT_.Text[VT_.GetLastChild(VT_.RootNode), 3] +
                                                  VT_.Text[VT_.GetLastChild(VT_.RootNode), 2] + '"',
                 False, True, True);
        WorkPG.Position := WorkPG.Position + Round((WorkPG.Max - 5)/Upd.ITEM.Count);
        Sleep(50);
      end;
    end;
    WorkPG.Position := 0;
    MessProc('', '', False, False, True);
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(FillGridWithUpdateXML): ������ ���������� ������� ������� �� �������������� ����������.' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      Exit;
    end;
  end;
end;

// ��������� ��������� ���������� �� ���������� ������� �� ���������� Update.xml
procedure TIUPCreatorForm.FillIUPInfoWithUpdateXML(UpdXML_: string);
var
  Upd:  IXMLUPDATEType; //��������� ���-���� ���� ������
begin
  try
    Upd := LoadUpdate(UpdXML_);
    with Upd do begin
      IUPNameEdit.Text   := UPDATE_NAME;
      IUPAuthor.Text     := AUTHOR;
      IUPVers_Edit.Text  := VERSION;
      IUP_EMailEdit.Text := EMAIL;
      IUP_Date.Date      := StrToDate(DATE);
      IUP_Desc_Memo.Text := DESCRIPTION;
    end;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(FillIUPInfoWithUpdateXML): ������ ���������� ������� ������� �� �������������� ����������: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      Exit;
    end;
  end;
end;

{ TODO : �������� ���������� SQL-�� }
procedure TIUPCreatorForm.MoveSQLToTop(VT_: TVirtualStringTree);
var
  i  : Integer;
  CurNode: PVirtualNode;
  Elem:    PElemNode;
  Nodes: TNodeArray; // ������, � ������� ����� ������� ���������� ����

begin
  try
    if VT_.RootNodeCount = 0 then exit;
    CurNode := VT_.GetFirst();
    if not Assigned(CurNode) then exit;

    SetLength(Nodes, 0);
    repeat
      Elem := VT_.GetNodeData(CurNode);
      if (Assigned(Elem) and (UpperCase(Elem.Ext) = 'SQL')) then begin
        SetLength(Nodes, Length(Nodes)+1);
        Nodes[Length(Nodes)-1] := CurNode; // ���� ������� ������� - sql, �� ��������� ��� � �������.
      end;
      CurNode := VT_.GetNext(CurNode);
    until CurNode = nil;

    if Length(Nodes) = 0 then exit;
    
    VT_.MoveTo(Nodes[0], VT_.GetFirst(), amInsertBefore, False); // ������� ��������� � ����� ���� ������ ������� �������...
    for i := 1 to (Length(Nodes)-1) do begin
      VT_.MoveTo(Nodes[i], Nodes[i-1], amInsertAfter, False); //... � �� ��� ����������� ��� ���������.
    end;

  except
    on e:Exception do begin
      MessProc('������', 'main.pas(MoveSQLToTop): ������ ����������� SQL-������ � �������.' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      Exit;
    end;
  end;
end;

// ������� ��������� ����, � ������� ��������� ������� �����.
function VTIndexOf(VT_: TVirtualStringTree; srchstr: string): integer;
var VTNode:   PVirtualNode;
    ElemNode: PElemNode;
begin
  Result := -1;
  if VT_.RootNodeCount = 0 then exit;
  VTNode := VT_.GetFirst();
  if not Assigned(VTNode) then exit;
  repeat
    ElemNode := VT_.GetNodeData(VTNode);
    if (Assigned(ElemNode) and (UpperCase(ElemNode^.FileName) = UpperCase(srchstr))) then begin
      VT_.FocusedNode := VTNode;
      Result := VTNode.Index;
      exit;
    end;
    VTNode := VT_.GetNext(VTNode);
  until VTNode = nil;
end;

// ������� ��������, ���� �� � ������� ���������� �����
function TIUPCreatorForm.isSelectedElements(VT_: TVirtualStringTree): Boolean;
var CurNode: PVirtualNode;
    Elem: PElemNode;
begin
  Result := False;
  try
    if VT_.RootNodeCount = 0 then exit;
    CurNode := VT_.GetFirst(False);
    if not Assigned(CurNode) then exit;
    repeat
      Elem := VT_.GetNodeData(CurNode);
      if Assigned(Elem) and (Elem.InstallNum <> 0) then begin // ��������� ������ ������� �� �� ��� ���, ���� �� ����� ���� �� ����, ����������� ��� ���������
        Result := True;
        exit;
      end;
      CurNode := VT_.GetNext(CurNode, False);
    until CurNode = nil;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(isSelectedElements): ����������� ������ ������� ������.' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      Exit;
      Result := False;
    end;
  end;
end;

procedure TIUPCreatorForm.ClrListActExecute(Sender: TObject);
begin
  VT.Clear;
end;

{ DONE : ���� ����������� ���������� ���� ����� � ����� ������ ������� }
procedure TIUPCreatorForm.CopyFilePathToClipboardMIClick(Sender: TObject);
var Elem:   PElemNode;
begin
  Elem := VT.GetNodeData(VT.FocusedNode);
  if Assigned(Elem) then begin
    Lib.PutStringIntoClipBoard(Elem^.Path);
  end;
end;

// ������� ����������� �����/�����: (����)->(����), (�����+����������)->(�����+����������)
function TIUPCreatorForm.CopyMan(src, dest: string): Boolean;
begin
  Result := False;
  try

    if Length(src) = 0 then begin
      Result := True;
    end;

    if FileExists(src) then begin
//      CPRun('cmd /c xcopy /C /Y "'+src+'" "'+dest+'"',
//             AuthorInf.LoadAUTHOR_INFORMATION(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml').APPRUNMODE);
      CPRun('cmd /c xcopy /C /Y "'+src+'" "'+dest+'"', StrToInt(XMLWork.GetXMLNodeValue('RUNMODE')));
      Result := True;
    end
    else begin
      if DirectoryExists(src) then begin
        dest := dest + '\' + ExtractFileName(src);
        ForceDirectories(dest);
//        CPRun('cmd /c xcopy /E /C /Y "'+src+'" "'+dest+'"',
//             AuthorInf.LoadAUTHOR_INFORMATION(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml').APPRUNMODE);
        CPRun('cmd /c xcopy /E /C /Y "'+src+'" "'+dest+'"', StrToInt(XMLWork.getxmlnodevalue('RUNMODE')));
        Result := True;
      end;
    end;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(CopyMan): ������ ����������� ������ ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      Result := False;
      exit;
    end;
  end;
end;

function  CheckIUPinJournal(IUPName_, IUPVers_: string): string;
{
  ��������, ��� �� � ������� ���������� ������ �� IUP'�.
}
var
  oqComponents: TOracleQuery;
begin
  Result := '';
//  JournalForm.OraLogon; // ���������
  oqComponents := TOracleQuery.Create(nil);
  with oqComponents do begin
    Session := JournalForm.osJournal;
    Optimize := False;
    try
      Clear;
      SQL.Add('select U.version vers, U.name||P.lastname auth, U.crt_date date, U.description desc');
      SQL.Add('  from int_updates U, int_persons P');
      SQL.Add(' where ( (U.name = :IUPName) or (U.version = :IUPVers))');
      SQL.Add(' order by U.crt_date asc');
      DeclareAndSet('IUPName', otString, IUPName_);
      DeclareAndSet('IUPVers', otString, IUPVers_);
      Execute;
      while not Eof do begin
        Result := String(Field('vers')) + ', ' +
                  String(Field('name')) + ', ' +
                  String(Field('auth')) + ', ' +
                  String(Field('date')) + ', ' +
                  String(Field('desc') + #13#10);
        Next;
      end;
    finally
      Clear;
      FreeAndNil(oqComponents);
    end;
  end;
end;

procedure TIUPCreatorForm.CrtIUPActAccept(Sender: TObject);
var TmpDir, UpdFilePath: string;
    UpdateXMLFilePath: string;
    CurNode: PVirtualNode;
    Elem: PElemNode;
    JournalRecordRes: string;
    formatSettings : TFormatSettings;
begin
  if not FileExists(ScenarioXMLPathLabel.Caption) then begin
    MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� ����� ���� ��������� Scenario.xml', True, True, True, MB_ICONERROR);
    exit;
  end;
  if not isSelectedElements(VT) then begin
    MessProc('������', 'main.pas(IUPCreateBtnClick): �������, ����� ����� �� ������ �������� � ����������', True, True, True, MB_ICONERROR);
    exit;
  end;
  if Length(Trim(IUPNameEdit.Text)) = 0 then begin
    MessProc('������', 'main.pas(IUPCreateBtnClick): ����������, ������� ������������ ����������', True, True, True, MB_ICONERROR);
    IUPNameEdit.SetFocus;
    exit;
  end;
  // << ������ ���������� ��������� �����
  try
    TmpDir := GetEnvironmentVariable('TEMP') + '\IUPCreator_' + FormatDateTime('yyyy-mm-dd_(hh-mm-ss)', Now);
    MessProc('', '��������� (�������) ����������: ' + TmpDir, False, True, True);
    if not DirectoryExists(TmpDir) then
      if SysUtils.ForceDirectories(TmpDir) then begin// ���� ����������, ��� ����� ������������ ��������
        MessProc('', '������� ������� �����: '+ TmpDir, False, True, True);
      end
      else begin
        MessProc('', '�� ������� ������� ����� "'+ TmpDir + '": '+IntToStr(GetLastError)+' - '+SysErrorMessage(GetLastError),
                 True, True, True);
      end;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(IUPCreateBtnClick): ������ �������� ������� �����: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;
  // >> ������ ���������� ��������� �����
  // << �������� ������� ���������� ���������� � ������� (���������) �����
  try
    if VT.RootNodeCount = 0 then exit;
    WorkPG.Position := 0;
    CurNode := VT.GetFirst(False);
    if not Assigned(CurNode) then exit;
    repeat
      Elem := VT.GetNodeData(CurNode);
      if Assigned(Elem) then begin
        if not CopyMan(Elem.Path, TmpDir) then begin
          MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� ��������� ����������� "'+Elem.Path+'" �� ��������� ����������', True, True, True, MB_ICONERROR);
        end;
        WorkPG.Position := WorkPG.Position + Round((WorkPG.Max - 5)/VT.RootNodeCount);
        MessProc('', '��������� "'+ Elem.FileName + Elem.Ext + '"', False, True, True);
        IUPCreatorForm.Refresh;
      end;
      CurNode := VT.GetNext(CurNode, False);
    until CurNode = nil;
    WorkPG.Position := 0;
    MessProc('', '����� ����������� �� ��������� �����', False, True, True);
    IUPCreatorForm.Refresh;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(IUPCreateBtnClick): ������ ����������� ������ �� ��������� (�������) ����������: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;
  // >> �������� ������� ���������� ���������� � ������� (���������) �����
  // << ����� � �������� � ������� ����� ���� �������� ���������� Update.xml
  UpdateXMLFilePath := UpdateXMLWriter;
  if FileExists(UpdateXMLFilePath) then begin
    if CopyFile(PAnsiChar(UpdateXMLFilePath),
                PAnsiChar(TmpDir+'\Update.xml'),
                True)
    then begin
      MessProc('', '���� �������� ���������� Update.xml ������� � "'+TmpDir+'\Update.xml"', False, True, True);
    end
    else begin
      MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� �������� ���� "' + TmpDir + '\Update.xml"', True, True, True, MB_ICONERROR);
    end;
  end;
  IUPCreatorForm.Refresh;
  // >> ����� � �������� � ������� ����� ���� �������� ���������� Update.xml
  Sleep(1000); // ���� � ��� ���������� ����.

  // << �������� Scenario.xml - ���� �������� �������� ���������
  if FileExists(ScenarioXMLPathLabel.Caption) then begin
    if CopyFile(PAnsiChar(ScenarioXMLPathLabel.Caption),
                PAnsiChar(TmpDir+'\Scenario.xml'),
                True)
    then begin
      MessProc('', '���� Scenario.xml ������� � "'+TmpDir+'\Scenario.xml"', False, True, True);
    end
    else begin
      MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� �������� ���� "' + TmpDir + '\Scenario.xml"', True, True, True, MB_ICONERROR);
    end;
  end;
  IUPCreatorForm.Refresh;
  // >> �������� Scenario.xml - ���� �������� �������� ���������
  Sleep(1000);

  Lib.ReplaceSpacesByUnderline(TmpDir, ' ', '_');
  Lib.RemoveEmptyStringFromFilesInFolder(TmpDir, '.sql | ');
  // << ������
 { DONE : ������� ������ ����� ���������� }
  UpdFilePath := TmpDir + '.iup';
  MessProc('', '������������ ���������...', False, True, True);

  if not PackingLib.ManPack(TmpDir, UpdFilePath, XMLWork.GetXMLNodeValue('ARCHIVEAPPPACK'), StrToInt(XMLWork.GetXMLNodeValue('RUNMODE')))
  then begin
    MessProc('������', 'main.pas(IUPCreateBtnClick): ��������� ������ �� ����� ��������� ����������.', True, True, True, MB_ICONERROR);
  end
  else begin
    MessProc('', '���������� ����������. ���� � �����: ' + UpdFilePath, False, True, True);
  end;
  IUPCreatorForm.Refresh;
  // >> ������

  // << ����� ���������� �� ���������� � ������ ����������
  with formatSettings do begin
    DateSeparator   := '.';
    ShortDateFormat := 'yyyy.mm.dd';
  end;
  JournalRecordRes := JournalForm.JournalProc(IUPNameEdit.Text,
                                              ExtractFileName(CrtIUPAct.Dialog.FileName),
                                              DateToStr(IUP_Date.Date, formatSettings),
                                              IUP_Desc_Memo.Text,
                                              IUPVers_Edit.Text,
                                              cbAuthor.Tag,
                                              cbAuthor.Tag,
                                              cbComponent.Tag,
                                              cbClient.Tag);
  if not ( Pos('True.', JournalRecordRes) = 1 ) then begin
    MessProc('��������!', '��������� ������ ������ ������ � ����������� ������: ' + JournalRecordRes,
             False, True, True, MB_ICONWARNING);
  end
  else begin
    MessProc('', '������ �� ���������� "' + IUPNameEdit.Text + '" ��������� � ������ ����������',
             False, True, True);
  end;
  // >> ����� ���������� �� ���������� � ������ ����������

  // << ��������� ���� ���������� � ��������� ������������� �����
  try
    if CopyFile(PAnsiChar(UpdFilePath),
                PAnsiChar(CrtIUPAct.Dialog.FileName),
                False)
    then
      MessProc('', '���������� ���������. ���� � �����: ' + CrtIUPAct.Dialog.FileName, True, True, True)
    else
      MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� �������� ���� ' + CrtIUPAct.Dialog.FileName, True, True, True, MB_ICONERROR);
  IUPCreatorForm.Refresh;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(IUPCreateBtnClick): ������ ���������� ����� '+ e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;
  // >> ��������� ���� ���������� � ��������� ������������� �����

end;

procedure TIUPCreatorForm.CrtIUPActBeforeExecute(Sender: TObject);
begin
  CrtIUPAct.Dialog.FileName := IUPNameEdit.Text;
end;

procedure TIUPCreatorForm.CrtIUPTBtnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  ToolBar1.HotTrackColor := $00676AF3;
end;

procedure TIUPCreatorForm.AboutActExecute(Sender: TObject);
begin
  AboutForm.ShowModal;
end;

procedure TIUPCreatorForm.JournalDBLoginActExecute(Sender: TObject);
begin
  with JournalForm, ReqSelectForm do begin
    if OraLogon then begin
      FillAuthorsList(cbAuthor);
      FillCompsList(cbComponent);
      FillClientsList(cbClient);
      FillIUPsListFromDB;
      FillReqsMemo;
    end;
  end;

end;

procedure TIUPCreatorForm.AddFileBtnClick(Sender: TObject);
var i: integer;
    AddFileDialog: TOpenDialog;
begin
  AddFileDialog := TOpenDialog.Create(Application);

  with AddFileDialog do begin
    Options := Options + [ofAllowMultiSelect, ofFileMustExist];
    Title := '����� ����� �� ������ �������� � ����������?';
  end;

  if not AddFileDialog.Execute then exit;
  WorkPG.Position := 0;
  for i := 0 to (AddFileDialog.Files.Count - 1) do begin
    InsertIntoVT(AddFileDialog.Files[i], '', VT.RootNode);
    WorkPG.Position := WorkPG.Position + Round((WorkPG.Max - 5)/AddFileDialog.Files.Count);
//    MessProc('', '�������� "' + VT.Text[VT.GetLastChild(VT.RootNode), 3] +
//                                VT.Text[VT.GetLastChild(VT.RootNode), 2] + '"', False, True, True);
  end;
  WorkPG.Position := 0;
  MessProc('', '����� ���������', False, True, True);

  if Length(Trim(IUPNameEdit.Text)) = 0 then begin
    IUPNameEdit.Text := ExtractFileName(ExtractFileDir(AddFileDialog.Files[0]));
  end;
  MoveSQLToTop(VT);
end;

procedure TIUPCreatorForm.AddFolderBtnClick(Sender: TObject);
var dir: string;
begin
  if SelectDirectory('�������� �����, ������� �� ������ �������� � ����������.', '\', dir) then begin
    InsertIntoVT(dir, '\', VT.RootNode);
  end;
  MessProc('', '����� ���������', False, True, True);
//  MessProc('', '����� "' + dir + '" ���������', False, True, True);

  if Length(Trim(IUPNameEdit.Text)) = 0 then begin
    IUPNameEdit.Text := ExtractFileName(dir);
  end;
end;

procedure TIUPCreatorForm.ApplicationEvents1Exception(Sender: TObject; E: Exception);
begin
  // ��������� ������������� ����� ��������
  if (E is EDBeditError) and (Sender is TMaskEdit) then
  begin
    MessProc('', '����������, ������� ���������� ������', False, False, True);
//    MessageBox(0, PChar('����������, ������� ���������� ������'), PChar('������!'), MB_ICONWARNING);
//    (Sender as TMaskEdit).SetFocus;
  end;
end;

procedure TIUPCreatorForm.RecompileListBtnClick(Sender: TObject);
var i: integer;
begin
  try
    FilesSelectForm.ShowModal;
    RecompileListMemo.Clear;
    for i := 0 to (FilesSelectForm.SelectedFilesChLB.Count - 1) do begin
      RecompileListMemo.Lines.Add(FilesSelectForm.SelectedFilesChLB.Items[i]);
    end;
    // << ��������� �� ������ � ������ ���� - ������ ��� �������� ������������
    with RecompileListMemo do begin
     selstart := perform( EM_LINEINDEX, 0, 0 );
     perform( EM_SCROLLCARET, 0, 0 );
    end;
    // >> ��������� �� ������ � ������ ���� - ������ ��� �������� ������������
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(RecompileListBtnClick): ������ ������ �� ������� ���� ��� ��������������' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

procedure TIUPCreatorForm.FillReqsMemo();
var
  i: integer;
  cur_vers: string;
  function FieldCheck(Field_: string): string;
  {
    ���� ��������-������ �� �����, �� ���������� � ��.
    ���� �����, �� ���������� ������ ������.
  }
  begin
    Result := Field_;
    if ( Length(Trim(Field_)) = 0 ) then begin
      Result := '';
    end;
  end;
begin
  RequirementsListMemo.Clear;
  with ReqSelectForm do begin
    for I := 1 to jvsgJournalIUPs.RowCount do begin
      cur_vers := jvsgJournalIUPs.Cells[0, i];
      if FieldCheck(cur_vers) <> '' then begin
        RequirementsListMemo.Lines.Add(cur_vers);
      end;
    end;
  end;
end;

procedure TIUPCreatorForm.ReqsListBtnClick(Sender: TObject);
begin
  ReqSelectForm.ShowModal;
  FillReqsMemo;
end;

procedure TIUPCreatorForm.BrowseForFolderActAccept(Sender: TObject);
begin
  InsertIntoVT(BrowseForFolderAct.Folder, '\', VT.RootNode);

  MessProc('', '����� ���������', False, True, True);

  if Length(Trim(IUPNameEdit.Text)) = 0 then begin
    IUPNameEdit.Text := ExtractFileName(BrowseForFolderAct.Folder);
  end;

end;

procedure TIUPCreatorForm.Button1Click(Sender: TObject);
begin
ManPack('Src_', 'Dest_', XMLWork.GetXMLNodeValue('ARCHIVEAPPPACK'), 0);
{
  with formatSettings do begin
    DateSeparator   := '.';
    ShortDateFormat := 'yyyy.mm.dd';
  end;

  JournalForm.JournalProc(IUPNameEdit.Text,
                          '���_�����_���������� ' + DateTimeToStr(Now) + '.iup',
                          DateToStr(IUP_Date.Date, formatSettings),
                          IUP_Desc_Memo.Text,
                          cbAuthor.Tag,
                          cbAuthor.Tag,
                          cbComponent.Tag,
                          cbClient.Tag)
}
  ReqSelectForm.ShowModal;

end;

procedure TIUPCreatorForm.cbAuthorSelect(Sender: TObject);
begin
  cbAuthor.Tag := integer(cbAuthor.Items.Objects[cbAuthor.ItemIndex]);
  MessProc('', '������ ��������� "' + cbAuthor.Items[cbAuthor.ItemIndex] + '"',
           False, True, True);
end;

procedure TIUPCreatorForm.cbClientSelect(Sender: TObject);
begin
  cbClient.Tag := integer(cbClient.Items.Objects[cbClient.ItemIndex]);
  MessProc('', '������ �������� "' + cbClient.Items[cbClient.ItemIndex] + '"',
           False, True, True);
end;

procedure TIUPCreatorForm.cbComponentSelect(Sender: TObject);
begin
  cbComponent.Tag := integer(cbComponent.Items.Objects[cbComponent.ItemIndex]);
  MessProc('', '������� ���������� "' + cbComponent.Items[cbComponent.ItemIndex] + '"',
           False, True, True);
end;

procedure TIUPCreatorForm.UpdateXMLSaveActAccept(Sender: TObject);
var UpdateXMLFilePath: string;
begin
  try
    UpdateXMLFilePath := UpdateXMLWriter;
    if CopyFile(PAnsiChar(UpdateXMLFilePath),
                PAnsiChar(UpdateXMLSaveAct.Dialog.FileName),
                False) then NULL
    else
      MessProc('������', 'main.pas(UpdateXMLSaveActAccept): �� ������� �������� ���� ' + sLineBreak +
                IUPOpenAct.Dialog.FileName, True, True, True, MB_ICONERROR);
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(UpdateXMLSaveActAccept): ������ ������ Update.xml: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;

end;

procedure TIUPCreatorForm.UpdateXMLWriteBtnClick(Sender: TObject);
var UpdateXMLFilePath: string;
    SaveUpdXMLDialog: TSaveDialog;
begin
  try
    UpdateXMLFilePath := UpdateXMLWriter;
    if not FileExists(UpdateXMLFilePath) then exit;

    SaveUpdXMLDialog := TSaveDialog.Create(Application);
    with SaveUpdXMLDialog do begin
      Title := '���� �� ������ ��������� ���� �������� ����������?';
      Filter := '����� ���������� (*.xml)|*.xml';
    end;

    if SaveUpdXMLDialog.Execute then
      if CopyFile(PAnsiChar(UpdateXMLFilePath),
                  PAnsiChar(SaveUpdXMLDialog.FileName),
                  False) then NULL
      else
        MessProc('������', 'main.pas(UpdateXMLWriteBtnClick): �� ������� �������� ���� ' + sLineBreak +
                  SaveUpdXMLDialog.FileName, True, True, True, MB_ICONERROR);
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(UpdateXMLWriteBtnClick): ������ ������ Update.xml: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;
end;

procedure TIUPCreatorForm.DelAllBtnClick(Sender: TObject);
begin
  VT.Clear;
end;

procedure TIUPCreatorForm.DelBtnClick(Sender: TObject);
var Nodes: TNodeArray; // ������, � ������� ����� ������� ���������� ����
    i: integer;
begin
  Nodes := VT.GetSortedSelection(True); // �������� ��� ���������� ���� � ������� ������
  for i := 0 to High(Nodes) do begin
    VT.DeleteNode(Nodes[i]);
  end;
end;

procedure TIUPCreatorForm.ExitActExecute(Sender: TObject);
begin
  IUPCreatorForm.Close;
end;

procedure TIUPCreatorForm.ExploreNFocusFileMIClick(Sender: TObject);
var Elem: PElemNode;
begin
  try
    Elem := VT.GetNodeData(VT.FocusedNode);
    if not Assigned(Elem) then exit;
    ShellExecute(0, 'open', 'explorer.exe', PChar('/select, ' + Elem^.Path), nil, SW_SHOWNORMAL);
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(ExploreNFocusFileMIClick): �� ������� �������� ���� � �����.' + sLineBreak +
                         '������: ' + e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;
end;

procedure TIUPCreatorForm.ExtInstMIClick(Sender: TObject);
var Elem:   PElemNode;
begin
  Elem := VT.GetNodeData(VT.FocusedNode);
  with ExtInstForm do begin
    if Assigned(Elem) then begin
      Caption := '����������� ��������� "' + Elem.FileName + '"';
      InstallMemo.Text := Elem^.Install.Text;
      BackupMemo.Text  := Elem^.BackUp.Text;
      ReturnMemo.Text  := Elem^.Return.Text;
      if ExtInstForm.ShowModal = mrOK then begin
        Elem^.Install.Text := InstallMemo.Text;
        Elem^.BackUp.Text  := BackupMemo.Text;
        Elem^.Return.Text  := ReturnMemo.Text;
        Elem^.Changed := True;
      end;
    end;
  end;
end;

{ DONE : ������������ SQL-����� }

procedure TIUPCreatorForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 if MessageDlg('�� ������������� ������ �����?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
   CanClose := False;
end;

procedure TIUPCreatorForm.FormCreate(Sender: TObject);
//var AuthInfFile: IXMLAUTHOR_INFORMATIONType;
  var i: Integer;

  function CreateProgressBar(SB_: TStatusBar; PanelNum_: integer): TProgressBar;
  begin
    Result := TProgressBar.create(SB_);
    try
      with Result do begin
        parent  := SB_;
        visible := true;
        top     := 2;
        left := SB_.Panels[PanelNum_].width + 1;
        width  := SB_.Panels[PanelNum_].width - 4;
        height := SB_.height - 2;
      end;
    except
      on e:Exception do begin
        Result := nil;
        exit;
      end;
    end;
  end;

begin
  IUPCreatorForm.Caption := 'IUP Creator ' + FileVersion(ParamStr(0));
  WorkPG := CreateProgressBar(SB, 0);
  MessProc('', '����������� �������� � ����� � ������', False, True, True);

  VT.NodeDataSize := SizeOf(TElemNode);

  // �������� ����� �������, ���� ������� ��������������� ��������
  for i := 1 to ParamCount do begin
    if LowerCase(ParamStr(i)) = '/d' then begin
      ScenarioXMLPathLabel.Visible := True;
      Label10.Visible              := True;
      RecompileListMemo.Visible    := True;
      Button1.Visible              := True;
    end;
  end;

  // << ������������� �������������� ����
  IUP_Date.DateTime := Now;
  IUPVers_Edit.Text := FormatDateTime('yyyy.MM.dd.hh.mm.ss', Now);

  try
//    if FileExists(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml') then begin
//      AuthInfFile := LoadAUTHOR_INFORMATION(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml');
//      ScenarioXMLPathLabel.Caption := AuthInfFile.SCENARIO;// ExtractFileDir(ParamStr(0)) + '\Scenario.xml';
//      with AuthInfFile do begin
//        IUP_PhoneEdit.Text := IntToStr(PHONE);
//        IUPAuthor.Text     := AUTHOR;
//        IUP_EMailEdit.Text := MAIL;
//      end;
//    end;

      ScenarioXMLPathLabel.Caption := XMLWork.GetXMLNodeValue('SCENARIO');
      IUP_PhoneEdit.Text           := XMLWork.GetXMLNodeValue('PHONE');
      IUPAuthor.Text               := XMLWork.GetXMLNodeValue('AUTHOR');
      IUP_EMailEdit.Text           := XMLWork.GetXMLNodeValue('MAIL');

  except
    on e:Exception do begin
      MessProc('������', 'main.pas(FormCreate): ������ �������������� �������������� �����' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
  // >> ������������� �������������� ����
end;

procedure TIUPCreatorForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F1 then begin
    AboutForm.ShowModal;
  end;
end;

procedure TIUPCreatorForm.FormResize(Sender: TObject);
begin
  WorkPG.Left  := Round(SB.Width*(2/3));
  WorkPG.Width := Round(SB.Width*(1/3)) - 15;
end;

procedure TIUPCreatorForm.HelpActExecute(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar('./Creator.chm'), nil, nil, SW_SHOWNORMAL);
end;

{ DONE : ��������� �������� � ���������� }
{ DONE : ������ � ����������� ������ � ��� � ����� InsertIntoVT, � �� � ������ � ������ }
function  TIUPCreatorForm.InsertIntoVT(FilePath_, Ext_: string; TargetNode_: PVirtualNode): Boolean;
var objectname: string;
    fileext: string;
    fname: string;
    NewNode: PVirtualNode;
    NewElem: PElemNode;
    OrigSQL, RenamedSQL: string;

    function GetFirstLineFromFile(File_: string): string;
    var f:TextFile;
    begin
      try
        AssignFile(F, File_);
        Reset(f);
        ReadLn(f, Result);
        Result := SysUtils.StringReplace(Result, 'Start of DDL Script for', '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, '--'          , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'View'        , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'Package Body', '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'Package'     , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'Function'    , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'Procedure'   , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'Drop'        , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'Public'      , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, 'Synonym'     , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, '  '          , '', [rfReplaceAll, rfIgnoreCase]);
        Result := SysUtils.StringReplace(Result, '  '          , '', [rfReplaceAll, rfIgnoreCase]);
        CloseFile(f);
      except
        on e:Exception do Result := '';
      end;  
    end;

begin
  Result := True;
  try

    // << ���������, ���� �� ���������� ������� � ������� �, ���� ����, ����������, ��� ������
    fname := ExtractFileName(FilePath_);
    if VTIndexOf(VT, fname) <> -1 then begin
      if MessageDlg('� ������� ��� ���� ������� � ������ "' + fname + '"' + sLineBreak +
                    '�������� "' + VT.Text[VT.FocusedNode, 6] + '" �� "' + FilePath_ + '"?',
                     mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        VT.DeleteNode(VT.FocusedNode);
      end
      else begin
        exit;
      end;
    end;
    // >> ���������, ���� �� ���������� ������� � ������� �, ���� ����, ����������, ��� ������

    // << ��������� ������� �������� � SQL-������
    if (ExtractFileExt(FilePath_) = '.sql') and (Pos(' ', ExtractFileName(FilePath_))<>0) then begin
      OrigSQL    := ExtractFileName(FilePath_);
      RenamedSQL := StringReplace(OrigSQL, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
      MessProc('��������!', '� ����� ����� "'+OrigSQL+'" ������������ �������. ���� ����� ������������ � '+RenamedSQL, True, True, True);
      if not RenameFile(FilePath_, ExtractFileDir(FilePath_)+'\'+RenamedSQL) then begin
        MessProc('��������!', '�� ������� ������������� ���� "'+OrigSQL+'"', True, True, True);
      end;
      FilePath_ := ExtractFileDir(FilePath_)+'\'+RenamedSQL;
    end;
    // >> ��������� ������� �������� � SQL-������

    NewNode := VT.InsertNode(TargetNode_, amInsertAfter);
    NewElem := VT.GetNodeData(NewNode);
    if not Assigned(NewElem) then exit;

    with NewElem^ do begin
      // ���������������� ����� ��� ���������
      InstallNum := VT.RootNodeCount;

      // ����������
      if Ext_ = '' then begin
        fileext := ExtractFileExt(FilePath_);
        System.Delete(fileext, 1, 1);
        Ext        := fileext;
      end
      else begin
        Ext :=  Ext_;
      end;

      // ��� �����
      FileName   := ExtractFileName(FilePath_);

      // ��� �������
      objectname := ExtractFileName(FilePath_);
      Delete(objectname, length(objectname) - length(ExtractFileExt(objectname)) + 1, length(ExtractFileExt(objectname)));
      ObjName := UpperCase(objectname);

      // �������� ��������
      if UpperCase(Ext) = UpperCase('sql') then
        Desc       := GetFirstLineFromFile(FilePath_);

      // ���� � �����
      Path       := FilePath_;

      Changed    := False;

      // << �������� ��� ����������� ���������
      Install := TStringList.Create;
      Install.Add('');
      BackUp := TStringList.Create;
      BackUp.Add('');
      Return := TStringList.Create;
      Return.Add('');
      // >> �������� ��� ����������� ���������

      // << ���� ������ � VT
      VTColor := StringToColor(XMLWork.GetXMLNodeValue(UpperCase(Ext)));
      // >> ���� ������ � VT


      MessProc('', '�������� "' + VT.Text[VT.GetLastChild(VT.RootNode), 3] +
                                  VT.Text[VT.GetLastChild(VT.RootNode), 2] + '"', False, True, True);


    end;

  except
    on e:EConvertError do begin
      NULL;
    end;
    on e:Exception do begin
      Result := False;
      MessProc('������', 'main.pas(InsertIntoVT): ������ ���������� ������� � �������: ' + sLineBreak +
               e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

{ DONE : ������� �������� �������� �� ����� }
{ DONE : �������� ����� � ������ ����������� � ����������������� �� ��� }
//
//procedure TIUPCreatorForm.IUPCreateBtnClick(Sender: TObject);
//var TmpDir, UpdFilePath: string;
//    UpdateXMLFilePath: string;
//    CurNode: PVirtualNode;
//    Elem: PElemNode;
//    SaveIUPFileDialog: TSaveDialog;
//begin
//  if not FileExists(ScenarioXMLPathLabel.Caption) then begin
//    MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� ����� ���� ��������� Scenario.xml', True, True, True, MB_ICONERROR);
//    exit;
//  end;
//  if not isSelectedElements(VT) then begin
//    MessProc('������', 'main.pas(IUPCreateBtnClick): �������, ����� ����� �� ������ �������� � ����������', True, True, True, MB_ICONERROR);
//    exit;
//  end;
//  if Length(Trim(IUPNameEdit.Text)) = 0 then begin
//    MessProc('������', 'main.pas(IUPCreateBtnClick): ����������, ������� ������������ ����������', True, True, True, MB_ICONERROR);
//    IUPNameEdit.SetFocus;
//    exit;
//  end;
//
//  // << ������ ���������� ��������� �����
//  try
//    TmpDir := GetEnvironmentVariable('TEMP') + '\IUPCreator_' + FormatDateTime('yyyy-mm-dd_(hh-mm-ss)', Now);
//    MessProc('', '��������� (�������) ����������: ' + TmpDir, False, True, True);
//    if not DirectoryExists(TmpDir) then
//      if SysUtils.ForceDirectories(TmpDir) then begin// ���� ����������, ��� ����� ������������ ��������
//        MessProc('', '������� ������� �����: '+ TmpDir, False, True, True);
//      end
//      else begin
//        MessProc('', '�� ������� ������� ����� "'+ TmpDir + '": '+IntToStr(GetLastError)+' - '+SysErrorMessage(GetLastError),
//                 True, True, True);
//      end;
//  except
//    on e:Exception do begin
//      MessProc('������', 'main.pas(IUPCreateBtnClick): ������ �������� ������� �����: ' + sLineBreak +
//                e.Message, True, True, True, MB_ICONERROR);
//      exit;
//    end;
//  end;
//  // >> ������ ���������� ��������� �����
//
//  // << �������� ������� ���������� ���������� � ������� (���������) �����
//  try
//    if VT.RootNodeCount = 0 then exit;
//    WorkPG.Position := 0;
//    CurNode := VT.GetFirst(False);
//    if not Assigned(CurNode) then exit;
//    repeat
//      Elem := VT.GetNodeData(CurNode);
//      if Assigned(Elem) then begin
//        if not CopyMan(Elem.Path, TmpDir) then begin
//          MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� ��������� ����������� "'+Elem.Path+'" �� ��������� ����������', True, True, True, MB_ICONERROR);
//        end;
//        WorkPG.Position := WorkPG.Position + Round((WorkPG.Max - 5)/VT.RootNodeCount);
//        MessProc('', '��������� "'+ Elem.FileName + Elem.Ext + '"', False, True, True);
//      end;
//      CurNode := VT.GetNext(CurNode, False);
//    until CurNode = nil;
//    WorkPG.Position := 0;
//    MessProc('', '����� ����������� �� ��������� �����', False, True, True);
//  except
//    on e:Exception do begin
//      MessProc('������', 'main.pas(IUPCreateBtnClick): ������ ����������� ������ �� ��������� (�������) ����������: ' + sLineBreak +
//                e.Message, True, True, True, MB_ICONERROR);
//      exit;
//    end;
//  end;
//  // >> �������� ������� ���������� ���������� � ������� (���������) �����
//
//  // << ����� � �������� � ������� ����� ���� �������� ���������� Update.xml
//  UpdateXMLFilePath := UpdateXMLWriter;
//  if FileExists(UpdateXMLFilePath) then begin
//    if CopyFile(PAnsiChar(UpdateXMLFilePath),
//                PAnsiChar(TmpDir+'\Update.xml'),
//                True)
//    then begin
//      MessProc('', '���� �������� ���������� Update.xml ������� � "'+TmpDir+'\Update.xml"', False, True, True);
//    end
//    else begin
//      MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� �������� ���� "' + TmpDir + '\Update.xml"', True, True, True, MB_ICONERROR);
//    end;
//  end;
//  // >> ����� � �������� � ������� ����� ���� �������� ���������� Update.xml
//
//  Sleep(1000); // ���� � ��� ���������� ����.
//
//  // << �������� Scenario.xml - ���� �������� �������� ���������
//  if FileExists(ScenarioXMLPathLabel.Caption) then begin
//    if CopyFile(PAnsiChar(ScenarioXMLPathLabel.Caption),
//                PAnsiChar(TmpDir+'\Scenario.xml'),
//                True)
//    then begin
//      MessProc('', '���� Scenario.xml ������� � "'+TmpDir+'\Scenario.xml"', False, True, True);
//    end
//    else begin
//      MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� �������� ���� "' + TmpDir + '\Scenario.xml"', True, True, True, MB_ICONERROR);
//    end;
//  end;
//  // >> �������� Scenario.xml - ���� �������� �������� ���������
//
//  Sleep(1000);
//
//  if not VersionRecording(StrToInt(IUPCreatorForm.VersYear.Text),
//                          StrToInt(IUPCreatorForm.VersUpd.Text),
//                          StrToInt(IUPCreatorForm.VersPatch.Text),
//                          StrToInt(IUPCreatorForm.VersStatus.Text)) then
//  begin
//    exit;
//  end;
//
//  Sleep(1000);
//
//  Lib.ReplaceSpacesByUnderline(TmpDir, ' ', '_');
//  Lib.RemoveEmptyStringFromFilesInFolder(TmpDir, '.sql | ');
//
//  // << ������
// { DONE : ������� ������ ����� ���������� }
//  UpdFilePath := TmpDir + '.iup';
//  MessProc('', '������������ ���������...', False, True, True);
//
//  if not PackingLib.ManPack(TmpDir, UpdFilePath,
//                            AuthorInf.LoadAUTHOR_INFORMATION(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml').APPRUNMODE)
//  then begin
//    MessProc('������', 'main.pas(IUPCreateBtnClick): ��������� ������ �� ����� ��������� ����������.', True, True, True, MB_ICONERROR);
//  end
//  else begin
//    MessProc('', '���������� ����������. ���� � �����: ' + UpdFilePath, False, True, True);
//  end;
//  // >> ������
//
//  // << ��������� ���� ���������� � ��������� ������������� �����
//  try
//    SaveIUPFileDialog := TSaveDialog.Create(self);
//
//    with SaveIUPFileDialog do begin
//      Filter     := '����� ���������� (*.iup)|*.iup';
//      Title      := '���� �� ������ ��������� ��������� ����������?';
//      DefaultExt := 'iup';
//      FileName   := IUPNameEdit.Text;
//    end;
//
//    if not FileExists(UpdFilePath) then
//      MessProc('������', 'main.pas(IUPCreateBtnClick): ����� ' + UpdFilePath + ' �� ����������', True, True, True, MB_ICONERROR);
//
//    if SaveIUPFileDialog.Execute(0) then
//      if CopyFile(PAnsiChar(UpdFilePath),
//                  PAnsiChar(SaveIUPFileDialog.FileName),
//                  False)
//      then
//        MessProc('', '���������� ���������. ���� � �����: ' + SaveIUPFileDialog.FileName, False, True, True)
//      else
//        MessProc('������', 'main.pas(IUPCreateBtnClick): �� ������� �������� ���� ' + SaveIUPFileDialog.FileName, True, True, True, MB_ICONERROR);
//    FreeAndNil(SaveIUPFileDialog);
//
////   if MessageDlg('������� ��� ���� ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
////     ShellExecute(0, 'open', PChar(ParamStr(0)), nil, nil, SW_SHOWNORMAL);
////     Application.Terminate;
////   end;
//
//  except
//    on e:Exception do begin
//      MessProc('������', 'main.pas(IUPCreateBtnClick): ������ ���������� ����� '+ e.Message, True, True, True, MB_ICONERROR);
//      exit;
//    end;
//  end;
//  // >> ��������� ���� ���������� � ��������� ������������� �����
//
//end;
//

procedure TIUPCreatorForm.IUPOpenActAccept(Sender: TObject);
var TEMP, IUP_file: string; // ������� ����� � ������������� ���� ����������
    OpenIUPFileDialog: TOpenDialog;
    archtypes: TStrings;
begin
  try
    TEMP := GetEnvironmentVariable('TEMP')+'\IUPCreator\';
    IUP_file := IUPOpenAct.Dialog.FileName;
    // << ������������� � ����������� �� ���� ������
    archtypes := TStringList.Create;
    with archtypes do begin
      Add('rar');
      Add('int');
      case IndexOf(DetectArchType(IUP_file)) of
        0: begin
             if not ManUnpack(IUP_file, TEMP, XMLWork.GetXMLNodeValue('ARCHIVEAPPUNPACK'), StrToInt(XMLWork.GetXMLNodeValue('RUNMODE')))
             then MessProc('������', 'main.pas(LoadIUPBtnClick): ��������� ������ �� ����� ���������� ����������', True, True, True, MB_ICONERROR)
             else MessProc('', '���������� ����������� � ' + TEMP, False, True, True);
           end;
        1: begin
             if   FileExists(IUP_file) then begin
               FolderActions.De_DoFolderAction(TEMP, OpenIUPFileDialog.FileName, DODO3)
             end
             else begin
               MessProc('������', '����� "' + IUP_file + '" �� ����������', True, True, True, MB_ICONERROR);
               exit;
             end;
             if CS_check(TEMP) <> True then
               MessProc('������', 'main.pas(LoadIUPBtnClick): ���� ���������� ��������', False, True, True, MB_ICONERROR);
           end;
      end;
    end;
    // >> ������������� � ����������� �� ���� ������

    FillGridWithUpdateXML(VT, Lib.ForceFolderPath(TEMP)+'Update.xml');
    MessProc('', '���������� "' + IUP_file + '" ���������', False, True, True);

    if MessageDlg('�������� ������ � ������� ���������� ����������� �� ��������������?',
                   mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      FillIUPInfoWithUpdateXML(Lib.ForceFolderPath(TEMP)+'Update.xml');
    end;
    SysUtils.DeleteFile(Lib.ForceFolderPath(TEMP)+'Update.xml');


    MoveSQLToTop(VT);

  except
    on e:Exception do begin
      MessProc('������', 'main.pas(LoadIUPBtnClick): ������ ���������� ����������: '+ e.Message, True, True, True, MB_ICONERROR);
    end;
  end;

end;

procedure TIUPCreatorForm.IUPVers_EditKeyPress(Sender: TObject; var Key: Char);
begin
  // � ���� ������ ����� ������� ������ ����� � �����. 
  if  not (Key in ['0'..'9', '.'])
  and not (ord(Key)=VK_Back) then
    key:=#0;
end;

procedure TIUPCreatorForm.IUP_Desc_MemoDblClick(Sender: TObject);
var
  TheMStream : TMemoryStream;
  Zero : char;
  OpenDescFileExtDialog: TOpenTextFileDialog;
  Elem:   PElemNode;
  old_text: string;
begin
  TheMStream := TMemoryStream.Create;

  OpenDescFileExtDialog := TOpenTextFileDialog.Create(Application);

  with OpenDescFileExtDialog do begin
    FileName   := '#done.txt';
    Filter     := '��������� ����� (*.txt)|*.txt';
    InitialDir := 'C:\';

    Elem := VT.GetNodeData(VT.FocusedNode);
    if Assigned(Elem) then begin
      InitialDir := ExtractFilePath(Elem^.Path);
    end;
  end;

  if OpenDescFileExtDialog.execute then begin
    TheMStream.LoadFromFile(OpenDescFileExtDialog.FileName);
    TheMStream.Seek(0, soFromEnd);
    //����� ����������� ����!
    Zero := #0;
    TheMStream.Write(Zero, 1);
    TheMStream.Seek(0, soFromBeginning);


    if Length(IUP_Desc_Memo.Text) = 0 then begin
      IUP_Desc_Memo.Lines.LoadFromStream(TheMStream);
      TheMStream.Free;
      exit;
    end;

    case MessageDlg('��������� ������� �������� ���������� ������� �� ������������ ����� (������� ������ "No" ������� � ������ ������ ���������� �� ������)?',
                     mtConfirmation, mbYESNO, 0) of
      mrYes: begin
        old_text := IUP_Desc_Memo.Text;
        IUP_Desc_Memo.Lines.LoadFromStream(TheMStream);
        IUP_Desc_Memo.Text := old_text + sLineBreak + Trim(IUP_Desc_Memo.Text);
      end;
      mrNo:  begin
        IUP_Desc_Memo.Lines.LoadFromStream(TheMStream);
        IUP_Desc_Memo.Text := Trim(IUP_Desc_Memo.Text);
      end;
    end;
    TheMStream.Free;
  end;
end;

function DODO2(fl1,fl2:string):boolean;
begin
  Result:=ZLATCompressFile(fl1,fl2,2,false,true,nil,0,0);
end;

function DODO(fl1,fl2:string):boolean;
begin
  Result:=CopyFile(pchar(fl1),pchar(fl2),false);
end;

procedure TIUPCreatorForm.LoadArchIUPBtnClick(Sender: TObject);
var TEMP, IUP_file: string; // ������� ����� � ������������� ���� ����������
//    SearchRec: TSearchRec;
    OpenIUPFileDialog: TOpenDialog;
begin
  try
    TEMP := GetEnvironmentVariable('TEMP')+'\IUPCreator\';

    OpenIUPFileDialog := TOpenDialog.Create(Application);
    with OpenIUPFileDialog do begin
      Filter := '����� ���������� (*.iup)|*.iup';
      if Execute then begin
        IUP_file := FileName;
      end
      else begin
        exit;
      end;
    end;

    // << �������������
    if not PackingLib.ManUnpack(IUP_file, TEMP, XMLWork.GetXMLNodeValue('ARCHIVEAPPUNPACK'), StrToInt(XMLWork.GetXMLNodeValue('RUNMODE')))
    then begin
      MessProc('������', 'main.pas(LoadArchIUPBtnClick): ��������� ������ �� ����� ���������� ����������.', True, True, True, MB_ICONERROR);
    end
    else begin
      MessProc('', '���������� ����������� � ' + TEMP, False, True, True);
    end;
    // >> �������������

    FillGridWithUpdateXML(VT, Lib.ForceFolderPath(TEMP)+'Update.xml');

  except
    on e:Exception do begin
      MessProc('������', 'main.pas(LoadArchIUPBtnClick): ������ ���������� ����������: '+ sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

{ DONE : ��������������� ���� ������ }
procedure TIUPCreatorForm.LoadIUPBtnClick(Sender: TObject);
var TEMP, IUP_file: string; // ������� ����� � ������������� ���� ����������
    OpenIUPFileDialog: TOpenDialog;
    archtypes: TStrings;
begin
  try
    // << �������� ������ ������� ���������� � ����� ����������
    TEMP := GetEnvironmentVariable('TEMP')+'\IUPCreator\';
    OpenIUPFileDialog := TOpenDialog.Create(Application);
    with OpenIUPFileDialog do begin
      Filter := '����� ���������� (*.iup)|*.iup';
      if Execute then begin
        IUP_file := FileName;
      end
      else begin
        exit;
      end;
    end;
    // >> �������� ������ ������� ���������� � ����� ����������

    // << ������������� � ����������� �� ���� ������
    archtypes := TStringList.Create;
    with archtypes do begin
      Add('rar');
      Add('int');
      case IndexOf(DetectArchType(IUP_file)) of
        0: begin
             if not ManUnpack(IUP_file, TEMP, XMLWork.GetXMLNodeValue('ARCHIVEAPPUNPACK'), StrToInt(XMLWork.GetXMLNodeValue('RUNMODE')))
             then MessProc('������', 'main.pas(LoadIUPBtnClick): ��������� ������ �� ����� ���������� ����������', True, True, True, MB_ICONERROR)
             else MessProc('', '���������� ����������� � ' + TEMP, False, True, True);
           end;
        1: begin
             if   FileExists(IUP_file) then begin
               De_DoFolderAction(TEMP, OpenIUPFileDialog.FileName, DODO3)
             end
             else begin
               MessProc('������', '����� "' + IUP_file + '" �� ����������', True, True, True, MB_ICONERROR);
               exit;
             end;
             if CS_check(TEMP) <> True then
               MessProc('������', 'main.pas(LoadIUPBtnClick): ���� ���������� ��������', False, True, True, MB_ICONERROR);
           end;
      end;
    end;
    // >> ������������� � ����������� �� ���� ������
    FillGridWithUpdateXML(VT, Lib.ForceFolderPath(TEMP)+'Update.xml');
    MessProc('', '���������� "' + IUP_file + '" ���������', False, True, True);

    if MessageDlg('�������� ������ � ������� ���������� ����������� �� ��������������?',
                   mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      FillIUPInfoWithUpdateXML(Lib.ForceFolderPath(TEMP)+'Update.xml');
    end;
    SysUtils.DeleteFile(Lib.ForceFolderPath(TEMP)+'Update.xml');


    MoveSQLToTop(VT);
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(LoadIUPBtnClick): ������ ���������� ����������: '+ e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

procedure TIUPCreatorForm.LoadOldIUPBtnClick(Sender: TObject);
var TEMP, IUP_file: string; // ������� ����� � ������������� ���� ����������
//    SearchRec: TSearchRec;
    OpenIUPFileDialog: TOpenDialog;
begin
  try
    TEMP := GetEnvironmentVariable('TEMP')+'\IUPCreator\';

    OpenIUPFileDialog := TOpenDialog.Create(Application);

    OpenIUPFileDialog.Filter := '����� ���������� (*.iup)|*.iup';

    if OpenIUPFileDialog.Execute then
      IUP_file := OpenIUPFileDialog.FileName
    else
      exit;

    FillGridWithUpdateXML(VT, Lib.ForceFolderPath(TEMP)+'Update.xml');

  except
    on e:Exception do begin
      MessProc('������', 'main.pas(LoadOldIUPBtnClick): ������ ���������� ����������: '+ e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

procedure TIUPCreatorForm.LogSaveActAccept(Sender: TObject);
begin
  try
    LogMemo.Lines.SaveToFile(LogSaveAct.Dialog.FileName);
    MessProc('', '��� ������ �������� � "' + LogSaveAct.Dialog.FileName + '"', True, True, True);
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(LogSaveActAccept): ������ ������ ���� ������: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;
end;

procedure TIUPCreatorForm.LogSaveActBeforeExecute(Sender: TObject);
var now_str: string;
begin
  DateTimeToString(now_str, 'yyyy.mm.dd_hh-mm-ss', Now);


  with LogSaveAct.Dialog do begin
    Title    := '���� �� ������ ��������� ��� ������ Interin Update Creator?';
    Filter   := '��������� ����� (*.txt)|*.txt';
    DefaultExt := '.txt';
    FileName := now_str;
  end;



end;

procedure TIUPCreatorForm.OpenInAppMIClick(Sender: TObject);
var Elem:    PElemNode;
    execres: HINST;
begin
  Elem := VT.GetNodeData(VT.FocusedNode);
  if Assigned(Elem) then begin
    execres := ShellAPI.ShellExecute(Handle, 'open', PChar('"'+Elem^.Path+'"'), nil, nil, SW_SHOWNORMAL);
    case execres of
      SE_ERR_ASSOCINCOMPLETE: begin
        MessProc('', '����������, ��������������� � ������ "' + Elem^.Path + '" �� �������.' + sLineBreak +
                     '������� ��� � Windows-��������� ����� (IUP Creator ��������� �� ����).', True, True, True);
        exit;
      end;
      SE_ERR_NOASSOC: begin
        MessProc('', '���� "' + Elem^.Path + '" �� ������������ � �����-���� �����������.' + sLineBreak +
                     '������� ��� � Windows-��������� ����� (IUP Creator ��������� �� ����).', True, True, True);
        exit;
      end;
    end;
  end;
end;

procedure TIUPCreatorForm.OpenInNotepadMIClick(Sender: TObject);
var Elem:   PElemNode;
begin
  Elem := VT.GetNodeData(VT.FocusedNode);
  if Assigned(Elem) then begin
    ShellAPI.ShellExecute(Handle, 'open', 'notepad.exe', PChar('"'+Elem^.Path+'"'), nil, SW_SHOWNORMAL);
  end;
end;

procedure TIUPCreatorForm.OptActExecute(Sender: TObject);
begin
  OptionsForm.ShowModal;
end;

procedure TIUPCreatorForm.SaveLogBtnClick(Sender: TObject);
var SaveLogDialog: TSaveDialog;
    tmpstr: string;
begin
  try
    SaveLogDialog := TSaveDialog.Create(Application);
    with SaveLogDialog do begin
      Title    := '���� �� ������ ��������� ��� ������ Interin Update Creator?';
      Filter   := '��������� ����� (*.txt)|*.txt';
      DateTimeToString(tmpstr, 'yyyy.mm.dd_hh:mm:ss', Now);
//      FileName := 'IUP_Creator_log_' + tmpstr;
      DefaultExt := '.txt';
    end;

    if not SaveLogDialog.Execute then exit;
    LogMemo.Lines.SaveToFile(SaveLogDialog.FileName);
    MessProc('', '��� ������ �������� � "'+SaveLogDialog.FileName+'"', True, True, True);
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(SaveLogBtnClick): ������ ������ ���� ������: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
      exit;
    end;
  end;
end;

procedure TIUPCreatorForm.ScenarioXMLLoadBtnClick(Sender: TObject);
var OpenScenarioFileDialog: TOpenDialog;
begin
  try
    OpenScenarioFileDialog := TOpenDialog.Create(Application);

    with OpenScenarioFileDialog do begin
      Options := Options - [ofAllowMultiSelect];
      InitialDir := ExtractFileDir(ParamStr(0));
      Title := '�������� ����� Scenarion.xml';
      Filter := '���� �������� ��������� ������|*.xml';
      Filename := 'Scenario.xml';
      DefaultExt := 'xml';
    end;

    if OpenScenarioFileDialog.Execute then begin
      ScenarioXMLPathLabel.Caption := OpenScenarioFileDialog.FileName;
    end;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(ScenarioXMLLoadBtnClick): ������ �������� ����� ��������: '+ e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

procedure TIUPCreatorForm.ScenLoadActAccept(Sender: TObject);
begin
  ScenarioXMLPathLabel.Caption := ScenLoadAct.Dialog.FileName;
end;

procedure TIUPCreatorForm.ToolBar1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  ToolBar1.HotTrackColor := $00EFD3C6;
end;

function  TIUPCreatorForm.UpdateXMLWriter(): string;
{
  ������� ����� ���� �������� ���������� Update.xml � ��������� ��� �� ���������
  �����.
  ������������ ��������: ������ ���� � ����� Update.xml.
}
var UpdateXMLFile: IXMLUPDATEType;
    i: integer;
    UpdateXMLFilePath: string;
    CurNode: PVirtualNode;
    Elem: PElemNode;

    function GetReqs(ReqFile_: string): TStrings;
    {
      ������� ������ ���� #requirements.txt � ���������� ��� ����������.
      ������������ ��������: TStrings, ������ ������ - ���� ������ �� �����.
    }
    begin
      if FileExists(ReqFile_) then
        NULL;
    end;

begin
  try
    UpdateXMLFile := NewUPDATE();
    with UpdateXMLFile do begin
      OwnerDocument.Options := OwnerDocument.Options + [doNodeAutoIndent];

      DATE    := DateToStr(IUP_Date.Date);
      AUTHOR  := IUPAuthor.Text;
      EMAIL   := IUP_EMailEdit.Text;
      PHONE   := IUP_PhoneEdit.Text;
{   ��������� ����, 2011,02,14. ��������� �� ����� ������ ������!
      VERSION := VersYear.Text  + '.' +
                 VersUpd.Text   + '.' +
                 VersPatch.Text + '.' +
                 VersStatus.Text;
}
      VERSION     := IUPVers_Edit.Text;
      UPDATE_NAME := IUPNameEdit.Text;
      DESCRIPTION := IUP_Desc_Memo.Text;
    end;

    // << ���������� ������ ������, ������� ���� ����� ����������������� ��� ���������.
    for i := 0 to RecompileListMemo.Lines.Count - 1 do begin
      UpdateXMLFile.COMPILE_FORMS.Add(RecompileListMemo.Lines[i]);
    end;
    // >> ���������� ������ ������, ������� ���� ����� ����������������� ��� ���������.

    // << ���������� �����������.
    for i := 0 to RecompileListMemo.Lines.Count - 1 do begin
      UpdateXMLFile.REQUIREMENTS.Add(RequirementsListMemo.Lines[i]);
    end;
    // >> ���������� �����������.

    if VT.RootNodeCount = 0 then exit;
    CurNode := VT.GetFirst(False);
    if not Assigned(CurNode) then exit;
    repeat
      Elem := VT.GetNodeData(CurNode);
      with UpdateXMLFile.ITEM.Add do begin
      { ��������� ������ ������� ����������� ������ ��� ��������� ����� �������������� ��������������,
        ������ ������ �� ������� ����� ������ ����� ������}
//        SEQ         := Elem.InstallNum;
//        OwnerDocument.Options := OwnerDocument.Options + [doNodeAutoIndent];
        SEQ         := Elem.ListNum;
        OBJECT_TYPE := Elem.Ext;
        FILE_NAME   := Elem.FileName;
        OBJECT_NAME := Elem.ObjName;
        DESCRIPTION := Elem.Desc;

        // << ����� ������������� ���������, ���� ������� ����
        if Length(Trim(Elem.Install.Text)) <> 0 then begin
          EXTINS.INSTALL.NUM := Elem.Install.Count;
          for i := 0 to (Elem.Install.Count - 1) do begin
            EXTINS.INSTALL.OPERATION.ADD(Elem.Install.Strings[i]);
          end;
        end;
        if Length(Trim(Elem.BackUp.Text)) <> 0 then begin
          EXTINS.BACK_UP.NUM := Elem.BackUp.Count;
          for i := 0 to (Elem.BackUp.Count - 1) do begin
            EXTINS.BACK_UP.OPERATION.ADD(Elem.BackUp.Strings[i]);
          end;
        end;
        if Length(Trim(Elem.Return.Text)) <> 0 then begin
          EXTINS.RETURN.NUM := Elem.Return.Count;
          for i := 0 to (Elem.Return.Count - 1) do begin
            EXTINS.Return.OPERATION.ADD(Elem.Return.Strings[i]);
          end;
        end;
        // >> ����� ������������� ���������, ���� ������� ����
      end;
      CurNode := VT.GetNext(CurNode, False);
    until CurNode = nil;
    // ���-�� ��������� "C:\Documents and Settings\User\Local Settings\Temp\Update_30.07.2009.xml"
    UpdateXMLFilePath := GetEnvironmentVariable('TEMP') + '\Update_' + DateToStr(Now) + '.xml';
    UpdateXMLFile.OwnerDocument.SaveToFile(UpdateXMLFilePath);
    Result := UpdateXMLFilePath;
  except
    on e:Exception do begin
      Result := 'main.pas(UpdateXMLWriter): ������ ������ ����� Update.xml' + sLineBreak + e.Message;
      MessProc('������', Result, True, True, True, MB_ICONERROR);
    end;
  end;

end;

procedure TIUPCreatorForm.VersUpdChange(Sender: TObject);
var VersRepPath: string;
    VersionRepositoryFile: IXMLREPOSITORYType;
    index: integer;
//    AuthInfFile: IXMLAUTHOR_INFORMATIONType;
begin
  if (VersUpd.Text = '') then exit;
  try
//    AuthInfFile := LoadAUTHOR_INFORMATION(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml');

    VersRepPath := XMLWork.GetXMLNodeValue('VERS_PATH');
    if FileExists(VersRepPath) then begin
      VersionRepositoryFile := LoadREPOSITORY(VersRepPath);
    end
    else exit;
    index := IndexByMAINValue(VersRepPath, StrToInt(VersUpd.Text));
    MessProc('', '���� � ����� ������: '      + VersRepPath,     False, True, True);
    MessProc('', '������� �������� MAIN: '    + VersUpd.Text,    False, True, True);
    MessProc('', '������� �������� �������: ' + IntToStr(index), False, True, True);


    if (index >= 0) then begin
      with VersionRepositoryFile.UPDATE_ITEM[index] do begin
        VersYear.Text   := IntToStr(YEAR);
        VersUpd.Text    := IntToStr(MAIN);
        VersPatch.Text  := IntToStr(PATCH);
        VersStatus.Text := IntToStr(STATUS);
      end;
    end;
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(VersUpdChange): ������ ��������������� ������: ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

procedure TIUPCreatorForm.VTChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  with Sender do
  begin
    if Assigned(Node) then begin
      // ������� ����� ����� ��������������� ����. ��� �� �� ��������� � �����,
      // (��������, ��������) ����� ������� �������������� �������.
      VT.Selected[Node] := True;
      VT.EditNode(Node, Sender.FocusedColumn);
    end;
  end;
end;

procedure TIUPCreatorForm.VTCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode;
  Column: TColumnIndex; var Result: Integer);
var Data1, Data2: PElemNode;
begin
  Data1 := Sender.GetNodeData(Node1);
  Data2 := Sender.GetNodeData(Node2);

  case Column of
    2: Result := WideCompareStr(Data1.Ext,      Data2.Ext);
    3: Result := WideCompareStr(Data1.FileName, Data2.FileName);
    4: Result := WideCompareStr(Data1.ObjName,  Data2.ObjName);
    5: Result := WideCompareStr(Data1.Desc,     Data2.Desc);
    6: Result := WideCompareStr(Data1.Path,     Data2.Path);
  end;
end;

procedure TIUPCreatorForm.VTCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; out EditLink: IVTEditLink);
begin
  EditLink := TVTCustomEditor.Create;
end;

{ TODO : ������� ������� ����� }
{ DONE : ��� ��������� �� �������� ��������� ��� � ��������������� ���������� }

procedure TIUPCreatorForm.VTDragAllowed(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := True;
end;

procedure TIUPCreatorForm.VTDragDrop(Sender: TBaseVirtualTree; Source: TObject;
  DataObject: IDataObject; Formats: TFormatArray; Shift: TShiftState; Pt: TPoint;
  var Effect: Integer; Mode: TDropMode);
var Nodes: TNodeArray; // ������, � ������� ����� ������� ���������� ����
    i, j: integer;
    DropFilesList: TStringList;
begin
  for i := 0 to High(Formats) do begin
    if Formats[i] = CF_HDROP then begin           // ����� �� Explorer'a...
        DropFilesList := GetDropFilesList(DataObject, Sender as TVirtualStringTree);
        for j := 0 to (DropFilesList.Count - 1) do begin
          if DirectoryExists(DropFilesList.Strings[j]) then
            InsertIntoVT(DropFilesList.Strings[j], '\', Sender.DropTargetNode)
          else
            InsertIntoVT(DropFilesList.Strings[j], '', Sender.DropTargetNode);
        end;
        MessProc('', '����� ���������', False, True, True);

        if Length(Trim(IUPNameEdit.Text)) = 0 then begin
          IUPNameEdit.Text := ExtractFileName(ExtractFileDir(DropFilesList.Strings[0]));
        end;
        MoveSQLToTop(VT);
    end
    else begin
      if Formats[i] = CF_VIRTUALTREE then begin    // ���� ����� VT
        if Sender.DropTargetNode = nil then Exit; // ������ ������ ���� ���� �� ����
        Nodes := Sender.GetSortedSelection(True); // ���������� ��� ���������� ����
        for j := 0 to High(Nodes) do begin
          Sender.MoveTo(Nodes[j], Sender.DropTargetNode, amInsertAfter, False);
        end;
      end;
    end;
  end;
end;

procedure TIUPCreatorForm.VTDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState;
  State: TDragState; Pt: TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
begin
  Accept := True;
  if Source = VT then // ���� ������������� ���� � ������, �� �������������: drop'��� ���� ��� �� ���� ������
    Accept := Sender.DropTargetNode <> Sender.FocusedNode;
end;

procedure TIUPCreatorForm.VTEditing(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := (Column = 1) or (Column = 4) or (Column = 5);
end;

procedure TIUPCreatorForm.VTGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: WideString);
begin
  HintText := VT.Text[Node, Column];
end;

procedure TIUPCreatorForm.VTGetPopupMenu(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; const P: TPoint; var AskParent: Boolean; var PopupMenu: TPopupMenu);
begin
  PopupMenu := NodeMenu;
end;

procedure TIUPCreatorForm.VTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
var
  ElemNode: PElemNode;
begin
  ElemNode := Sender.GetNodeData(Node);
  if not Assigned(ElemNode) then exit;
  case Column of
    0: CellText := IntToStr(ElemNode^.ListNum);
    1: CellText := IntToStr(ElemNode^.InstallNum);
    2: CellText := ElemNode^.Ext;
    3: CellText := ElemNode^.FileName;
    4: CellText := ElemNode^.ObjName;
    5: CellText := ElemNode^.Desc;
    6: CellText := ElemNode^.Path;
  end;
end;

procedure TIUPCreatorForm.VTHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
begin
  if HitInfo.Button = mbLeft then begin
    // ������ ������ ����������� ������� �� ������ �������,
    // ������� ���� ������.
    VT.Header.SortColumn := HitInfo.Column;
    // ��������� ��� ������ ������������ ���� �������
    // � �������� ������� ���������� �� ��������������
    if VT.Header.SortDirection = sdAscending then begin
      VT.Header.SortDirection := sdDescending;
      VT.SortTree(HitInfo.Column, VT.Header.SortDirection);
    end
    else begin
      VT.Header.SortDirection := sdAscending;
      VT.SortTree(HitInfo.Column, VT.Header.SortDirection);
    end;
  end;
end;

procedure TIUPCreatorForm.VTInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
  var InitialStates: TVirtualNodeInitStates);
begin
  Node.CheckType := ctButton;
end;

procedure TIUPCreatorForm.VTNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; NewText: WideString);
var ElemNode: PElemNode;
begin
  ElemNode := VT.GetNodeData(Node);
  if Assigned(ElemNode) then begin
    case Column of
      1: begin
           if not ElemNode^.Changed then
             ElemNode^.Changed    := IntToStr(ElemNode^.InstallNum) <> NewText;
           ElemNode^.InstallNum := StrToInt(NewText);
      end;
      4: begin
           if not ElemNode^.Changed then
             ElemNode^.Changed := UpperCase(ElemNode^.ObjName) <> UpperCase(NewText);
           ElemNode^.ObjName := NewText;
      end;
      5: begin
           if not ElemNode^.Changed then
             ElemNode^.Changed := UpperCase(ElemNode^.Desc) <> UpperCase(NewText);
           ElemNode^.Desc    := NewText;
      end;
    end;
  end;
end;

procedure TIUPCreatorForm.VTPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType);
var
  ElemNode: PElemNode;
begin
  ElemNode := Sender.GetNodeData(Node);
  if not Assigned(Node) then exit;

  try
    if ElemNode.Changed then begin
      TargetCanvas.Font.Style := [fsBold];

      // << ���� ���� ��������������� ������������, �� �������� ������� ��������
      if ((Length(Trim(ElemNode.Install.Text)) <> 0)  or
          (Length(Trim(ElemNode.BackUp.Text))  <> 0)  or
          (Length(Trim(ElemNode.Return.Text))  <> 0))
      then begin
        TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsItalic];
      end
      else begin
        TargetCanvas.Font.Style := TargetCanvas.Font.Style - [fsItalic];
      end;
      // >> ���� ���� ��������������� ������������, �� �������� ������� ��������
    end;

    ElemNode.ListNum := Node.Index+1;

    // << ������������ ���� � ����������� �� ���� �����
//    if      UpperCase(ElemNode.Ext) = 'SQL' then TargetCanvas.Font.Color := $003399
//    else if UpperCase(ElemNode.Ext) = 'FMB' then TargetCanvas.Font.Color := clBlue
//    else if UpperCase(ElemNode.Ext) = 'TXT' then TargetCanvas.Font.Color := clGray;
    TargetCanvas.Font.Color := ElemNode.VTColor;
    // >> ������������ ���� � ����������� �� ���� �����

  except
    on e:EConvertError do begin
      NULL;
    end;
    on e:Exception do begin
      MessProc('������', 'main.pas(VTPaintText): ������ ��������� ������ � �������' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

procedure TIUPCreatorForm._RecompileListBtnClick(Sender: TObject);
var i: integer;
begin
  try
    FilesSelectForm.ShowModal;
    RecompileListMemo.Clear;
    for i := 0 to (FilesSelectForm.SelectedFilesChLB.Count - 1) do begin
      RecompileListMemo.Lines.Add(FilesSelectForm.SelectedFilesChLB.Items[i]);
    end;
    // << ��������� �� ������ � ������ ���� - ������ ��� �������� ������������
    with RecompileListMemo do begin
     selstart := perform( EM_LINEINDEX, 0, 0 );
     perform( EM_SCROLLCARET, 0, 0 );
    end;
    // >> ��������� �� ������ � ������ ���� - ������ ��� �������� ������������
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(RecompileListBtnClick): ������ ������ �� ������� ���� ��� ��������������' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
end;

procedure TIUPCreatorForm.RefreshAllActExecute(Sender: TObject);
//var AuthInfFile: IXMLAUTHOR_INFORMATIONType;
begin
  // ������� ��������� �����.
  Lib.RemoveFilesAndFoldersByMask(SysUtils.GetEnvironmentVariable('TEMP'), '*IUPCreator*');

  // << ������������� �������������� ����
  IUP_Date.DateTime := Now;
  VersYear.Text := FormatDateTime('yyyy', Now);
  IUPNameEdit.Clear;
  RequirementsListMemo.Clear;

  try
//    if FileExists(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml') then begin
//      AuthInfFile := LoadAUTHOR_INFORMATION(ExtractFileDir(ParamStr(0)) + '\AuthorInf.xml');
//      ScenarioXMLPathLabel.Caption := AuthInfFile.SCENARIO;// ExtractFileDir(ParamStr(0)) + '\Scenario.xml';
//      with AuthInfFile do begin
//        IUP_PhoneEdit.Text := IntToStr(PHONE);
//        IUPAuthor.Text     := AUTHOR;
//        IUP_EMailEdit.Text := MAIL;
//      end;
//    end;
    ScenarioXMLPathLabel.Caption := XMLWork.GetXMLNodeValue('SCENARIO');
    IUP_PhoneEdit.Text := XMLWork.GetXMLNodeValue('PHONE');
    IUPAuthor.Text     := XMLWork.GetXMLNodeValue('AUTHOR');
    IUP_EMailEdit.Text := XMLWork.GetXMLNodeValue('MAIL');
  except
    on e:Exception do begin
      MessProc('������', 'main.pas(ClearAllInfoBtnClick): ������ ���������� �������������� �����. ' + sLineBreak +
                e.Message, True, True, True, MB_ICONERROR);
    end;
  end;
  // >> ������������� �������������� ����
  VT.Clear;
  WorkPG.Position := 0;
  MessProc('', '��� �������� ��������� ������', False, True, True);
end;

procedure TIUPCreatorForm.RemRecActExecute(Sender: TObject);
var Nodes: TNodeArray; // ������, � ������� ����� ������� ���������� ����
    i: integer;
begin
  Nodes := VT.GetSortedSelection(True); // �������� ��� ���������� ���� � ������� ������
  for i := 0 to High(Nodes) do begin
    VT.DeleteNode(Nodes[i]);
  end;
end;

end.
