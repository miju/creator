object FilesSelectForm: TFilesSelectForm
  Left = 0
  Top = 0
  BorderIcons = [biMaximize]
  Caption = 
    #1059#1082#1072#1078#1080#1090#1077' '#1092#1072#1081#1083#1099', '#1082#1086#1090#1086#1088#1099#1077' '#1085#1072#1076#1086' '#1087#1077#1088#1077#1082#1086#1084#1087#1080#1083#1080#1088#1086#1074#1072#1090#1100' '#1087#1088#1080' '#1091#1089#1090#1072#1085#1086#1074#1082#1077' '#1086#1073#1085#1086 +
    #1074#1083#1077#1085#1080#1103
  ClientHeight = 655
  ClientWidth = 642
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  DesignSize = (
    642
    655)
  PixelsPerInch = 96
  TextHeight = 13
  object FileIndexChLB: TCheckListBox
    Left = 8
    Top = 63
    Width = 265
    Height = 553
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Columns = 2
    Flat = False
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    Sorted = True
    TabOrder = 3
    OnDblClick = FileIndexChLBDblClick
  end
  object SelButton: TButton
    Left = 285
    Top = 223
    Width = 73
    Height = 25
    Anchors = [akRight]
    Caption = '->'
    Constraints.MaxWidth = 73
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
    WordWrap = True
    OnClick = SelButtonClick
    ExplicitTop = 224
  end
  object SelectedFilesChLB: TCheckListBox
    AlignWithMargins = True
    Left = 371
    Top = 63
    Width = 265
    Height = 547
    Anchors = [akTop, akRight, akBottom]
    BorderStyle = bsNone
    Columns = 1
    Flat = False
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    Sorted = True
    TabOrder = 5
    OnDblClick = SelectedFilesChLBDblClick
  end
  object SelChButton: TButton
    Left = 285
    Top = 254
    Width = 73
    Height = 25
    Anchors = [akRight]
    Caption = '->>'
    Constraints.MaxWidth = 73
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    OnClick = SelChButtonClick
    ExplicitTop = 255
  end
  object UnSelButton: TButton
    Left = 285
    Top = 285
    Width = 73
    Height = 25
    Anchors = [akRight]
    Caption = '<-'
    Constraints.MaxWidth = 73
    ParentShowHint = False
    ShowHint = True
    TabOrder = 10
    OnClick = UnSelButtonClick
    ExplicitTop = 286
  end
  object UnSelChButton: TButton
    Left = 285
    Top = 316
    Width = 73
    Height = 25
    Anchors = [akRight]
    Caption = '<<-'
    Constraints.MaxWidth = 73
    ParentShowHint = False
    ShowHint = True
    TabOrder = 11
    OnClick = UnSelChButtonClick
    ExplicitTop = 317
  end
  object FilesFilterEdit: TEdit
    Left = 8
    Top = 628
    Width = 265
    Height = 21
    Anchors = [akLeft, akBottom]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    OnChange = FilesFilterEditChange
  end
  object BrowseButton: TButton
    Left = 561
    Top = 6
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #1054#1073#1079#1086#1088
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = BrowseButtonClick
  end
  object PathEdit: TEdit
    Left = 8
    Top = 8
    Width = 547
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Text = 'PathEdit'
    OnChange = PathEditChange
  end
  object CloseBtn: TButton
    Left = 561
    Top = 624
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 1
    TabOrder = 7
  end
  object FilesCheckBox: TCheckBox
    Left = 8
    Top = 40
    Width = 265
    Height = 17
    Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077'?'
    TabOrder = 2
    OnClick = FilesCheckBoxClick
  end
  object SelFilesCheckBox: TCheckBox
    Left = 371
    Top = 40
    Width = 265
    Height = 17
    Anchors = [akTop, akRight]
    Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077'?'
    TabOrder = 4
    OnClick = SelFilesCheckBoxClick
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofOldStyleDialog, ofEnableSizing]
    Left = 312
    Top = 576
  end
end
