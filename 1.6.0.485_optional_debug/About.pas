unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TAboutForm = class(TForm)
    URLLbl: TLabel;
    CloseBtn: TButton;
    HelpBtn: TButton;
    LogoImage: TImage;
    CompanyInfoLbl: TLabel;
    AppInfoMemo: TMemo;
    procedure LogoImageClick(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure URLLblMouseLeave(Sender: TObject);
    procedure URLLblMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure URLLblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutForm: TAboutForm;

implementation

uses ShellAPI, Lib;

{$R *.dfm}

procedure TAboutForm.FormCreate(Sender: TObject);
begin
  AppInfoMemo.Text := 'Interin Updates Creator v.' + FileVersion(ParamStr(0));
  CompanyInfoLbl.Caption := '� ����������� "�������", ' + sLineBreak +
                            '���� ��� ��.�.�. ���������� ���.'
end;

procedure TAboutForm.HelpBtnClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar('./Creator.chm'), nil, nil, SW_SHOWNORMAL);
end;

procedure TAboutForm.LogoImageClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar(URLLbl.Caption), nil, nil, SW_SHOWNORMAL);
end;

procedure TAboutForm.URLLblClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open', PChar(URLLbl.Caption), nil, nil, SW_SHOWNORMAL);
end;

procedure TAboutForm.URLLblMouseLeave(Sender: TObject);
begin
  URLLbl.Font.Color := clBlue;
  URLLbl.Font.Style := [];

end;

procedure TAboutForm.URLLblMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  URLLbl.Font.Color := clRed;
  URLLbl.Font.Style := [fsUnderline];
end;

end.
