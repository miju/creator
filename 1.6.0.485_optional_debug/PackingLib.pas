unit PackingLib;

interface

uses
  Dialogs, SysUtils;

function ManPack(Src_,   Dest_, ArchApp_: string; ShowMode_: Word): Boolean;
function ManUnpack(Src_, Dest_, ArchApp_: string; ShowMode_: Word): Boolean;

implementation

uses Windows, Lib;

function ManPack(Src_, Dest_, ArchApp_: string; ShowMode_: Word): Boolean;
var cline: string;
begin
  Result := True;
  cline := ArchApp_;
  cline := StringReplace(cline, '$Src',  Src_,  [rfReplaceAll, rfIgnoreCase]);
  cline := StringReplace(cline, '$Dest', Dest_, [rfReplaceAll, rfIgnoreCase]);
  try
    Result := CPRun(cline, ShowMode_);
  except
    on e:Exception do begin
      ShowMessage('�� ������� ��������� ������� "' + cline + '".' + sLineBreak +
                  '������: ' + SysErrorMessage(GetLastError));
      Result := False;
      Exit;
    end;
  end;
end;

function ManUnpack(Src_, Dest_, ArchApp_:  string; ShowMode_: Word): Boolean;
var cline: string;
begin
  Result := True;
  cline := ArchApp_;
  cline := StringReplace(cline, '$Src',  Src_,  [rfReplaceAll, rfIgnoreCase]);
  cline := StringReplace(cline, '$Dest', Dest_, [rfReplaceAll, rfIgnoreCase]);
  try
    Result := CPRun(cline, ShowMode_);
  except
    on e:Exception do begin
      ShowMessage('�� ������� ��������� ������� "' + cline + '".' + sLineBreak +
                  '������: ' + SysErrorMessage(GetLastError));
      Result := False;
      Exit;
    end;
  end;
end;

end.
