
{***************************************************************************************}
{                                                                                       }
{                                   XML Data Binding                                    }
{                                                                                       }
{         Generated on: 02.06.2009 11:13:22                                             }
{       Generated from: D:\Work\Borland\Creator\1.01_vers\Debug\VersionRepository.xml   }
{   Settings stored in: D:\Work\Borland\Creator\1.01_vers\Debug\VersionRepository.xdb   }
{                                                                                       }
{***************************************************************************************}

unit VersionRepository;

interface

uses xmldom, XMLDoc, XMLIntf,
     //
     SysUtils;

type

{ Forward Decls }

  IXMLREPOSITORYType = interface;
  IXMLUPDATE_ITEMType = interface;

{ IXMLREPOSITORYType }

  IXMLREPOSITORYType = interface(IXMLNodeCollection)
    ['{79EEE822-D0E9-4941-90AF-DDADCEF1A036}']
    { Property Accessors }
    function Get_UPDATE_ITEM(Index: Integer): IXMLUPDATE_ITEMType;
    { Methods & Properties }
    function Add: IXMLUPDATE_ITEMType;
    function Insert(const Index: Integer): IXMLUPDATE_ITEMType;
    property UPDATE_ITEM[Index: Integer]: IXMLUPDATE_ITEMType read Get_UPDATE_ITEM; default;
  end;

{ IXMLUPDATE_ITEMType }

  IXMLUPDATE_ITEMType = interface(IXMLNode)
    ['{8E2207BA-A65A-4286-98BD-A592CBD8BC45}']
    { Property Accessors }
    function Get_YEAR: Integer;
    function Get_MAIN: Integer;
    function Get_PATCH: Integer;
    function Get_STATUS: Integer;
    function Get_CRT_DATE: WideString;
    function Get_CRT_IP: WideString;
    procedure Set_YEAR(Value: Integer);
    procedure Set_MAIN(Value: Integer);
    procedure Set_PATCH(Value: Integer);
    procedure Set_STATUS(Value: Integer);
    procedure Set_CRT_DATE(Value: WideString);
    procedure Set_CRT_IP(Value: WideString);
    { Methods & Properties }
    property YEAR: Integer read Get_YEAR write Set_YEAR;
    property MAIN: Integer read Get_MAIN write Set_MAIN;
    property PATCH: Integer read Get_PATCH write Set_PATCH;
    property STATUS: Integer read Get_STATUS write Set_STATUS;
    property CRT_DATE: WideString read Get_CRT_DATE write Set_CRT_DATE;
    property CRT_IP: WideString read Get_CRT_IP write Set_CRT_IP;
  end;

{ Forward Decls }

  TXMLREPOSITORYType = class;
  TXMLUPDATE_ITEMType = class;

{ TXMLREPOSITORYType }

  TXMLREPOSITORYType = class(TXMLNodeCollection, IXMLREPOSITORYType)
  protected
    { IXMLREPOSITORYType }
    function Get_UPDATE_ITEM(Index: Integer): IXMLUPDATE_ITEMType;
    function Add: IXMLUPDATE_ITEMType;
    function Insert(const Index: Integer): IXMLUPDATE_ITEMType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLUPDATE_ITEMType }

  TXMLUPDATE_ITEMType = class(TXMLNode, IXMLUPDATE_ITEMType)
  protected
    { IXMLUPDATE_ITEMType }
    function Get_YEAR: Integer;
    function Get_MAIN: Integer;
    function Get_PATCH: Integer;
    function Get_STATUS: Integer;
    function Get_CRT_DATE: WideString;
    function Get_CRT_IP: WideString;
    procedure Set_YEAR(Value: Integer);
    procedure Set_MAIN(Value: Integer);
    procedure Set_PATCH(Value: Integer);
    procedure Set_STATUS(Value: Integer);
    procedure Set_CRT_DATE(Value: WideString);
    procedure Set_CRT_IP(Value: WideString);
  end;

{ Global Functions }

function GetREPOSITORY(Doc: IXMLDocument): IXMLREPOSITORYType;
function LoadREPOSITORY(const FileName: WideString): IXMLREPOSITORYType;
function NewREPOSITORY: IXMLREPOSITORYType;
function IndexByMAINValue(XMLDocPath: string; MAINFieldValue: Integer): integer;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetREPOSITORY(Doc: IXMLDocument): IXMLREPOSITORYType;
begin
  Result := Doc.GetDocBinding('REPOSITORY', TXMLREPOSITORYType, TargetNamespace) as IXMLREPOSITORYType;
end;

function LoadREPOSITORY(const FileName: WideString): IXMLREPOSITORYType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('REPOSITORY', TXMLREPOSITORYType, TargetNamespace) as IXMLREPOSITORYType;
end;

function NewREPOSITORY: IXMLREPOSITORYType;
begin
  Result := NewXMLDocument.GetDocBinding('REPOSITORY', TXMLREPOSITORYType, TargetNamespace) as IXMLREPOSITORYType;
end;

{ TXMLREPOSITORYType }

procedure TXMLREPOSITORYType.AfterConstruction;
begin
  RegisterChildNode('UPDATE_ITEM', TXMLUPDATE_ITEMType);
  ItemTag := 'UPDATE_ITEM';
  ItemInterface := IXMLUPDATE_ITEMType;
  inherited;
end;

function TXMLREPOSITORYType.Get_UPDATE_ITEM(Index: Integer): IXMLUPDATE_ITEMType;
begin
  Result := List[Index] as IXMLUPDATE_ITEMType;
end;

function TXMLREPOSITORYType.Add: IXMLUPDATE_ITEMType;
begin
  Result := AddItem(-1) as IXMLUPDATE_ITEMType;
end;

function TXMLREPOSITORYType.Insert(const Index: Integer): IXMLUPDATE_ITEMType;
begin
  Result := AddItem(Index) as IXMLUPDATE_ITEMType;
end;

function IndexByMAINValue(XMLDocPath: string; MAINFieldValue: Integer): integer;
var XMLDoc: IXMLREPOSITORYType;
    i: integer;
begin
  Result := -1;
  try
    if SysUtils.FileExists(XMLDocPath) then begin
      XMLDoc := LoadREPOSITORY(XMLDocPath);
      Result := -1;
      for i := 0 to (XMLDoc.Count - 1) do begin
        if (XMLDoc.UPDATE_ITEM[i].MAIN = MAINFieldValue) then begin
          Result := i;
          break;
        end;
      end;
    end;
  except
    on e:Exception do begin
      Result := -2;
    end;
  end;
end;


{ TXMLUPDATE_ITEMType }

function TXMLUPDATE_ITEMType.Get_YEAR: Integer;
begin
  Result := ChildNodes['YEAR'].NodeValue;
end;

procedure TXMLUPDATE_ITEMType.Set_YEAR(Value: Integer);
begin
  ChildNodes['YEAR'].NodeValue := Value;
end;

function TXMLUPDATE_ITEMType.Get_MAIN: Integer;
begin
  Result := ChildNodes['MAIN'].NodeValue;
end;

procedure TXMLUPDATE_ITEMType.Set_MAIN(Value: Integer);
begin
  ChildNodes['MAIN'].NodeValue := Value;
end;

function TXMLUPDATE_ITEMType.Get_PATCH: Integer;
begin
  Result := ChildNodes['PATCH'].NodeValue;
end;

procedure TXMLUPDATE_ITEMType.Set_PATCH(Value: Integer);
begin
  ChildNodes['PATCH'].NodeValue := Value;
end;

function TXMLUPDATE_ITEMType.Get_STATUS: Integer;
begin
  Result := ChildNodes['STATUS'].NodeValue;
end;

procedure TXMLUPDATE_ITEMType.Set_STATUS(Value: Integer);
begin
  ChildNodes['STATUS'].NodeValue := Value;
end;

function TXMLUPDATE_ITEMType.Get_CRT_DATE: WideString;
begin
  Result := ChildNodes['CRT_DATE'].Text;
end;

procedure TXMLUPDATE_ITEMType.Set_CRT_DATE(Value: WideString);
begin
  ChildNodes['CRT_DATE'].NodeValue := Value;
end;

function TXMLUPDATE_ITEMType.Get_CRT_IP: WideString;
begin
  Result := ChildNodes['CRT_IP'].Text;
end;

procedure TXMLUPDATE_ITEMType.Set_CRT_IP(Value: WideString);
begin
  ChildNodes['CRT_IP'].NodeValue := Value;
end;

end.