{$WARN SYMBOL_DEPRECATED OFF}
{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_PLATFORM OFF}

unit FilesSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ComCtrls, FileCtrl;

type
  TFilesSelectForm = class(TForm)
    FileIndexChLB: TCheckListBox;
    SelButton: TButton;
    SelectedFilesChLB: TCheckListBox;
    SelChButton: TButton;
    UnSelButton: TButton;
    UnSelChButton: TButton;
    FilesFilterEdit: TEdit;
    BrowseButton: TButton;
    PathEdit: TEdit;
    CloseBtn: TButton;
    FilesCheckBox: TCheckBox;
    SelFilesCheckBox: TCheckBox;
    OpenDialog1: TOpenDialog;
    procedure SelectedFilesChLBDblClick(Sender: TObject);
    procedure PathEditChange(Sender: TObject);
    procedure SelFilesCheckBoxClick(Sender: TObject);
    procedure FilesCheckBoxClick(Sender: TObject);
    procedure UnChAllSelFilesBtnClick(Sender: TObject);
    procedure ChAllSelFilesBtnClick(Sender: TObject);
    procedure UnChAllFilesBtnClick(Sender: TObject);
    procedure ChAllFilesBtnClick(Sender: TObject);
    procedure BrowseButtonClick(Sender: TObject);
    procedure UnSelChButtonClick(Sender: TObject);
    procedure UnSelButtonClick(Sender: TObject);
    procedure SelChButtonClick(Sender: TObject);
    procedure FileIndexChLBDblClick(Sender: TObject);
    procedure FilesFilterEditChange(Sender: TObject);
    procedure SelButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function FillFileIndexWithMask(Dir_ : string = 'o:\mc2\Forms\';
                                   mask_: string = '*.fmb'): boolean;
  public
    { Public declarations }
  end;

var
  FilesSelectForm: TFilesSelectForm;

implementation

uses XMLWork;

{$R *.dfm}

procedure TFilesSelectForm.BrowseButtonClick(Sender: TObject);
var dir: string;
begin
  try
    Dir := PathEdit.Text; 
    SelectDirectory(Dir, [sdAllowCreate, sdPerformCreate, sdPrompt], 1000);
//    SelectDirectory('�������� �����, ������� �� ������ �������� � ����������.', '\', dir);
    if DirectoryExists(dir) then begin
      PathEdit.Text := dir;
      FillFileIndexWithMask(PathEdit.Text, FilesFilterEdit.Text);
    end;
  except
    on e:Exception do begin
      ShowMessage('FilesSelect.pas: �������� � ��������� �����. ' + e.Message);
      Exit;
    end;
  end;
end;

procedure TFilesSelectForm.ChAllFilesBtnClick(Sender: TObject);
var i: integer;
begin
  for i := 0 to (FileIndexChLB.Count - 1) do begin
    FileIndexChLB.Checked[i] := True;
  end;
end;

procedure TFilesSelectForm.ChAllSelFilesBtnClick(Sender: TObject);
var i: integer;
begin
  for i := 0 to (SelectedFilesChLB.Count - 1) do begin
    SelectedFilesChLB.Checked[i] := True;
  end;
end;

procedure TFilesSelectForm.SelFilesCheckBoxClick(Sender: TObject);
var i: integer;
begin
  for i := 0 to (SelectedFilesChLB.Count - 1) do begin
    SelectedFilesChLB.Checked[i] := SelFilesCheckBox.Checked;
  end;
end;

procedure TFilesSelectForm.FileIndexChLBDblClick(Sender: TObject);
begin
  FileIndexChLB.Checked[FileIndexChLB.ItemIndex] :=
    not FileIndexChLB.Checked[FileIndexChLB.ItemIndex];
end;

procedure TFilesSelectForm.FilesCheckBoxClick(Sender: TObject);
var i: integer;
begin
  for i := 0 to (FileIndexChLB.Count - 1) do begin
    FileIndexChLB.Checked[i] := FilesCheckBox.Checked;
  end;
end;

procedure TFilesSelectForm.FilesFilterEditChange(Sender: TObject);
begin
  FillFileIndexWithMask(PathEdit.Text, FilesFilterEdit.Text);
end;

function TFilesSelectForm.FillFileIndexWithMask(Dir_ : string = 'o:\mc2\Forms\';
                                                mask_: string = '*.fmb'): boolean;
var SearchRec: TSearchRec;
begin
  Result := True;
  FileIndexChLB.Items.Clear;

  try
    if Dir_<>'' then if Dir_[length(Dir_)]<>'\' then Dir_:=Dir_+'\';
    if FindFirst(Dir_+mask_, faAnyFile-faDirectory-faVolumeID, SearchRec)=0 then
      repeat
        FileIndexChLB.Items.Add(SearchRec.Name);
      until FindNext(SearchRec)<>0;
    FindClose(SearchRec);
  except
    on e:Exception do begin
      ShowMessage('FilesSelect.pas: �� ������� ��������� ������ ������� � ������. ' + e.Message);
      Result := False;
      Exit;
    end;
  end;
end;

procedure TFilesSelectForm.SelButtonClick(Sender: TObject);
begin
  if FileIndexChLB.ItemIndex = -1 then exit;
  SelectedFilesChLB.Items.Add(FileIndexChLB.Items[FileIndexChLB.ItemIndex]);
  FileIndexChLB.Items.Delete(FileIndexChLB.ItemIndex);
//  FileIndexChLB.Sorted := True;
end;

procedure TFilesSelectForm.SelChButtonClick(Sender: TObject);
var
  i: Integer; // ������ �������� ��������
begin
  for i := (FileIndexChLB.Count - 1) DownTo 0 do begin
    if FileIndexChLB.Checked[i] then begin
      SelectedFilesChLB.Items.Add(FileIndexChLB.Items[i]);
      FileIndexChLB.Items.Delete(i);
    end;
  end;
end;

procedure TFilesSelectForm.SelectedFilesChLBDblClick(Sender: TObject);
begin
  SelectedFilesChLB.Checked[SelectedFilesChLB.ItemIndex] :=
    not SelectedFilesChLB.Checked[SelectedFilesChLB.ItemIndex];
end;

procedure TFilesSelectForm.UnChAllFilesBtnClick(Sender: TObject);
var i: integer;
begin
  for i := 0 to (FileIndexChLB.Count - 1) do begin
    FileIndexChLB.Checked[i] := False;
  end;
end;

procedure TFilesSelectForm.UnChAllSelFilesBtnClick(Sender: TObject);
var i: integer;
begin
  for i := 0 to (SelectedFilesChLB.Count - 1) do begin
    SelectedFilesChLB.Checked[i] := False;
  end;
end;

procedure TFilesSelectForm.UnSelButtonClick(Sender: TObject);
begin
  if SelectedFilesChLB.ItemIndex = -1 then exit;
  FileIndexChLB.Items.Add(SelectedFilesChLB.Items[SelectedFilesChLB.ItemIndex]);
  SelectedFilesChLB.Items.Delete(SelectedFilesChLB.ItemIndex);
end;

procedure TFilesSelectForm.UnSelChButtonClick(Sender: TObject);
var
  i: Integer; // ������ �������� ��������
begin
  for i := (SelectedFilesChLB.Count - 1) DownTo 0 do begin
    if SelectedFilesChLB.Checked[i] then begin
      FileIndexChLB.Items.Add(SelectedFilesChLB.Items[i]);
      SelectedFilesChLB.Items.Delete(i);
    end;
  end;
end;

procedure TFilesSelectForm.FormShow(Sender: TObject);
begin
  SelButton.ShowHint := True;
  SelChButton.ShowHint := True;
  UnSelButton.ShowHint := True;
  UnSelChButton.ShowHint := True;
  FileIndexChLB.ShowHint := True;
  SelectedFilesChLB.ShowHint := True;

  SelButton.Hint     := '�������� ������ ���� � ������.';
  SelChButton.Hint   := '�������� ��� ���������� ����� � ������.';
  UnSelButton.Hint   := '������ ������ ���� �� ������.';
  UnSelChButton.Hint := '������ ��� ���������� ����� �� ������.';
  FileIndexChLB.Hint := '���������� �����, ������� ���������� ����������������� ' + sLineBreak +
                        '��� ��������� ����������, � ������ ����. ��� ����� ' + sLineBreak +
                        '�������������� �������� "->", "->>", "<<-", "<-"';
  SelectedFilesChLB.Hint := '������ ������, ������� ����� ����������������� ' + sLineBreak +
                            '��� ��������� ����������';
  FilesFilterEdit.Hint := '������ �� ����� �����';

  FilesFilterEdit.Text := XMLWork.GetXMLNodeValue('MASK');
  PathEdit.Text        := XMLWork.GetXMLNodeValue('DIR');

  FillFileIndexWithMask(PathEdit.Text, FilesFilterEdit.Text);
end;

procedure TFilesSelectForm.PathEditChange(Sender: TObject);
begin
  FillFileIndexWithMask(PathEdit.Text, FilesFilterEdit.Text);
end;

end.
