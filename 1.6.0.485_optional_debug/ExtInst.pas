unit ExtInst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TExtInstForm = class(TForm)
    CloseBtn: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SaveBtn: TButton;
    InstallMemo: TMemo;
    BackupMemo: TMemo;
    ReturnMemo: TMemo;
    ExtsCB: TComboBox;
    LoadScenarioDataBtn: TButton;
    Label4: TLabel;
    ScenPathLblLbl: TLabel;
    ClearBtn: TButton;
    procedure ClearBtnClick(Sender: TObject);
    procedure CloseBtnKeyPress(Sender: TObject; var Key: Char);
    procedure LoadScenarioDataBtnClick(Sender: TObject);
    procedure ExtsCBSelect(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ExtInstForm: TExtInstForm;

implementation

uses Scenario2, main;

{$R *.dfm}

procedure TExtInstForm.ClearBtnClick(Sender: TObject);
begin
  InstallMemo.Clear;
  BackupMemo.Clear;
  ReturnMemo.Clear;
end;

procedure TExtInstForm.CloseBtnKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = 't' then begin
    ScenPathLblLbl.Visible := not ScenPathLblLbl.Visible; 
  end;
end;

procedure TExtInstForm.ExtsCBSelect(Sender: TObject);
begin
  LoadScenarioDataBtn.SetFocus;
end;

procedure TExtInstForm.FormShow(Sender: TObject);
var Scn: IXMLSCENARIOType; //��������� ���-���� ���� ��������
    i: integer;
begin
  Scn := LoadSCENARIO(IUPCreatorForm.ScenarioXMLPathLabel.Caption);
  ScenPathLblLbl.Caption := IUPCreatorForm.ScenarioXMLPathLabel.Caption;
  with ExtsCB do begin
    Clear;
    for i := 0 to (Scn.ITEM.Count - 1) do begin
        Items.Add(Scn.ITEM[i].EXT);
    end;
    ItemIndex := 1;
  end;
end;

procedure TExtInstForm.LoadScenarioDataBtnClick(Sender: TObject);
var Scn: IXMLSCENARIOType; //��������� ���-���� ���� ��������
    i, j:   integer;
begin
  Scn := LoadSCENARIO(IUPCreatorForm.ScenarioXMLPathLabel.Caption);
  for i := 0 to (Scn.ITEM.Count-1) do begin
    //�������� �� ����� �������� ������ ����������
    if UpperCase(Scn.ITEM.Items[i].Ext) = UpperCase(ExtsCB.Items[ExtsCB.ItemIndex]) then begin
      // ��� ���������� ���������� ��������� ���� �����������...
      InstallMemo.Clear;
      for j:=0 to (Scn.ITEM.Items[i].INSTALL.OPERATION.Count-1) do begin
        InstallMemo.Lines.Add(Scn.ITEM.Items[i].INSTALL.OPERATION[j]);
      end;
      // ...������...
      BackupMemo.Clear;
      for j:=0 to (Scn.ITEM.Items[i].BACK_UP.OPERATION.Count-1) do begin
        BackupMemo.Lines.Add(Scn.ITEM.Items[i].BACK_UP.OPERATION[j]);
      end;
      // ...��������������...
      ReturnMemo.Clear;
      for j:=0 to (Scn.ITEM.Items[i].RETURN.OPERATION.Count-1) do begin
        ReturnMemo.Lines.Add(Scn.ITEM.Items[i].RETURN.OPERATION[j]);
      end;
    end;

  end;
end;

end.
