unit CrtProcess;

interface

uses Windows, Dialogs;

function CPRun(cline :string):Boolean;

implementation

function CPRun(cline :string):Boolean;
var
  //����������� ��������� ��� CreateProcess
  pi : TProcessInformation;
  si : TStartupInfo;
begin
  Result := True;
  ZeroMemory(@si,sizeof(si));
  si.cb:=SizeOf(si);
  si.dwFlags :=  STARTF_USESHOWWINDOW;
  si.wShowWindow := SW_HIDE;//�������� ���� �������
  if (not CreateProcess(nil, //pointer to name of executable module
                        PChar(cline), // Command line.
                        nil, // Process handle not inheritable.
                        nil, // Thread handle not inheritable.
                        False, // Set handle inheritance to FALSE.
                        0, // No creation flags.
                        nil, // Use parent's environment block.
                        nil, // Use parent's starting directory.
                        si, // Pointer to STARTUPINFO structure.
                        pi )) // Pointer to PROCESS_INFORMATION structure.
  then
  begin
    ShowMessage('������ �������� �����������.');
    Result := False;
  end;
  //������� ��������� ��������
  WaitForSingleObject(pi.hProcess,INFINITE);
  CloseHandle(pi.hProcess);
  CloseHandle(pi.hThread);
end;

end.
