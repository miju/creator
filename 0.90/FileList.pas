unit FileList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Grids, ExtCtrls,

  // ������, ����������� �������������
  CheckingSum, IUPCreator, CheckLst, IUP_Creating, CopyFile,
  Gauges, RemoveWholeDir, CrtProcess, FolderActions;

type
  TFilelist_Form3 = class(TForm)
    IUP_Crt_Buttn: TButton;
    Edit_Parent: TEdit;
    Title_Label: TLabel;
    FileIndex_Panel: TPanel;
    FileIndex_Grid: TStringGrid;
    CheckBox_Parent: TCheckBox;
    ChBx_Sel_Button: TButton;
    Back_ToParamsButton: TButton;
    Seq_ComboBox: TComboBox;
    Scnr_OpenDialog: TOpenDialog;
    Scnr_sel_Buttn: TButton;
    Button1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
//    procedure Button_ParentClick(Sender: TObject);
    procedure Scnr_sel_ButtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Seq_ComboBoxExit(Sender: TObject);
    procedure Seq_ComboBoxChange(Sender: TObject);
    procedure Back_ToParamsButtonClick(Sender: TObject);
    procedure FileIndex_GridClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ChBx_Sel_ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FileIndex_GridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure IUP_Crt_ButtnClick(Sender: TObject);
    procedure FileIndex_GridSelectCell(
                Sender: TObject;
                ACol, ARow: Integer;
                var CanSelect: Boolean);
//  procedure CheckBox_ParentClick(Sender: TObject);
    { Private declarations }
  public
    procedure AddCheckBoxes;
    procedure AddJIPCheckBoxes;
    Procedure clean_previus_buffer;
    procedure JIPClean_Previus_Buffer;
    procedure Set_CheckBox_Alignment;
    procedure JIPSet_Checkbox_Alignment;
    procedure Files2Grid(mask: String);
    procedure GridColDraw(WorkGrid: TStringGrid);
    procedure AutoSizeGridColumn(Grid : TStringGrid; column : integer);
    function  UpdXML_Wrtr(): Boolean;
    function  CheckSelCheckBox(): Boolean;
    procedure JIP_Crt();
//    procedure AddComboBox;
end;

var
  Filelist_Form3: TFilelist_Form3;
  CheckBoxes: array of TCheckBox;
  JIPCheckBoxes: array of TCheckBox;
  ComboBoxes_Indctr: array of Boolean;
  ComboBoxes: array of TComboBox;
  chkd: Boolean;
  cofupd :integer;//���������� ������ � ����������
  i, j: integer; // �������
implementation

uses IUP_Set_Params, Final_Dlg;

{$R *.dfm}

// ������� ��������� ���-�� ������ � �����
function GetFileCount(Dir:string):integer;
var fs:TSearchRec;
begin
  Result:=0;
  if FindFirst(Dir+'\*', faAnyFile - faVolumeID, fs) = 0 then
  begin
    repeat
      if ((fs.Name <> 'Update.xml') AND
          (fs.Name <> 'checksum') AND
          (fs.Name <> 'Scenario.xml')) then
      begin
        inc(Result);
      end;
    until FindNext(fs)<>0;
    FindClose(fs);
  end;
  Result := Result - 2;
end;

// ��������� ������������ ������� �� ����� ������� ������
procedure TFilelist_Form3.AutoSizeGridColumn(Grid : TStringGrid; column : integer);
var
  i : integer;
  temp : integer;
  max : integer;
begin
  max := 0;
  for i := 0 to (Grid.RowCount - 1) do begin
    temp := Grid.Canvas.TextWidth(grid.cells[column, i]);
    if temp > max then max := temp;
  end;
  Grid.ColWidths[column] := Max + Grid.GridLineWidth + 3;
end;

procedure TFilelist_Form3.Back_ToParamsButtonClick(Sender: TObject);
begin
  Filelist_Form3.Hide;
  Params_Form2.Show;
end;

procedure TFilelist_Form3.Button1Click(Sender: TObject);
var SearchRec: TSearchRec;
    subfld_path: string;
begin

      // ������� ������������ �����
      for i := 1 to (Length(JIPCheckBoxes) - 1) do begin
        if (not (JIPCheckBoxes[i].Checked)) then begin
          RemoveAllDir(IUP_fld_path+FileIndex_Grid.Cells[3, i]);
        end;
      end;

      // ������ �������� Folder
      try
        ForceDirectories(IUP_fld_path + 'Folder');
      except
        ShowMessage('���������� ������ �������� ������� ������: ' + IntToStr(GetLastError));
      end;

      // �������� ��� �������� � ���� - Folder
      if FindFirst(IUP_fld_path + '*', faDirectory, SearchRec) = 0 then
        repeat
          if ((SearchRec.Attr and faDirectory) = faDirectory) then
            if ((SearchRec.Name <> '.') and
                (SearchRec.Name <> '..') and
                (SearchRec.Name <> 'Folder')) then
            begin
              CPRun('cmd /c xcopy /E /C /Y "' +
                      IUP_fld_path + SearchRec.Name + '" "' +
                      IUP_fld_path + 'Folder\' + SearchRec.Name + '\"');
              // ������� ��������, ������������ � Folder
              RemoveAllDir(IUP_fld_path + SearchRec.Name);

            end;
        until (FindNext(SearchRec) <> 0);
      FindClose(SearchRec);

      // ������ ����� � JIP-�����
      Crt_IUP(IUP_fld_path + 'Folder', IUP_fld_path + 'Folder.jip');

      CPRun('cmd /c rmdir /S /Q "' + IUP_fld_path + 'Folder"');
//      RemoveAllDir(IUP_fld_path + 'Folder');

end;

procedure TFilelist_Form3.Button2Click(Sender: TObject);
begin
  De_DoFolderAction('F:\112\','F:\arch.dat',DODO3);
  De_DoFolderAction('D:\TEMP\Creator\', 'D:\TEMP\Creator\Folder.jip', DODO3);
end;

procedure TFilelist_Form3.Scnr_sel_ButtnClick(Sender: TObject);
var mask: String;
begin
  mask := IUP_fld_path;
  // ���������� ��������� ������� ���������� �����
  Scnr_OpenDialog.Title := '�������� ����� Scenarion.xml';
  // ��������� ��������� ����� ���� .iup
  Scnr_OpenDialog.Filter := 'Interin Update Package|*.xml';
  Scnr_OpenDialog.Filename := 'Scenario.xml';
  // ��������� ���������� �� ���������
  Scnr_OpenDialog.DefaultExt := 'xml';
  // �� ��������� ����� ���������� = �������� �����
  Scnr_OpenDialog.InitialDir := mask + '\';
  if Scnr_OpenDialog.execute then begin
//    ShowMessage('Copy '+Scnr_OpenDialog.FileName);
    WindowsCopyFile(Scnr_OpenDialog.FileName, mask + '\');
  end;
end;

procedure TFilelist_Form3.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  Paths_Form1.WhenClose;
end;

procedure TFilelist_Form3.FormCreate(Sender: TObject);
begin
//  ShowMessage('TFilelist_Form3.FormCreate');
end;

procedure TFilelist_Form3.FormShow(Sender: TObject);
begin
  // ���-�� ������
  cofupd := GetFileCount(IUP_fld_path + '\');
//ShowMessage(IntToStr(cofupd));
  // ��������� ������� �����
  GridColDraw(FileIndex_Grid);
//ShowMessage('GridColDraw(FileIndex_Grid);');
  // ���������� ����� ������� ������ �� ���������� mask
  Files2Grid(IUP_fld_path);
//ShowMessage('Files2Grid(IUP_fld_path + \);');
//  Files2Grid('D:\New\');
  FileIndex_Grid.RowCount := cofupd + 1;
//ShowMessage('FileIndex_Grid.RowCount := cofupd + 1;');
  // ��������� �������� � ������ �������...
  chkd := True;
  AddCheckBoxes;
  AddJIPCheckBoxes;
  FileIndex_Grid.DefaultRowHeight := Seq_ComboBox.Height;
  Seq_ComboBox.Visible := False;

//  SetLength(ComboBoxes, cofupd);
  for i := 0 to (cofupd - 1) do begin
//    ComboBoxes_Indctr[i] := False;
    FileIndex_Grid.Cells[1, i+1] := IntToStr(i+1);
  end;
//ShowMessage('AddCheckBoxes;');
  // ����������� ������� �� ����� ������� ������
  for i := 1 to 4 do AutoSizeGridColumn(FileIndex_Grid, i);
end;

procedure TFilelist_Form3.GridColDraw(WorkGrid: TStringGrid);
//var
//  CheckArray: array[1..100] of TCheckBox;//!!! ���� ��������. ���� ��� ����� ��������!
begin
  // ��������� ������� �����
  WorkGrid.ColWidths[0]:= 45;

  WorkGrid.Cells[1, 0] := '�������';
  WorkGrid.ColWidths[1]:= 15;

  WorkGrid.Cells[2, 0] := '����.';
//  WorkGrid.ColWidths[2]:= 50;

  WorkGrid.Cells[3, 0] := '��� �����';
//  WorkGrid.ColWidths[3]:= 200;

  WorkGrid.Cells[4, 0] := '��� �������';

  WorkGrid.Cells[5, 0] := '��������';
  WorkGrid.ColWidths[5] := 300;
end;

procedure TFilelist_Form3.FileIndex_GridClick(Sender: TObject);
begin
  for i := 1 to (FileIndex_Grid.ColCount-1) do begin
    AutoSizeGridColumn(FileIndex_Grid, i);
  end;
end;

procedure TFilelist_Form3.FileIndex_GridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin 
 if not (gdFixed in State) then
{  case ACol  of
  0: set_checkbox_alignment;
  3: JIPset_checkbox_alignment;
  end;
  }
 begin
  set_checkbox_alignment;
  JIPset_checkbox_alignment;
 end;

end;

procedure TFilelist_Form3.FileIndex_GridSelectCell(
  Sender: TObject;
  ACol, ARow: Integer;
  var CanSelect: Boolean);
  var i:integer;
      R: TRect;

begin
  if ((ACol = 1) or (ACol = 4) or (ACol = 5)) then begin
    FileIndex_Grid.Options:=FileIndex_Grid.Options+[goEditing];
  end
  else
    FileIndex_Grid.Options:=FileIndex_Grid.Options-[goEditing];

  if ((ACol = 1) AND
      (ARow <> 0)) then begin
   {������ � ������������ combobox ��������� ��� ������}

    R := FileIndex_Grid.CellRect(ACol, ARow);
{    R.Left := R.Left + FileIndex_Grid.Left;
    R.Right := R.Right + FileIndex_Grid.Left;
    R.Top := R.Top + FileIndex_Grid.Top;
    R.Bottom := R.Bottom + FileIndex_Grid.Top;
 }
    Seq_ComboBox.Left := R.Left + 10;
    Seq_ComboBox.Top := R.Top + 58;
    Seq_ComboBox.Width := (R.Right + 1) - R.Left;
    Seq_ComboBox.Height := (R.Bottom + 1) - R.Top;
   {���������� combobox}
    Seq_ComboBox.Visible := True;
    Seq_ComboBox.SetFocus;
  end;
  CanSelect := True;


end;

// ���������� ����� ����������� � ������ � ����������
procedure TFilelist_Form3.Files2Grid(mask: String);
var
  SearchRec:    TSearchRec;
  sfile: string; // ������ ���� �� ���������  �����
  i: integer; // ������� ������
  filename, fileext: string;
  start, finish: TDateTime;
  fileattr: Integer;
  Result: integer;

begin
  i:=0;
  try
    // ���� �������������...
    if FindFirst(mask + '*', faDirectory, SearchRec) = 0 then
      repeat
        if ((SearchRec.Attr and faDirectory) = faDirectory) then
          if ((SearchRec.Name <> '.') and (SearchRec.Name <> '..')) then
          begin
            inc(i);
            FileIndex_Grid.Cells[2, i] := '�����';
            FileIndex_Grid.Cells[3, i] := SearchRec.Name + '\';
            FileIndex_Grid.Cells[4, i] := '�����';
//            ShowMessage('������� = ' + IntToStr(i) + #13#10 + '��� ����� ' + SearchRec.Name);
          end;
      until (FindNext(SearchRec) <> 0);
    FindClose(SearchRec);
    // ... � �����.
    if FindFirst(mask + '*', faAnyFile - (faVolumeID + faDirectory), SearchRec) = 0 then begin
//      if (SearchRec.Name <> '.') AND (SearchRec.Name <> '..') then begin
      repeat

        begin
          if ((SearchRec.Name <> 'Update.xml')    AND
              (SearchRec.Name <> 'checksum')      AND
              (SearchRec.Name <> 'Scenario.xml')) then
          begin
            inc(i);
            // �������� ��� �����...
            filename := ExtractFileName(SearchRec.Name);
            Delete(filename, length(SearchRec.Name) - length(extractfileext(SearchRec.Name)) + 1, length(extractfileext(SearchRec.Name)));
            // ... � ��� ����������.
            fileext := ExtractFileExt(SearchRec.Name);
            Delete(fileext, 1, 1);    // �������� ����� �� ����������
//            ShowMessage('��� ' + filename + #13#10 + '���������� ' + fileext);

            // �������� ��������� ������ � ����: ����������, ���, ��� ������� (�� ��������� - ��� �����)
            FileIndex_Grid.Cells[2, i] := fileext;
            FileIndex_Grid.Cells[3, i] := filename;
            FileIndex_Grid.Cells[4, i] := AnsiUpperCase(filename);

//            ShowMessage('������� = ' + IntToStr(i) + #13#10 + '��� ����� ' + SearchRec.Name);
          end;
        end;

      until (FindNext(SearchRec) <> 0);
    FindClose(SearchRec);
  end;
  except
    ShowMessage('������ ������ � �������� ��������.');
  end;
end;

// ��������� ������� � columnNo-�������...
procedure TFilelist_Form3.AddCheckBoxes;
var
  i: Integer;
  NewCheckBox: TCheckBox;
begin
  try
    SetLength(CheckBoxes, FileIndex_Grid.RowCount);
    clean_previus_buffer; // ������� �������������� ��������...
    for i := 0 to (FileIndex_Grid.RowCount - 1) do begin
      CheckBoxes[i+1] := TCheckBox.Create(Application);
      CheckBoxes[i+1].Width := 0;
      CheckBoxes[i+1].Visible := false;
  //    CheckBoxes[i+1].Caption := IntToStr(i);
      CheckBoxes[i+1].Caption := IntToStr(i);
      CheckBoxes[i+1].Color := clWindow;
      CheckBoxes[i+1].Tag := i;
      //��������� ���������� ������� OnClick � ������������ TCheckBox
  //  CheckBoxes[i+1].OnClick := CheckBox_ParentClick;
      CheckBoxes[i+1].Parent := FileIndex_Panel;
      FileIndex_Grid.Objects[0, i] := CheckBoxes[i+1];
  //    FileIndex_Grid.RowCount := i;
    end;
    set_checkbox_alignment; // ������������ ��������� � ������� �������...
  except
    ShowMessage('�������� ��������� ' + IntToStr(GetLastError));
  end;
end;

// ������� �������������� �������� � columnNo-�������...
procedure TFilelist_Form3.Clean_Previus_Buffer;
var
  NewCheckBox: TCheckBox;
  i: Integer;
begin
  for i := 0 to (FileIndex_Grid.RowCount - 1) do begin
    NewCheckBox := (FileIndex_Grid.Objects[0, i+1] as TCheckBox);
    if NewCheckBox <> nil then begin
      NewCheckBox.Visible := false;
      FileIndex_Grid.Objects[0,i+1] := nil;
    end;
  end;
end;

// �����������/������ ���� ������� � 1-�� �������
procedure TFilelist_Form3.ChBx_Sel_ButtonClick(Sender: TObject);
begin
  for i := 0 to (FileIndex_Grid.RowCount - 2) do
//  for i := 0 to 7 do
  begin
    CheckBoxes[i+1].Checked := chkd;
  end;
  chkd:= not chkd;
end;

procedure TFilelist_Form3.Seq_ComboBoxChange(Sender: TObject);
begin
 {�������� ��������� ������� �� ComboBox � �������� ��� � ����}
  FileIndex_Grid.Cells[FileIndex_Grid.Col,
                    FileIndex_Grid.Row] :=
    Seq_ComboBox.Items[Seq_ComboBox.ItemIndex];
  Seq_ComboBox.Visible := False;
  FileIndex_Grid.SetFocus;
end;

procedure TFilelist_Form3.Seq_ComboBoxExit(Sender: TObject);
begin 
 {�������� ��������� ������� �� ComboBox � �������� ��� � ����} 
  FileIndex_Grid.Cells[FileIndex_Grid.Col, 
                    FileIndex_Grid.Row] := 
    Seq_ComboBox.Items[Seq_ComboBox.ItemIndex]; 
  Seq_ComboBox.Visible := False;
  FileIndex_Grid.SetFocus;
end; 

// ������������ ��������� � ������� ������� (� columnNo-�������)...
procedure TFilelist_Form3.Set_Checkbox_Alignment;
var
  NewCheckBox: TCheckBox;
  Rect: TRect;
  i: Integer;
begin
  for i := 0 to (FileIndex_Grid.RowCount - 1) do begin
    CheckBoxes[i+1] := (FileIndex_Grid.Objects[0, i+1] as TCheckBox);
    if CheckBoxes[i+1] <> nil then begin
      // �������� ������ ������ ��� ��������
      Rect := FileIndex_Grid.CellRect(0, i+1);
      CheckBoxes[i+1].Top := FileIndex_Grid.Top + Rect.Top + 2;
      CheckBoxes[i+1].Height := Rect.Bottom - Rect.Top - 1;
      CheckBoxes[i+1].Left := FileIndex_Grid.Left + 10;
      CheckBoxes[i+1].Width := 32;
      CheckBoxes[i+1].Visible := True;
    end;
  end;
end;

procedure TFilelist_Form3.IUP_Crt_ButtnClick(Sender: TObject);
begin
  if CheckSelCheckBox() then
  begin
    if FileExists(IUP_fld_path + '\Scenario.xml') then
    begin

      if not DirectoryExists(ExtractFileDir(IUP_file_path)) then
      begin
        try
          ForceDirectories(IUP_file_path);
        except
          ShowMessage('���������� ������ �������� ������� ������: ' + IntToStr(GetLastError));
        end;
      end;

      JIP_Crt;
      // ����� ���� �������� ���������� Update.xml
      if UpdXML_Wrtr then
      begin
        // ������ JIP-�����...
        // ... � IUP.
        Crt_IUP(IUP_fld_path, IUP_file_path);
        Filelist_Form3.Hide;
        Final_Form4.Show;
    	end
    end
		else ShowMessage('����������, ������� ���� � ����� "Scenario.xml"');
  end
  else ShowMessage('����������, �������� ���� �� ���� ����');
end;

// �������� JIP-������
procedure TFilelist_Form3.JIP_Crt();
var SearchRec: TSearchRec;
    subfld_path: string;
begin

      // ������� ������������ �����
      for i := 1 to (Length(JIPCheckBoxes) - 1) do begin
        if (not (JIPCheckBoxes[i].Checked)) then begin
          RemoveAllDir(IUP_fld_path+FileIndex_Grid.Cells[3, i]);
        end;
      end;

      // ������ �������� Folder
      try
        ForceDirectories(IUP_fld_path + 'Folder');
      except
        ShowMessage('���������� ������ �������� ������� ������: ' + IntToStr(GetLastError));
      end;

      // �������� ��� �������� � ���� - Folder
      if FindFirst(IUP_fld_path + '*', faDirectory, SearchRec) = 0 then
        repeat
          if ((SearchRec.Attr and faDirectory) = faDirectory) then
            if ((SearchRec.Name <> '.') and
                (SearchRec.Name <> '..') and
                (SearchRec.Name <> 'Folder')) then
            begin
              CPRun('cmd /c xcopy /E /C /Y "' +
                      IUP_fld_path + SearchRec.Name + '" "' +
                      IUP_fld_path + 'Folder\' + SearchRec.Name + '\"');
              // ������� ��������, ������������ � Folder
              RemoveAllDir(IUP_fld_path + SearchRec.Name);

            end;
        until (FindNext(SearchRec) <> 0);
      FindClose(SearchRec);

      // ������ ����� � JIP-�����
      ShowMessage('������ ����� � JIP-�����');

      if (not (DoFolderAction(IUP_fld_path + 'Folder', IUP_fld_path + 'Folder.jip', DODO2))) then
        ShowMessage('�������� ������ �������� ����� ����������: ' + IntToStr(GetLastError));
      ShowMessage('���������� � JIP-�����');

      // ������� ������ ��� �������� ����� Folder
      CPRun('cmd /c rmdir /S /Q "' + IUP_fld_path + 'Folder"');

end;

// ������� ������ ����� Update.xml
function TFilelist_Form3.UpdXML_Wrtr(): Boolean;
// ��������������� ���� ��������
var Update:TextFile;

begin
  try
    AssignFile(Update, IUP_fld_path + '\Update.xml');
    Rewrite(Update);
    // ������ ���������� �� ����������
    Writeln(Update, '<?xml version="1.0" encoding="Windows-1251"?>');
    Writeln(Update, '<UPDATE>');
    Writeln(Update, '  <DATE>'        + DateToStr(Params_Form2.Date_Picker.Date) + '</DATE>');
    Writeln(Update, '  <AUTHOR>'      + Params_Form2.Author_Edit.Text            + '</AUTHOR>');
    Writeln(Update, '  <EMAIL>'       + Params_Form2.Email_Edit.Text             + '</EMAIL>');
    Writeln(Update, '  <PHONE>'       + Params_Form2.Phone_Edit.Text             + '</PHONE>');
    Writeln(Update, '  <UPDATE_NAME>' + Params_Form2.Name_Edit.Text              + '</UPDATE_NAME>');
    Writeln(Update, '  <DESCRIPTION>' + Params_Form2.Desc_Memo.Text              + '</DESCRIPTION>');

    if (FileExists(IUP_fld_path + '\Folder.jip')) then
      ShowMessage('���� ' + IUP_fld_path + 'Folder.jip' + ' ����������');
    // ���������� ���������� � JIP-������
    if (FileExists(IUP_fld_path + '\Folder.jip')) then
    begin
      Writeln(Update, '  <ITEM>');
      Writeln(Update, '    <SEQ>'         + IntToStr(FileIndex_Grid.RowCount + 1) + '</SEQ>');
      Writeln(Update, '    <OBJECT_TYPE>' + 'jip'                            + '</OBJECT_TYPE>');
      Writeln(Update, '    <OBJECT_NAME>' + 'FOLDER.JIP'                     + '</OBJECT_NAME>');
      Writeln(Update, '    <FILE_NAME>'   + 'Folder.jip'                     + '</FILE_NAME>');
      Writeln(Update, '    <DESCRIPTION>' + FileIndex_Grid.Cells[5, i+1]     + '</DESCRIPTION>');
      Writeln(Update, '  </ITEM>');
    end;

    // ������ ���������� � ������
    for i := 0 to (FileIndex_Grid.RowCount - 2) do begin
      if (CheckBoxes[i+1].Checked = True) then begin
        Writeln(Update, '  <ITEM>');
        Writeln(Update, '    <SEQ>'         + IntToStr(i+1)                + '</SEQ>');
        Writeln(Update, '    <OBJECT_TYPE>' + FileIndex_Grid.Cells[2, i+1] + '</OBJECT_TYPE>');
        Writeln(Update, '    <OBJECT_NAME>' + FileIndex_Grid.Cells[4, i+1] + '</OBJECT_NAME>');
        Writeln(Update, '    <FILE_NAME>'   +
                              FileIndex_Grid.Cells[3, i+1] +
                              '.' +
                              FileIndex_Grid.Cells[2, i+1] + '</FILE_NAME>');
        Writeln(Update, '    <DESCRIPTION>' + FileIndex_Grid.Cells[5, i+1] + '</DESCRIPTION>');
        Writeln(Update, '  </ITEM>');
      end;
    end;
    Writeln(Update, '</UPDATE>');
    CloseFile(Update);
    result := True;
  except
    result := False;
  end;
end;

// ������� ��������, ���� �� � ������� ���������� �����
function TFilelist_Form3.CheckSelCheckBox(): Boolean;
begin
  Result := False;
  for i := 0 to (FileIndex_Grid.RowCount - 2) do
  begin
    if (CheckBoxes[i+1].Checked = True) then
    begin
      Result := True;
    end;
  end;
end;

// ��������� ������� � ������ �����
procedure TFilelist_Form3.AddJIPCheckBoxes;
var
  i: Integer;
  NewCheckBox: TCheckBox;
begin
  try
    SetLength(JIPCheckBoxes, FileIndex_Grid.RowCount);
    JIPclean_previus_buffer; // ������� �������������� ��������...
    for i := 0 to (FileIndex_Grid.RowCount - 1) do
    begin
        JIPCheckBoxes[i+1] := TCheckBox.Create(Application);
        JIPCheckBoxes[i+1].Width := FileIndex_Grid.ColWidths[4];
        JIPCheckBoxes[i+1].Visible := false;
    //    JIPCheckBoxes[i+1].Caption := IntToStr(i);
        JIPCheckBoxes[i+1].Caption := '�������� � �����';
        JIPCheckBoxes[i+1].Color := clWindow;
        JIPCheckBoxes[i+1].Tag := i;
        //��������� ���������� ������� OnClick � ������������ TCheckBox
    //  JIPCheckBoxes[i+1].OnClick := CheckBox_ParentClick;
        JIPCheckBoxes[i+1].Parent := FileIndex_Panel;
        FileIndex_Grid.Objects[4, i] := JIPCheckBoxes[i+1];
    //    FileIndex_Grid.RowCount := i;
    end;
    JIPset_checkbox_alignment; // ������������ ��������� � ������� �������...
  except
    ShowMessage('�������� ��������� ' + IntToStr(GetLastError));
  end;
end;

// ������� �������������� �������� � columnNo-�������...
procedure TFilelist_Form3.JIPClean_Previus_Buffer;
var
  NewCheckBox: TCheckBox;
  i: Integer;
begin
  for i := 0 to (FileIndex_Grid.RowCount - 1) do begin
    NewCheckBox := (FileIndex_Grid.Objects[4, i+1] as TCheckBox);
    if NewCheckBox <> nil then begin
      NewCheckBox.Visible := false;
      FileIndex_Grid.Objects[4,i+1] := nil;
    end;
  end;
end;

procedure TFilelist_Form3.JIPSet_Checkbox_Alignment;
var
  NewCheckBox: TCheckBox;
  Rect: TRect;
  i: Integer;
begin
  for i := 0 to (FileIndex_Grid.RowCount - 1) do
    if (DirectoryExists(IUP_fld_path + FileIndex_Grid.Cells[3, i+1])) then
      begin
        JIPCheckBoxes[i+1] := (FileIndex_Grid.Objects[4, i+1] as TCheckBox);
        if JIPCheckBoxes[i+1] <> nil then begin
          // �������� ������ ������ ��� ��������
          Rect := FileIndex_Grid.CellRect(4, i+1);
          JIPCheckBoxes[i+1].Top := FileIndex_Grid.Top + Rect.Top + 2;
          JIPCheckBoxes[i+1].Height := Rect.Bottom - Rect.Top - 1;
          JIPCheckBoxes[i+1].Left := FileIndex_Grid.Left + Rect.Left + 2;
          JIPCheckBoxes[i+1].Width := FileIndex_Grid.ColWidths[4];
//          JIPCheckBoxes[i+1].Width := 32;
          JIPCheckBoxes[i+1].Visible := True;
        end;
      end;


end;



end.
