object Params_Form2: TParams_Form2
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1057#1086#1079#1076#1072#1090#1077#1083#1100' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1081' '#1048#1053#1058#1045#1056#1048#1053': '#1086#1087#1080#1089#1072#1085#1080#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
  ClientHeight = 597
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Title_Label: TLabel
    Left = 148
    Top = 8
    Width = 273
    Height = 33
    Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clNavy
    Font.Height = -29
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object IUP_PathLabel: TLabel
    Left = 15
    Top = 49
    Width = 451
    Height = 16
    Caption = 
      #1055#1091#1090#1100' '#1082' '#1092#1072#1081#1083#1091': D:\Temp_user\DATA\Borland Studio Projects\!Creator' +
      '_0.67\'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Name_Label: TLabel
    Left = 15
    Top = 73
    Width = 69
    Height = 16
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Author_Label: TLabel
    Left = 15
    Top = 98
    Width = 43
    Height = 16
    Caption = #1040#1074#1090#1086#1088':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Version_Label: TLabel
    Left = 15
    Top = 123
    Width = 50
    Height = 16
    Caption = #1042#1077#1088#1089#1080#1103':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Pass_Label: TLabel
    Left = 15
    Top = 148
    Width = 52
    Height = 16
    Caption = #1055#1072#1088#1086#1083#1100':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Date_Label: TLabel
    Left = 400
    Top = 172
    Width = 35
    Height = 16
    Caption = #1044#1072#1090#1072':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Desc_Label: TLabel
    Left = 15
    Top = 242
    Width = 524
    Height = 16
    Caption = 
      #1054#1087#1080#1089#1072#1085#1080#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' ('#1076#1083#1103' '#1074#1099#1073#1086#1088#1072' '#1092#1072#1081#1083#1072' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103' '#1082#1083#1080#1082#1085#1080#1090#1077' '#1085#1072' '#1086#1082#1085 +
      #1077' '#1074#1074#1086#1076#1072'):'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Email_Label: TLabel
    Left = 15
    Top = 172
    Width = 41
    Height = 16
    Caption = 'E-mail:'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Phone_Label: TLabel
    Left = 15
    Top = 194
    Width = 60
    Height = 16
    Caption = #1058#1077#1083#1077#1092#1086#1085
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
  end
  object Name_Edit: TEdit
    Left = 92
    Top = 69
    Width = 457
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Author_Edit: TEdit
    Left = 92
    Top = 93
    Width = 457
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object Version_Edit: TEdit
    Left = 92
    Top = 118
    Width = 457
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object Pass_Edit: TEdit
    Left = 92
    Top = 143
    Width = 457
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object Desc_Memo: TMemo
    Left = 15
    Top = 264
    Width = 531
    Height = 281
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    Lines.Strings = (
      '')
    OEMConvert = True
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 6
    OnDblClick = Desc_MemoDblClick
  end
  object Next_ToFileIndexButton: TButton
    Left = 474
    Top = 559
    Width = 75
    Height = 25
    Caption = #1044#1072#1083#1077#1077' >>'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = Next_ToFileIndexButtonClick
  end
  object Back_ToMainButton: TButton
    Left = 376
    Top = 559
    Width = 75
    Height = 25
    Caption = '<< '#1053#1072#1079#1072#1076
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    OnClick = Back_ToMainButtonClick
  end
  object Cncl_Button: TButton
    Left = 15
    Top = 559
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 9
    OnClick = Cncl_ButtonClick
  end
  object Date_Picker: TDateTimePicker
    Left = 448
    Top = 169
    Width = 101
    Height = 24
    Date = 39318.534916585640000000
    Time = 39318.534916585640000000
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object Email_Edit: TEdit
    Left = 92
    Top = 168
    Width = 285
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object Phone_Edit: TEdit
    Left = 92
    Top = 193
    Width = 285
    Height = 24
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Interin Font'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
  end
  object Desc_OpenDialog: TOpenDialog
    Left = 56
    Top = 7
  end
end
