unit RemoveWholeDir;

interface

uses SysUtils, Dialogs, CrtProcess;

function RemoveAllDir(sDir : String) : Boolean;

implementation

// ������� ������� ��� ���������� (�����, ����� � ���� ����������)
function RemoveAllDir(sDir : String) : Boolean;
var
  iIndex : Integer;
  SearchRec : TSearchRec;
  sFileName : String;
begin
  try
    if DirectoryExists(sDir) then begin
      CPRun('cmd /c rmdir /S /Q "' + sDir);
      Result := True;
    end;
  except
    ShowMessage('�������� �������� ������� ������ ' + IntToStr(GetLastError));
    Result := False;
  end
//      ShowMessage('DirEx '+sDir);
{      sDir := sDir + '\*.*';
      iIndex := FindFirst(sDir, faAnyFile, SearchRec);
      while iIndex = 0 do begin
        sFileName := ExtractFileDir(sDir)+'\'+SearchRec.Name;
        if SearchRec.Attr = faDirectory then begin
          if (SearchRec.Name <> '' ) and
             (SearchRec.Name <> '.') and
             (SearchRec.Name <> '..') then RemoveAllDir(sFileName);
        end
        else begin
          if SearchRec.Attr <> faArchive then FileSetAttr(sFileName, faArchive);
          if NOT DeleteFile(sFileName) then ShowMessage('���� ' + sFileName + ' �� ��� �����');
        end;
        iIndex := FindNext(SearchRec);
      end;
      FindClose(SearchRec);
      RemoveDir(ExtractFileDir(sDir));
      Result := True;
    end;
//    else NULL;
//      ShowMessage('DirNotEx '+sDir);
  except
    ShowMessage('�������� �������� ������� ������ ' + IntToStr(GetLastError));
    Result := False;
  end
  }
end;

end.
