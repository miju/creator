unit IUP_Creating;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,Buttons, ExtCtrls, FileCtrl, jpeg,
  // added by user
  CheckingSum,
  FolderActions,
  ZLibAdvTools,
  Menus,
  IUPCreator;

  function CS_count(Path: String): Integer;
  function Crt_IUP(IUP_Fld, IUP_File: String): Integer;
  function DODO3(fl1,fl2:string):boolean;
  function DODO2(fl1,fl2:string):boolean;


implementation

function DODO2(fl1,fl2:string):boolean;
begin
  Result:=ZLATCompressFile(fl1,fl2,2,false,true,nil,0,0);
end;

function DODO3(fl1,fl2:string):boolean;
begin
  Result:=ZLATdecompressfile(fl1,fl2,false,true,nil,0,0);
end;

function DODO(fl1,fl2:string):boolean;
begin
  Result:=CopyFile(pchar(fl1),pchar(fl2),false);
end;

// ������� �� � �������� ��� �����-���������
function CS_count(Path: String): Integer;
var checksum_file: TextFile; // ��������� ����������� ����
    CS: int64;
    search:    TSearchRec;
    AttrFile: Integer;//�������� ������
    mask: string;
    sfile: string; // ������ ���� �� ���������  �����
    extn: string; // ��� ��������������� �����

begin
    CS := -1; // �������� ������� ��
    mask :=Path;
//    ShowMessage('Mask = ' + mask);
    extn := '*.*';
    AttrFile := $0000003F; {Any file}
    // ������� �� ������ � �����
    if FindFirst(mask + '\' + extn, AttrFile , search) = 0 then
      repeat
        sfile := mask + '\' + search.Name;
        CS := CS + CheckSum(sfile);
//        ShowMessage('CS of ' + sfile + ' is ' + IntToStr(CheckSum(sfile)));
//        ShowMessage('CS is ' + IntToStr(CS));
      until FindNext(search) <> 0;
    SysUtils.FindClose(search);
    // ������ �� � ����
    AssignFile(checksum_file, Path + '\checksum');
    Rewrite(checksum_file);
    writeln(checksum_file,IntToStr(CS));
    CloseFile(checksum_file);
end;

// ��������������� ���������
function Crt_IUP(IUP_Fld, IUP_File: String): Integer;
begin
  ShowMessage('������ ��.');
  CS_count(IUP_fld_path);
  ShowMessage('�� ���������.');

  // DoFolderAction('F:\111','F:\arch.dat',DODO2);
  if DoFolderAction(IUP_Fld, IUP_File, DODO2) then begin
    Result := 1;
  end
  else begin
    Result := 0;
  end;
end;

end.
